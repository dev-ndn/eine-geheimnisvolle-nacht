//Raumsteuerung
//steuerungsklasse zum wechseln der menues und spielraeume und funktion
//zum abspielen der animationssequenzen.

package adventurespiel.steuerung;
import adventurespiel.GUI.Preloader;
import adventurespiel.fachklassen.Raum;
import adventurespiel.fachklassen.Tuer;
import adventurespiel.handler.KlickAdapter;
import adventurespiel.handler.XMLHandlingLesen;
import java.awt.Image;
import java.util.ArrayList;
/**
 * @author Jessica, Simon
 */
public class RaumSteuerung {
 
    private SpielLoop spiel;
    private KlickAdapter ka;
    private boolean StatusSpiel=false;
    private boolean standardsound=false;
    private Bild ueberspringen;
    
    public RaumSteuerung(SpielLoop s){
        /**
         * @param spiel spiel auf das verwiesen wird
         * @param ka klickadapter auf den verwiesen wird
         * @param statusSpiel statusSpiel abfrage ob man sich im menue oder im spiel befindet. wenn true dann im spiel
         * @param standrardsound wenn true dann spiele spiele spielhintergrundmusik
         * @param ueberspringen ueberspringenbild
         */
        this.spiel = s;
        this.ka = spiel.getKa();
    }
    //WechselnZu Fumktion, um Raumwechsel zu ermoeglichen
     public void wechselnZu(Tuer tuer){
         //wenn die Tuer nicht die FoyerTuer aus Raum 6 ist
         if(!tuer.getName().equals("FoyerTuer")){
              //Raumwechselmethode wird aufgerufen
              ladeRaum(sucheRaum(tuer.getWunschort()));
              if (tuer.getStartbild().equals("oben")) {
                  spiel.setAussehen((Image) spiel.getSpielfigur().getBewegungOben().get(0));
              }
              if (tuer.getStartbild().equals("unten")) {
                  spiel.setAussehen((Image) spiel.getSpielfigur().getBewegungUnten().get(0));
              }
              if (tuer.getStartbild().equals("links")) {
                  spiel.setAussehen((Image) spiel.getSpielfigur().getBewegungLinks().get(0));
              }
              if(tuer.getStartbild().equals("rechts")){
                  spiel.setAussehen((Image) spiel.getSpielfigur().getBewegungRechts().get(0));
              }
              spiel.repaint();
         //Schrift ausgeben
         }else{
             spiel.getSchrift().setText("Da gehe ich lieber nicht runter, da steht der grimmige Butler!");
             ka.soundAbspielen(ka.getHenriHardSounds().get(6));
         }
     }


    /**
     * Raumwechsel- Methode
     *
     * alte Raumbilder werden entfernt und neue aufgerufen
     * Gegenstaende, Ikeas, Personen und Tueren werden ausgelesen und den Listen hinzugefuegt
     * das Inventarbild wird der GUI hinzugefuegt
     */
    public void ladeRaum(Raum ziel){
        //alle Bilder werden entfernt
        spiel.getImages().clear();
        spiel.getKontrollliste().clear();
        spiel.getImagesVordergrund().clear();
        Bild b = new Bild(ziel);
        //Hinzufuegen des Hintergrundbilds
        spiel.getImages().add(b);
        //for- Schleife fuegt sichtbare Ikeas der Bildliste hinzu, damit diese gezeichnet werden
        for(int x = 0 ; x < ziel.getIkea().size() ; x++){
            //wenn das Ikea sichtbar ist
            if(ziel.getIkea().get(x).getIstSichtbar()){
                //fuege Raumbild der Images- Liste hinzu
                spiel.getImages().add(ziel.getIkea().get(x).getRaumbild());
                //wenn es sich um das Ikea Nebel handelt
                if (!ziel.getIkea().get(x).getName().equals("Nebel"))
                {
                    //fuege Bild zu der Kontrollliste hinzu (alle Bilder die in dem Raum enthalten sind)
                    spiel.getKontrollliste().add(ziel.getIkea().get(x).getRaumbild());
                }
                //wenn es sich um spezielle Vordergrundbilder handelt
                if (ziel.getIkea().get(x).getName().equals("KerzeOst1") || ziel.getIkea().get(x).getName().equals("KerzeOst1Brennend") ||
                        ziel.getIkea().get(x).getName().equals("Kerzenständer")||
                    ziel.getIkea().get(x).getName().equals("Zaun")||ziel.getIkea().get(x).getName().equals("Speicherbuch") ||ziel.getIkea().get(x).getName().equals("Tisch"))
                {
                    //fuege sie der Vordergrundsbildliste hinzu
                    spiel.getImagesVordergrund().add(ziel.getIkea().get(x).getRaumbild());
                }
            }
        }

        //for- Schleife fuegt sichtbare Gegenstaende der Bildliste hinzu, damit diese gezeichnet werden
        for(int x = 0 ; x < ziel.getGegenstand().size() ; x++){
            if(ziel.getGegenstand().get(x).getIstSichtbar())
            {
                //fuegt Gegenstand den Listen hinzu
                spiel.getImages().add(ziel.getGegenstand().get(x).getRaumbild());
                spiel.getKontrollliste().add(ziel.getGegenstand().get(x).getRaumbild());
            }
            //wenn es sich um ein spezielles Vordergrundbild handelt
            if (ziel.getGegenstand().get(x).getName().equals("Kerzenstummel")){
                //fuege sie der Kontroll- und der Vordergrundsliste hinzu
                spiel.getImagesVordergrund().add(ziel.getGegenstand().get(x).getRaumbild());
                spiel.getKontrollliste().add(ziel.getGegenstand().get(x).getRaumbild());
            }
        }

        ////for- Schleife fuegt sichtbare Person der Bildliste hinzu, damit diese gezeichnet werden
        for(int x = 0 ; x < ziel.getPerson().size() ; x++){
            //wenn Person sichtbar, fuege sie den Listen hinzu
            if(ziel.getPerson().get(x).getIstSichtbar()){
                spiel.getImages().add(ziel.getPerson().get(x).getRaumbild());
                spiel.getKontrollliste().add(ziel.getPerson().get(x).getRaumbild());
            }
        }

        //for- Schleife fuegt sichtbare Tueren der Bildliste hinzu, damit diese gezeichnet werden
        for(int x = 0 ; x < ziel.getTuer().size() ; x++){
            //wenn Tuer sichtbar, fuege sie den Bildlisten hinzu
            if(ziel.getTuer().get(x).getIstSichtbar()){
                spiel.getImages().add(ziel.getTuer().get(x).getRaumbild());
                spiel.getKontrollliste().add(ziel.getTuer().get(x).getRaumbild());
            }
        }
        //Hintergrundbild des Inventars wird hinzugefügt
        spiel.getImages().add(spiel.getInventarGUI().bildInventar);
        //aktueller Raum wird gesetzt
        spiel.setAktuellerRaum(ziel);
        setStatusSpiel(true);
        if(StatusSpiel){
            if(ziel.getRaumName().equals("Abstellkammer") && standardsound==true){ //soundwechsel bei spielstart
                spiel.thMusik.soundStop();
                spiel.thMusik.soundLoopen(ziel.getSound());
                standardsound=false;
                ka.soundAbspielen(spiel.getHundebellen()); //hundebellen abspielen
                spiel.repaint();
            }
            if(ziel.getRaumName().equals("Innenhof")){ //soundwechsel bei innenhof
                spiel.thMusik.soundStop();
                spiel.thMusik.soundLoopen(ziel.getSound());
                standardsound=true;
            }
            if(ziel.getRaumName().equals("Foyer") && standardsound==true){ //wieder soundwechsel zum alten
                spiel.thMusik.soundStop();
                spiel.thMusik.soundLoopen(ziel.getSound());
                standardsound=false;
            }   
            if(ziel.getRaumName().equals("Speicherraum") && standardsound==true){
               spiel.thMusik.soundStop();
               spiel.thMusik.soundLoopen(ziel.getSound());
               standardsound=false;
               System.out.println("zuruck");
            }
            if(ziel.getRaumName().equals("Hunderaum")){
                spiel.getSchrift().setText("Charlie, endlich hab ich dich gefunden. Ich hole dich da raus!");
                ka.soundAbspielen(spiel.getCharlieHunderaum());
            }
        }
        spiel.setStatusMenue(false);
        spiel.add(spiel.getSchrift());  
    }

    public void wechselnMenue(ArrayList<Bild> bilder){
        spiel.getKa().setKlickenMoeglich(false);
        spiel.getImages().clear();
        spiel.getKontrollliste().clear();
        spiel.getImagesVordergrund().clear();
        spiel.getKa().setGeklicktX(0);
        spiel.getKa().setGeklicktY(0);
        spiel.getKa().setUrspruenglichX(0);
        spiel.getKa().setUrspruenglichY(0);
        spiel.getKa().setAltX(0);
        spiel.getKa().setAltY(0);
        if(StatusSpiel || !spiel.getStatusMenue()){
            spiel.thMusik.soundStop();
            spiel.thMusik.soundLoop(spiel.sound);
        }
        
        for (int i=0; i<bilder.size(); i++){
            spiel.getImages().add(bilder.get(i));
            spiel.getKontrollliste().add(bilder.get(i));
        }
   
        spiel.getKontrollliste().remove(0);

        if(bilder.equals(spiel.getHauptmenue())){
            ueberspringen = spiel.getImages().get(5);
            spiel.getImages().remove(5);//entferne den ueberspringenbutton
            spiel.getKontrollliste().remove(4);//entferne den ueberspringenbutton
            spiel.repaint();
        }
        setStatusSpiel(false);
        spiel.setStatusMenue(true);
        spiel.remove(spiel.getSchrift());
        
       // gui.getSpeicherMenue().getSpielstand01().setVisible(true);
        spiel.repaint();
        
        spiel.getKa().setKlickenMoeglich(true);
    }

    public void spieleAnimation(ArrayList<Bild> bilderAnimation, String animationsArt){
        spiel.getImages().clear();
        spiel.getKontrollliste().clear();
        spiel.setPreloader(new Preloader(spiel));  //introanimationsbilder vorausladen
        spiel.setPreload(true);
        spiel.repaint();
        spiel.setCursor(spiel.getStandardCursor());

        if(animationsArt.equals("intro")){
            spiel.setUeberspringen(true);
            setStatusSpiel(false);
            spiel.setStatusMenue(true);
        }

        if(animationsArt.equals("outro")){
            spiel.setUeberspringen(true);
            setStatusSpiel(false);
            spiel.setStatusMenue(true);
            spiel.thMusik.soundStop();
        }
    
        spiel.setIntroAbspielen(true);
        spiel.setUeberspringen(false);

        for (int i = 0; i < bilderAnimation.size(); i++) {
            spiel.getImages().clear();
            if(animationsArt.equals("spielstart")){
                spiel.getImages().clear();
                spiel.getImages().add(bilderAnimation.get(i));
                spiel.repaint();

                try {
                    Thread.sleep(bilderAnimation.get(i).getAnzeigeDauer());
                } catch (InterruptedException e) {
                }
            }

            if(animationsArt.equals("intro") || animationsArt.equals("outro")){
               if(spiel.isIntroAbspielen() || animationsArt.equals("outro")){
                   spiel.getImages().clear();
                   spiel.getImages().add(bilderAnimation.get(i));
                   spiel.repaint();

                   try {
                       Thread.sleep(bilderAnimation.get(i).getAnzeigeDauer());
                   } catch (InterruptedException e) {
                     }
               }

               if(animationsArt.equals("outro") ){                  
                   spiel.setStatusMenue(true);
                   setStatusSpiel(false);
                   spiel.getRaumsteuerung().wechselnMenue(spiel.getHauptmenue());
                   spiel.getHauptmenue().remove(spiel.getBtnWeiterspielen());    
                }           

                if (i == (bilderAnimation.size()-1)|| spiel.isUeberspringen()) {
                    if(animationsArt.equals("intro")){
                        spiel.getIntroUeberspringen().setVisible(false);
                        spiel.setUeberspringen(false);
                        spiel.repaint();
                        spiel.thMusik.soundStop(); //storysoundmusik beenden
                        XMLHandlingLesen xml = new XMLHandlingLesen(spiel);
                        spiel.laden = true;
                        xml.ladeSpielstand(spiel, "default");
                        spiel.laden = false;
                        xml=null;
                        spiel.setPreloader(new Preloader(spiel));  //raumbilder vorausladen
                        spiel.setPreload(true);
                        spiel.getRaumsteuerung().ladeRaum(spiel.getAbstellkammer());
                        spiel.thMusik.soundLoopen(spiel.getAbstellkammer().getSound());
                        spiel.repaint();
                    }
                }
            }
        }
        if(animationsArt.equals("spielstart")){
            spiel.getRaumsteuerung().wechselnMenue(spiel.getHauptmenue());
            spiel.repaint();
            spiel.thMusik.soundStop();
            spiel.thMusik.soundLoop(spiel.sound);
            standardsound=true;
            spiel.setAlwaysOnTop(false);
        }
        if(animationsArt.equals("outro")){
            spiel.thMusik.soundLoopen(spiel.getHintergrundmusik());
        }
    }

    //Methode zum suchen eines Raums
    public Raum sucheRaum(String name){
        Raum raum = null;
        for(int x = 0 ; x < spiel.getRaumliste().size() ; x++) {
            if(spiel.getRaumliste().get(x).getRaumName().equals(name)) raum = spiel.getRaumliste().get(x);
        }
        return raum;
    }

    //get- und set- Methoden
    public boolean getStatusSpiel(){
        return StatusSpiel;
    }

    public Bild getUeberspringen() {
        return ueberspringen;
    }

    public void setStatusSpiel(boolean sp){
        this.StatusSpiel = sp;
    }

    public void setStandardsound(boolean standardsound) {
        this.standardsound = standardsound;
    }
}
