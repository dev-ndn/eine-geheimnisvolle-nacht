//Klasse SpielLoop
package adventurespiel.steuerung;

import adventurespiel.GUI.DialogBeenden;
import adventurespiel.fachklassen.Speichermenue;
import adventurespiel.fachklassen.Hintergrundmusik;
import adventurespiel.GUI.InventarGUI;
import adventurespiel.GUI.Preloader;
import adventurespiel.GUI.Soundmenue;
import adventurespiel.fachklassen.Inventar;
import adventurespiel.fachklassen.Koordinaten;
import adventurespiel.fachklassen.LadeMenue;
import adventurespiel.fachklassen.Raum;
import adventurespiel.fachklassen.Spielfigur;
import adventurespiel.handler.KlickAdapter;
import adventurespiel.handler.XMLHandlingLesen;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Button;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.TextArea;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Simon, Jessica, Nadine
 */
public class SpielLoop extends Frame implements Runnable {

    /**
     * @return the logo
     */
    public static String getLogo() {
        return logo;
    }

    /**
     * @param aLogo the logo to set
     */
    public static void setLogo(String aLogo) {
        logo = aLogo;
    }

    private Toolkit tk = getToolkit();
    private static String logo = "src/AllgemeinBilder/cursor0.png";
    private KlickAdapter ka = new KlickAdapter();
    private ArrayList<Bild> images = new ArrayList<Bild>(); 
    private ArrayList<Bild> imagesVordergrund = new ArrayList<Bild>();
    private ArrayList<Bild> kontrollliste = new ArrayList<Bild>(); 
    private ArrayList<Raum> raumliste = new ArrayList<Raum>(); //Liste mit allen Raeumen
    private ArrayList<Bild> hauptmenue, optionen, anleitung, credits, online, introBilder, outroBilder, spielstartbilder;
    private Speichermenue speicherMenue;
    private LadeMenue ladenMenue;
    private Soundmenue soundMenue;
    private Raum Abstellkammer, Foyer, Kaminzimmer, Innenhof, Speicherraum, Hunderaum, aktuellerRaum;
    private RaumSteuerung raumsteuerung;
    private Image aussehen, image;
    private InventarGUI inventarGUI;
    private Inventar inventar;
    private Spielfigur spielfigur;
    private Graphics graphics;
    private Label schrift;
    private int grenzeOben = 80;
    private int grenzeUnten = 630;
    private int grenzeLinks = 30;
    private int grenzeRechts = 995;
    public Hintergrundmusik thMusik;
    private Font schriftTextleiste = new Font("Comic Sans", Font.BOLD, 14);
    private File soundEinsammeln = new File("src/sounds/sound01.wav");        //zu gegebener zeit verschiedene sounds in arraylist packen..
    private File hintergrundmusik = new File("src/sounds/menuemusik_2.wav");
    private File klickSound = new File("src/sounds/klick.wav");
    private File zurueckSound = new File("src/sounds/zurueck.wav");
    private File hundebellen = new File("src/sounds/hundebellen_3.wav");
    private File storySound = new File("src/sounds/spielstorymusik_1.wav");
    private File outroSound = new File("src/sounds/outro.wav");
    private File charlieHunderaum = new File ("src/sounds/ikeahinweissounds/raum6/kaefig.wav");
    private Bild btnWeiterspielen = new Bild(new Koordinaten(520, 670), "src/Bilder/menues/hauptmenue/btnWeiterspielen.png", 468, 78, "btnWeiterspielen");
    private boolean statusMenue = true;
    private boolean hintergrundmusikAbspielen; //soundausgaben ein und ausschalten
    private boolean soundsAbspielen;
    public AudioClip sound01;
    public AudioClip sound;
    public boolean kontrollzeichnen = true; //kontrolliert das zeichnen von dingen, die beim programmieren benötigt werden
    public boolean dateiGefunden = true;
    private DialogBeenden dialogBeenden;
    private MediaTracker mediatracker;
    private String spielstand;
    private Preloader preloader;
    private boolean preload=false;
    private boolean menuePreload=true;
    private boolean wirdGeladen=false;
    boolean ueberspringen=false;
    private boolean introAbspielen=true;
    private Button introUeberspringen = new Button("Intro überspringen");
    private Font stil1 = new Font("Arial", Font.BOLD, 16);
    public boolean gestartet;

    Cursor standardCursor = Toolkit.getDefaultToolkit().createCustomCursor(
            tk.getImage("src/AllgemeinBilder/cursor1.png"),
            new Point(2, 3), "Cursor");
    Cursor raumWechsel = Toolkit.getDefaultToolkit().createCustomCursor(
            tk.getImage("src/AllgemeinBilder/cursor3.png"),
            new Point(0, 0), "Cursor");
    private Cursor aufheben = Toolkit.getDefaultToolkit().createCustomCursor(
            tk.getImage("src/AllgemeinBilder/cursor0.png"),
            new Point(0, 0), "Cursor");
    public boolean laden;

    public SpielLoop(XMLHandlingLesen daten) {
        /**
         * @param tk Toolkit zum Bilder laden
         * @param logo pfad zum icon des frames
         * @param ka Klickadapter zur klickauswertung
         * @param images arrayListe - aktuell gezeigte Bilder des Fensters hinter der Spielfigur
         * @param imagesVordergrund - aktuell gezeigte Bilder des Fensters vor der Spielfigur
         * @param kontrollliste arrayListe - alle Gegenstaende im Spielfenster/Inventar
         * @param raumliste liste aller raeume
         * @param hauptmenue, optionen, anleitung, credits, online, introBilder, outroBilder, spielstartbilder menuebilder bzw sequenzbilder
         * @param speichermenue speichermenue fuer spielstand speichern
         * @param ladeMenue lademenue fuer speilstand laden
         * @param soundmenue soundmenue zum einstellen des sounds
         * @param abstellkammer, foyer, kaminzimmer, speicherraum, hunderaum, raeume des spiels
         * @param aktuellerRaum aktuell angezeigter raum des spiels
         * @param raumsteuerung raumsteuerung zum wechseln des menues, raumes, abspielen der sequenzen
         * @param aussehen aktuell angezeigtes spielfigurbild
         * @param image hilfsimage fuer doublebuffering
         * @param inventargui inventargui fuer spiel
         * @param inventar inventar
         * @param spielfigur spielfigur
         * @param graphics hilfsgraphics fuer doublebuffering
         * @param schrift textleiste ueberhalb des spielfeldes fuer infotexte
         * @paramgrenze oben, unten, links, rechts spielfeldbegrenzung
         * @param thMusik thread fuer hintergrundmusik
         * @param schriftTextleiste fontdarstellung fuer schrift
         * @param soundEinsammeln einsammelsound
         * @param hintergrundmusik hintergrundmusik
         * @param klicksound klicksound bei klick auf button im menue
         * @param zuruecksound klicksound bei zurueckbutton im menue
         * @param hundebellen hundebellensound nach introsequenz
         * @param storysound hintergrundmusik fuer introsequenz
         * @param btnWeiterspielen button weiterspielen fuer weiterspielen. befindet sich im hauptmenue
         * @param soundabspielen sound´wird abgespielt wenn true. andernfalls kein abspielen
         * @param raumnummer raumnummern fuer cursoraenderungen
         * @param sound01 hintergrundvariablensound
         * @param sound hintergrundvariablensound
         * @param kontrollzeichnen visuelle kontrolldarstellung der bilderbreite und hoehe durch rote linie, polygonanzeige
         * @param dateiGefunden true wenn sounddatei gefunden wurde. andernfalls false
         * @param dialogBeenden beenden abfrage bei klick auf x oder beenden im hauptmenue
         * @param mediatracker mediatracker fuer spielfigur
         * @param spielstand zu aendernde aufschrift fuer spielstandbuttons im ladenmenue (hilfsvariable zur zwischenspeicherung)
         * @param preloader preloader fuer alle bilder des spiels
         * @param preload lade alle bilder wenn true. andernfalls lade keine bilder
         * @param menuePreload preloader fuer alle menuebilder
         * @param menueStandardcursor standardcursor
         * @param cursorGeaendert true wenn cursor sich geaendert hat. andernfalls false
         * @param aktuellerCursor aktuell angezeigter Cursor
         * @param ueberspringen ueberspringe das intro wenn true. andernfalls intro weiter abspielen
         * @param introUeberspringen button zum ueberspringen des intros
         * @param stil1 fontdarstellung fuer ueberspringenbutton
         * @param standardCursor standardCursor
         * @param raumWechsel cursor bei raumwechsel
         * @param aufheben cursor bei gegenstandaufheben
         */

        super("Eine geheimnisvolle Nacht");
        //kleines Icon setzen, welches in der Titelzeile des Fensters erscheinen soll
        setIconImage(tk.getImage("src/AllgemeinBilder/cursor0.png"));
        //Kickadapter dem Spiel zuordnen
        ka.setSp(this);
        addMouseListener(ka);
        addMouseMotionListener(ka);
        mediatracker = new MediaTracker(this);
        dialogBeenden = new DialogBeenden(this);
        //bilder im vorraus laden, damit sie im arbeitsspeucher enthalten sind
        ladeBilder();
        this.getKa().setSoundeinstellungenGeaendert(true);
        this.setAlwaysOnTop(true);
        daten.ladeSoundeinstellungen(this);
        soundMenue = new Soundmenue(this);
        ladenMenue = new LadeMenue(this);
        speicherMenue = new Speichermenue(this);
        setSize(1024, 768);
        setLocationRelativeTo(null);
        setLayout(null);
        setResizable(false);
        //hintergrundmusik thread erschaffen;
        thMusik = new Hintergrundmusik(this);
        raumsteuerung = new RaumSteuerung(this);
        spielfigur = new Spielfigur("Peter", null, new Koordinaten(getKa().getAltX(), getKa().getAltY()), null);
        //Anfangsbild der Spielfigur = vorderansicht
        aussehen = spielfigur.bewegungUnten.get(0);
        inventar = new Inventar(spielfigur);
        spielfigur.setInventar(inventar);
        inventarGUI = new InventarGUI(inventar);
        schrift = new Label("");
        schrift.setVisible(false);
        schrift.setBounds(25, 30, 975, 25);
        schrift.setForeground(Color.darkGray);
        schrift.setFont(schriftTextleiste);
        //intro-ueberspringenbutton beim intro
        introUeberspringen.setBackground(new Color(90,103,57));
        introUeberspringen.setBounds(750, 670,200,50);
        introUeberspringen.setFont(stil1);
        introUeberspringen.setVisible(false);
        introUeberspringen.setCursor(aufheben);
        introUeberspringen.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                setUeberspringen(true);
                setIntroAbspielen(false);
                introUeberspringen.setLabel("Spiel wird geladen");
                thMusik.soundStop();
            }
        });
        add(introUeberspringen);

        //sound laden
        try {
            sound01 = Applet.newAudioClip(new File("src/sounds/menuemusik_2.wav").toURI().toURL());
        } catch (MalformedURLException ex) {
            Logger.getLogger(SpielLoop.class.getName()).log(Level.SEVERE, null, ex);
        }

        //textleiste hinzufuegen
        add(schrift);

        //musikthread
        try {
            sound = Applet.newAudioClip(hintergrundmusik.toURI().toURL());
        } catch (MalformedURLException ex) {
            Logger.getLogger(SpielLoop.class.getName()).log(Level.SEVERE, null, ex);
        }

        //beenden-fenster aufrufen bei klick auf beenden/ x
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                //System.exit(0);
                getDialogBeenden().setLocation((getLocation().x+700), (getLocation().y+30));
                getDialogBeenden().setVisible(true);
            }
        });
        gestartet = true;
    }

    public void start() {
        Thread th = new Thread(this);
        th.start();
    }

    public void stop() {
    }

    public void destroy() {
    }

    //double buffering - zwischenspeichern des noch nicht vollstaendig geladenen bildes
    public void update(Graphics g) {
        if (image == null) {
            image = createImage(this.getSize().width, this.getSize().height);
            graphics = image.getGraphics();
        }
        graphics.setColor(getBackground());
        graphics.fillRect(0, 0, this.getSize().width, this.getSize().height);
        graphics.setColor(getForeground());
        paint(graphics);
        g.drawImage(image, 0, 0, this);
        image.flush();
    }

    // while(true) - schleife
    public void run() {
        
        //anfangs auf statusMenue setzen und spielstartsequenz abspielen
        this.setStatusMenue(true);
        getRaumsteuerung().spieleAnimation(getSpielstartbilder(), "spielstart");

        while (gestartet){

            //sound ausschalten
            if (ka.isSoundeinstellungenGeaendert() && this.getHintergrundmusikAbspielen() == true) {
                thMusik.soundStop();
                thMusik.soundLoop(sound01);
                ka.setSoundeinstellungenGeaendert(false);
            }

            //sound einschalten
            if (ka.isSoundeinstellungenGeaendert() && this.getHintergrundmusikAbspielen() == false) {
                thMusik.soundStop();
                ka.setSoundeinstellungenGeaendert(false);
            }

            boolean mausWechsel = false;
            Cursor maus = null;
            for (int i = 0; i < kontrollliste.size(); i++) {
                //wenn der Cursor innerhalb des Polygons Dings ist, dann soll sich das Bild des Cursors aendern
                if (getAktuellerRaum()!=null && (kontrollliste.get(i).getFlaeche() != null &&
                        kontrollliste.get(i).getFlaeche().contains(getKa().getActX(), getKa().getActY()))) {
                    mausWechsel |= true;
                    //wenn der Cursor auf einer Tuer ist, soll es der raumwechselcursor sein
                    if(kontrollliste.get(i).getTuer()!=null)maus =  raumWechsel;
                    //wenn der Cursor auf keiner Tuer ist, soll es der aktions/aufhebencursor sein
                    if(kontrollliste.get(i).getTuer()==null)maus =  getAufheben();
                }
                if (getAktuellerRaum()!=null && (getKa().getBildAusInventar(getKa().getActX(), getKa().getActY(), inventarGUI.getBildliste())!=null)) {
                    mausWechsel |= true;
                    maus =  getAufheben();
                }
                //wenn der Cursor innerhalb des Bildes eines Menuesbuttons ist, dann soll sich das Bild des Cursors aendern
                if (getStatusMenue() && (getKa().getGeklicktesBild(getKa().getActX(), getKa().getActY(), kontrollliste)!=null)) {
                    mausWechsel |= true;
                    maus =  getAufheben();
                }
            }
            if (mausWechsel) {
                setCursor(maus);
            } else {
                setCursor(getStandardCursor());
            }


            //klicklistener aufrufen
            getKa().KlickListener();

            try {
                Thread.sleep(10);              
            } catch (InterruptedException ex) {
                Logger.getLogger(SpielLoop.class.getName()).log(Level.SEVERE, null, ex);
              }
        }
    }

    //aktuelle bilder zeichnen
    public void paint(Graphics gr) {
        if(!laden){
        
        //bilder einmal anzeigen, damit sie im arbeitsspeicher gespeichert sind
        //danach preload immer auf false
        //menuebilder
        if(isMenuePreload()){
            for (int i=0; i<getPreloader().getMenuePreloadListe().size();i++){
                gr.drawImage(preloader.getMenuePreloadListe().get(i).getRaumImage(),0,0,this);
                repaint();              
            }
            menuePreload=false;

        }

        //spielbilder/animationsbilder
        if(isPreload()){ 
            for (int i=0; i< preloader.getPreloadListe().size();i++){
                gr.drawImage(preloader.getPreloadListe().get(i).getRaumImage(),0,0,this);
                repaint();
            }          
            preload=false;
        }

        //spielfigurbilder in mediatracker vorausgeladen
        if (mediatracker.checkAll()) {

            //------------------------------------------------------
            //Bilder zeichnen (hinter Spielfigur)
            for (int i = 0; i < images.size(); i++) {              
                gr.drawImage(images.get(i).getRaumImage(), images.get(i).getKoordinaten().getXKoordinate(), images.get(i).getKoordinaten().getYKoordinate(), this);         
            }
            //wenn man sich im Spiel befindet, dann zeichne spielfigur und inventar
            if (raumsteuerung.getStatusSpiel() == true) { 
                //Spielfigur zeichnen an der position d.h. an altx/y bisher, minus einen betrag um die linke obere ecke des bildes zu bestimmen
                gr.drawImage(aussehen, getKa().getAltX()-45, getKa().getAltY()-195, this);
                //-----------------------------------------------------------------------------------------------------
                //Inventar zeichnen
                //for- Schleife: jedes Bild der BildListe wird gezeichnet
                for (int x = 0; x < inventarGUI.bildliste.size(); x++) { 
                    Bild aktuell = inventarGUI.bildliste.get(x);
                    //Rechnung, an welcher Stelle im Inventar die Bilder gezeichnet werden
                    gr.drawImage(inventarGUI.bildliste.get(x).getInventarImage(), inventarGUI.bildInventar.getKoordinaten().getXKoordinate() + 46 + (x * 88), inventarGUI.bildInventar.getKoordinaten().getYKoordinate(), this);
                    //Abspeicherung der neuen Koordinaten
                    aktuell.setKoordinaten(new Koordinaten(inventarGUI.bildInventar.getKoordinaten().getXKoordinate() + 46 + (x * 88), inventarGUI.bildInventar.getKoordinaten().getYKoordinate()));
                }
                //getAktuellerRaum().getWegfindung().zeichneGehflaeche(gr); //kontrollausgabe der gehflaeche
            }
        }
        if (mediatracker.isErrorAny()) {
            System.out.println("Fehler. Beim Laden in den Mediatracker ist einn Fehler entstanden");
        }

        //vordergrundbilder zeichnen
        for (int i = 0; i < imagesVordergrund.size(); i++) {         
            gr.drawImage(imagesVordergrund.get(i).getRaumImage(), imagesVordergrund.get(i).getKoordinaten().getXKoordinate(), imagesVordergrund.get(i).getKoordinaten().getYKoordinate(), this);
        }

        //kontrolle fuer programmierung - alle bilder werden mit werden umrahmt.
        //weißer rahmen = polygone des jeweiligen bildes (werte aus xml)
        //roter rahmen = hoehe und breite des jeweiligen bildes (werte aus xml)
        if(kontrollzeichnen){           
            gr.setColor(Color.white);
            for (int i = 0; i < images.size(); i++) {
                if (images.get(i).getFlaeche() != null) {
                    gr.drawPolygon(images.get(i).getFlaeche());
                }
            }       
            gr.setColor(Color.red);
            for (int i = 0; i < images.size(); i++) {
                gr.drawRect(images.get(i).getKoordinaten().getXKoordinate(), images.get(i).getKoordinaten().getYKoordinate(), images.get(i).getBreite(), images.get(i).getHoehe());          
            }          
        }
        //preload der bilder, alle bilder werden einmal gezeichnet vor spielstart
        if(isWirdGeladen()){
            gr.drawImage(preloader.getWirdGeladen().getRaumImage(),0,0,this);
            ladenMenue.setWirdGeladen(true);
            ladenMenue.actionPerformed(ladenMenue.getAe());
        }

        //Konntrollzeichnen fuer spielfigurbewegung
        if(kontrollzeichnen){
            // zeichnet gruenen punkt an position zu der tatsächlich gelaufen werden soll (geklicktx/y)
            gr.setColor(Color.green);
            gr.drawOval(ka.getGeklicktX(), ka.getGeklicktY(), 5, 5);
            // zeichnet pinken punkt an aktueller position der figur (altx/y)
            gr.setColor(Color.pink);
            gr.drawOval(ka.getAltX(), ka.getAltY(), 5, 5);
            // zeichnet gelben punkt an geklickter position (ursprunglichx/y)
            gr.setColor(Color.yellow);
            gr.drawOval(ka.getUrspruenglichX(), ka.getUrspruenglichY(), 5, 5);
        }
        }
    }

    //Startmethode des Spiels
    /**
     * das Spiel benoetigt mehr Speicher als die JVM per default bereit stellt,
     * daher muss der Speicher erhoeht werden. dies wird mittels aufruf per konsole oder batch-file erledigt
     * damit kein lauffaehiges spiel wegen zu wenig speicher gestartet wird, gibt es eine if()-anweisung
     */
    public static void main(String[] args) {
        //wenn der max.Speicher kleiner ist, als er soll
        if (Runtime.getRuntime().maxMemory() > 230000000) {
            //xml-daten laden
            XMLHandlingLesen daten = new XMLHandlingLesen();
            SpielLoop spiel = new SpielLoop(daten);
            XMLHandlingLesen.setSpiel(spiel);
            //menuebilder listen einlesen
            XMLHandlingLesen.getSpiel().setHauptmenue(daten.menueAuslesen("hauptmenue"));
            spiel.setOptionen(daten.menueAuslesen("optionen"));
            spiel.setAnleitung(daten.menueAuslesen("anleitung"));
            spiel.setCredits(daten.menueAuslesen("credits"));
            spiel.setIntroBilder(daten.menueAuslesen("introBilder"));
            spiel.setOutroBilder(daten.menueAuslesen("outroBilder"));
            spiel.setSpielstartbilder(daten.menueAuslesen("Spielstartbilder"));
            spiel.setOnline(daten.menueAuslesen("online"));

            //spielstaende laden
            for (int i = 0; i < spiel.getLadenMenue().getSpielstaende().size(); i++) {
                spiel.getLadenMenue().getSpielstaende().get(i).setAufschrift(daten.ladeSpielstandname(i + 1));
            }

            //daten.ladeSpielstand(spiel, "default");
            spiel.setPreloader(new Preloader(spiel));//meneubilder vorausladen
            spiel.setPreload(true);


            // Simons kleiner helfer ;)
            //Raumlader - springe in einen beliebigen raum
            //anleitung: zeile 293 und 294 auskommentieren, zeile 469 einkommentieren und
            //den gewuenschten raum einkommentieren

            //spiel.getRaumsteuerung().wechselnMenue(spiel.getHauptmenue());
            //spiel.getRaumsteuerung().ladeRaum(spiel.getAbstellkammer());
            //spiel.getRaumsteuerung().ladeRaum(spiel.getFoyer());
            //spiel.getRaumsteuerung().ladeRaum(spiel.getInnenhof());
            //spiel.getRaumsteuerung().ladeRaum(spiel.getKaminzimmer());
            //spiel.getRaumsteuerung().ladeRaum(spiel.getSpeicherraum());
            //spiel.getRaumsteuerung().ladeRaum(spiel.getHunderaum());


            spiel.setVisible(true);
            spiel.run();
            //wenn der speicher zu klein ist, öffne einen Frame, der den Fehler mitteilt
        } else {
            System.out.println("Memory ist zu klein");
            Frame fehler = new Frame("Fehler");
            fehler.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
            fehler.add(new TextArea("MaximumMemory der JavaVirtualMachine ist zu klein! \nBitte die readme.txt lesen. \n" +
                    "Spiel mit der 'spiel_starten_Win.bat' oder 'spiel_starten_Mac.command starten'.", 1, 3));
            fehler.setSize(500, 150);
            fehler.setLocationRelativeTo(null);
            fehler.setAlwaysOnTop(true);
            fehler.setVisible(true);
            fehler.setAlwaysOnTop(false);
        }
    }

   
    public Raum getAbstellkammer() {
        return Abstellkammer;
    }

    public Raum getAktuellerRaum() {
        return aktuellerRaum;
    }

    public void setAktuellerRaum(Raum aktuellerRaum) {
        this.aktuellerRaum = aktuellerRaum;
    }

    public void setAussehen(Image aussehen) {
        this.aussehen = aussehen;
    }

    public void setAbstellkammer(Raum kammer) {
        this.Abstellkammer = kammer;
    }

    public void setSchrift(Label schrift) {
        this.schrift = schrift;
    }

    public Raum getFoyer() {
        return Foyer;
    }

    public RaumSteuerung getRaumsteuerung() {
        return raumsteuerung;
    }

    public ArrayList<Raum> getRaumliste() {
        return raumliste;
    }

    public ArrayList<Bild> getKontrollliste() {
        return kontrollliste;
    }

    public Label getSchrift() {
        return schrift;
    }

    public ArrayList<Bild> getImages() {
        return images;
    }

    public ArrayList<Bild> getImagesVordergrund() {
        return imagesVordergrund;
    }

    public Inventar getInventar() {
        return inventar;
    }

    public InventarGUI getInventarGUI() {
        return inventarGUI;
    }

    public Image getAussehen() {
        return aussehen;
    }

    public Spielfigur getSpielfigur() {
        return spielfigur;
    }

    public int getGrenzeLinks() {
        return grenzeLinks;
    }

    public int getGrenzeOben() {
        return grenzeOben;
    }

    public int getGrenzeRechts() {
        return grenzeRechts;
    }

    public int getGrenzeUnten() {
        return grenzeUnten;
    }

    public File getSoundEinsammeln() {
        return soundEinsammeln;
    }

    public void setFoyer(Raum foyer) {
        this.Foyer = foyer;
    }

    public Raum getKaminzimmer() {
        return Kaminzimmer;
    }

    public void setKaminzimmer(Raum Kaminzimmer) {
        this.Kaminzimmer = Kaminzimmer;
    }

    public Raum getInnenhof() {
        return Innenhof;
    }

    public boolean getStatusMenue() {
        return statusMenue;
    }

    public void setInnenhof(Raum Innenhof) {
        this.Innenhof = Innenhof;
    }

    public Raum getHunderaum() {
        return Hunderaum;
    }

    public void setHunderaum(Raum Hunderaum) {
        this.Hunderaum = Hunderaum;
    }

    public Raum getSpeicherraum() {
        return Speicherraum;
    }

    public ArrayList<Bild> getIntroBilder() {
        return introBilder;
    }

    public ArrayList<Bild> getAnleitung() {
        return anleitung;
    }

    public ArrayList<Bild> getCredits() {
        return credits;
    }

    public File getKlickSound() {
        return klickSound;
    }

    public File getZurueckSound() {
        return zurueckSound;
    }

    public Speichermenue getSpeicherMenue() {
        return speicherMenue;
    }

    public DialogBeenden getDialogBeenden() {
        return dialogBeenden;
    }

    public void setSpeicherraum(Raum Speicherraum) {
        this.Speicherraum = Speicherraum;
    }

    public ArrayList<Bild> getHauptmenue() {
        return hauptmenue;
    }

    public LadeMenue getLadenMenue() {
        return ladenMenue;
    }

    public void setHauptmenue(ArrayList<Bild> hauptmenue) {
        this.hauptmenue = hauptmenue;
    }

    
    public ArrayList<Bild> getOptionen() {
        return optionen;
    }

    public void setOptionen(ArrayList<Bild> optionen) {
        this.optionen = optionen;
    }

    public void setStatusMenue(boolean statusMenue) {
        this.statusMenue = statusMenue;
    }

    public void setAnleitung(ArrayList<Bild> anleitung) {
        this.anleitung = anleitung;
    }

    public void setCredits(ArrayList<Bild> credits) {
        this.credits = credits;
    }

    public void setIntroBilder(ArrayList<Bild> introBilder) {
        this.introBilder = introBilder;
    }

    public void setSpeicherMenue(Speichermenue speicherMenue) {
        this.speicherMenue = speicherMenue;
    }

    public void setLadenMenue(LadeMenue ladenMenue) {
        this.ladenMenue = ladenMenue;
    }

    public Bild getBtnWeiterspielen() {
        return btnWeiterspielen;
    }

    public void setInventar(Inventar inventar) {
        this.inventar = inventar;
    }

    public KlickAdapter getKa() {
        return ka;
    }

    public void setKa(KlickAdapter ka) {
        this.ka = ka;
    }

    public void ladeBilder() {
        int index;
        int temp = 0;

        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/links_01.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/links_02.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/links_03.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/links_04.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/oben_01.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/oben_02.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/oben_03.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/rechts_01.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/rechts_02.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/rechts_03.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/rechts_04.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/unten_01.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/unten_02.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/Spielfigur/unten_03.PNG"), 1);
        mediatracker.addImage(tk.getImage("src/Bilder/raum/innenhof.jpg"), 1);
        mediatracker.addImage(tk.getImage("src/AllgemeinBilder/BilderAnimationen/1.jpg"), 2);
        mediatracker.addImage(tk.getImage("src/AllgemeinBilder/BilderAnimationen/2.jpg"), 2);
        mediatracker.addImage(tk.getImage("src/AllgemeinBilder/BilderAnimationen/3.jpg"), 2);
        mediatracker.addImage(tk.getImage("src/AllgemeinBilder/BilderAnimationen/4.jpg"), 2);
        mediatracker.addImage(tk.getImage("src/AllgemeinBilder/BilderAnimationen/5.jpg"), 2);
        try {
            mediatracker.waitForAll();
        } catch (Exception e) {
          }

    }

    public String getSpielstand() {
        return spielstand;
    }

    public Hintergrundmusik getThMusik() {
        return thMusik;
    }

    public void setThMusik(Hintergrundmusik thMusik) {
        this.thMusik = thMusik;
    }

    public void setSpielstand(String spielstand) {
        this.spielstand = spielstand;
    }

    public boolean getHintergrundmusikAbspielen() {
        return hintergrundmusikAbspielen;
    }

    public boolean getSoundsAbspielen() {
        return soundsAbspielen;
    }

    public Soundmenue getSoundMenue() {
        return soundMenue;
    }

    public void setSoundMenue(Soundmenue soundMenue) {
        this.soundMenue = soundMenue;
    }

    public void setHintergrundmusikAbspielen(boolean hintergrundmusikAbspielen) {
        this.hintergrundmusikAbspielen = hintergrundmusikAbspielen;
    }

    public void setSoundsAbspielen(boolean soundsAbspielen) {
        this.soundsAbspielen = soundsAbspielen;
    }

    public Preloader getPreloader() {
        return preloader;
    }

    public void setPreloader(Preloader preloader) {
        this.preloader = preloader;
    }

    public boolean isPreload() {
        return preload;
    }

    public boolean isMenuePreload() {
        return menuePreload;
    }

    public boolean isWirdGeladen() {
        return wirdGeladen;
    }

    public void setWirdGeladen(boolean wirdGeladen) {
        this.wirdGeladen = wirdGeladen;
    }

    public ArrayList<Bild> getSpielstartbilder() {
        return spielstartbilder;
    }

    public void setSpielstartbilder(ArrayList<Bild> spielstartbilder) {
        this.spielstartbilder = spielstartbilder;
    }

    public void setPreload(boolean preload) {
        this.preload = preload;
    }

    public File getStorySound() {
        return storySound;
    }

    public void setStorySound(File storySound) {
        this.storySound = storySound;
    }

    public File getHundebellen() {
        return hundebellen;
    }

    public ArrayList<Bild> getOnline() {
        return online;
    }

    public void setOnline(ArrayList<Bild> online) {
        this.online = online;
    }

    public Cursor getStandardCursor() {
        return standardCursor;
    }

    public ArrayList<Bild> getOutroBilder() {
        return outroBilder;
    }

    public void setOutroBilder(ArrayList<Bild> outroBilder) {
        this.outroBilder = outroBilder;
    }

    public boolean isUeberspringen() {
        return ueberspringen;
    }

    public void setUeberspringen(boolean ueberspringen) {
        this.ueberspringen = ueberspringen;
    }

    public Button getIntroUeberspringen() {
        return introUeberspringen;
    }

    public File getHintergrundmusik() {
        return hintergrundmusik;
    }
   
    public boolean isIntroAbspielen() {
        return introAbspielen;
    }

    public void setIntroAbspielen(boolean introAbspielen) {
        this.introAbspielen = introAbspielen;
    }

    public File getOutroSound() {
        return outroSound;
    }

    public File getCharlieHunderaum() {
        return charlieHunderaum;
    }

    

    /**
     * @return the aufheben
     */
    public Cursor getAufheben() {
        return aufheben;
    }

    
    
}
