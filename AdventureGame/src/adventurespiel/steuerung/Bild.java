//Klasse Bild
package adventurespiel.steuerung;

import adventurespiel.fachklassen.*;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Toolkit;
/**
 * @author Jessica, Nadine
 */
public class Bild {
    //Attribute
    private String name, quelleRaumbild, quelleInventar;
    private Image raumImage, inventarImage;
    private Koordinaten koordinaten;
    private int breite, hoehe, breiteInventar, hoeheInventar, anzeigeDauer;
    private Gegenstand gegenstand;
    private Ikea ikea;
    private Person person;
    private Tuer tuer;
    private Raum raum;
    private Polygon flaeche;
    private Toolkit tk = Toolkit.getDefaultToolkit();

    //Konstruktoren
    public Bild(){
    }

    public Bild(Koordinaten koordinaten, String quelle){
        this.koordinaten=koordinaten;
        this.quelleRaumbild= quelle;
        this.raumImage = tk.getImage(this.quelleRaumbild);
    }

    public Bild(Koordinaten koordinaten, String quelle, int breite, int hoehe, String name){
        this.koordinaten=koordinaten;
        this.quelleRaumbild= quelle;
        this.breite=breite;
        this.hoehe=hoehe;
        this.raumImage = tk.getImage(this.quelleRaumbild);
        this.name=name;
    }

    public Bild(String quelleInventar){
        this.quelleInventar= quelleInventar;
    }

    //Konstruktor fuer Raum
    public Bild(Raum raum){
        this.name=raum.getRaumName();
        this.koordinaten=raum.getKoordinaten();
        this.breite=raum.getBreite();
        this.hoehe=raum.getHoehe();
        this.quelleRaumbild=raum.getQuelle();
        this.quelleInventar=null;
        this.gegenstand = null;
        this.ikea = null;
        this.person = null;
        this.tuer = null;
        this.raum = raum;
        this.raumImage = tk.getImage(this.quelleRaumbild);
    }

    //Konstruktor fuer Gegenstand
    public Bild(Gegenstand gegenstand){
        this.name= gegenstand.getName();
        this.koordinaten=gegenstand.getKoordinaten();
        this.breite=gegenstand.getBreite();
        this.hoehe=gegenstand.getHoehe();
        this.breiteInventar=80;
        this.hoeheInventar=80;
        this.quelleRaumbild=gegenstand.getQuelleRaumbild();
        this.quelleInventar=gegenstand.getQuelleInventar();
        this.gegenstand = gegenstand;
        this.ikea = null;
        this.person = null;
        this.tuer = null;
        this.raumImage = tk.getImage(this.quelleRaumbild);
        this.inventarImage = tk.getImage(this.quelleInventar);
        this.flaeche = gegenstand.getFlaeche();
    }

    //Konstruktor fuer Ikea
    public Bild(Ikea ikea){
        this.name=ikea.getName();
        this.koordinaten=ikea.getKoordinaten();
        this.breite=ikea.getBreite();
        this.hoehe=ikea.getHoehe();
        this.quelleRaumbild=ikea.getQuelleRaumbild();
        this.quelleInventar=null;
        this.gegenstand = null;
        this.ikea = ikea;
        this.person = null;
        this.tuer = null;
        this.raumImage = tk.getImage(this.quelleRaumbild);
        this.flaeche = ikea.getFlaeche();
    }

    //Konstruktor fuer Person
    public Bild(Person person){
        this.name=person.getName();
        this.koordinaten=person.getKoordinaten();
        this.breite=person.getBreite();
        this.hoehe=person.getHoehe();
        this.quelleRaumbild=person.getQuelleRaumbild();
        this.quelleInventar=null;
        this.gegenstand = null;
        this.ikea = null;
        this.person = person;
        this.tuer = null;
        this.raumImage = tk.getImage(this.quelleRaumbild);
        this.flaeche = person.getFlaeche();
    }

    //Konstruktor fuer Tuer
    public Bild(Tuer tuer){
        this.name=tuer.getName();
        this.koordinaten=tuer.getKoordinaten();
        this.breite=tuer.getBreite();
        this.hoehe=tuer.getHoehe();
        this.quelleRaumbild=tuer.getQuelleRaumbild();
        this.quelleInventar= null;
        this.gegenstand = null;
        this.ikea = null;
        this.person = null;
        this.tuer = tuer;
        this.raumImage = tk.getImage(this.quelleRaumbild);
        this.flaeche = tuer.getFlaeche();
    }

    //get- Methoden
    public String getName(){
        return this.name;
    }

    public Koordinaten getKoordinaten(){
        return this.koordinaten;
    }

    public int getBreite(){
        return this.breite;
    }

    public int getHoehe(){
        return this.hoehe;
    }

    public String getQuelle(){
        return this.quelleRaumbild;
    }

    public String getQuelleInventar(){
        return quelleInventar;
    }

    public Gegenstand getGegenstand(){
        return gegenstand;
    }

    public Ikea getIkea(){
        return ikea;
    }

    public Person getPerson(){
        return person;
    }

    public Tuer getTuer(){
        return tuer;
    }

    public int getBreiteInventar(){
        return breiteInventar;
    }

    public int getHoeheInventar(){
        return hoeheInventar;
    }

    public Polygon getFlaeche(){
        return flaeche;
    }

    public Image getRaumImage(){
        return raumImage;
    }

    public Image getInventarImage(){
        return inventarImage;
    }

    public int getAnzeigeDauer() {
        return anzeigeDauer;
    }

    //set- Methoden
    public void setBreiteInventar(int breiteInventar){
        this.breiteInventar = breiteInventar;
    }

    public void setHoeheInventar(int hoeheInventar){
        this.hoeheInventar = hoeheInventar;
    }

    public void setName(String name){
        this.name=name;
    }

    public void setKoordinaten(Koordinaten BildKoordinaten){
        this.koordinaten=BildKoordinaten;
    }

    public void setBreite(int breite){
        this.breite=breite;
    }

    public void setHoehe(int hoehe){
        this.hoehe=hoehe;
    }
    
    public void setQuelle(String quelle){
        this.quelleRaumbild=quelle;
    }

    public void setQuelleInventar(String quelleInventar){
        this.quelleInventar = quelleInventar;
    }   

    public void setGegenstand(Gegenstand gegenstand){
        this.gegenstand = gegenstand;
    }

    public void setIkea(Ikea ikea){
        this.ikea = ikea;
    }

    public void setPerson(Person person){
        this.person = person;
    }

    public void setTuer(Tuer tuer){
        this.tuer = tuer;
    }

    public void setFlaeche(Polygon flaeche){
        this.flaeche = flaeche;
    }

    public void setRaumImage(Image raumImage){
        this.raumImage = raumImage;
    }

    public void setInventarImage(Image inventarImage){
        this.inventarImage = inventarImage;
    }

    public String getQuelleRaumbild() {
        return quelleRaumbild;
    }

    public void setQuelleRaumbild(String quelleRaumbild) {
        this.quelleRaumbild = quelleRaumbild;
    }

    public void setAnzeigeDauer(int anzeigeDauer) {
        this.anzeigeDauer = anzeigeDauer;
    }
}
