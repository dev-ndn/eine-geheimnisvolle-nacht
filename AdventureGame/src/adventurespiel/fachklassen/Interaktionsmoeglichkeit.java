package adventurespiel.fachklassen;

import java.io.File;

/**
 *
 * @author Nadine
 * @version 05.10.2009
 */
public class Interaktionsmoeglichkeit {

    private Ikea ikea;
    private Gegenstand gegenstand, neuerGegenstand;
    private String interaktionstext;
    private boolean zielErreicht;
    private File sound;

    /**
     * Konstruktor
     * 
     * @param ikea passendes Ikea der Interaktion
     * @param gegenstand passender Gegenstand der Interaktion
     * @param interaktionstext Text bei Durchführung der Interaktion
     * @param zielErreicht true, sobald letztes Rätsel gelöst wurde; es folgt das Outro
     */
    public Interaktionsmoeglichkeit(Ikea ikea, Gegenstand gegenstand, String interaktionstext, boolean zielErreicht, Gegenstand neuerGegenstand){
	this.ikea = ikea;
	this.gegenstand = gegenstand;
	this.interaktionstext = interaktionstext;
	this.zielErreicht = zielErreicht;
        this.neuerGegenstand = neuerGegenstand;
    }

    public Interaktionsmoeglichkeit(String interaktionstext, boolean zielErreicht, File sound){
	this.interaktionstext = interaktionstext;
	this.zielErreicht = zielErreicht;
        this.sound = sound;
    }

    /**
     * Liefert einen Absatz auf der Konsole mit allen Werten der Attribute des aufrufenden Objekts.
     *
     * @return String
     */
    @Override
    public String toString(){
        if(this.ikea==null && this.gegenstand==null)return "Ikea: "+ "keins" + "\n" +
		"Gegenstand: " + "keins" + "\n" +
		"Interaktionstext: " + this.interaktionstext + "\n" +
		"Ziel erreicht? " + this.zielErreicht;
        if(this.gegenstand==null)return "Ikea: "+ this.ikea.getName() + "\n" +
		"Gegenstand: " + "keins" + "\n" +
		"Interaktionstext: " + this.interaktionstext + "\n" +
		"Ziel erreicht? " + this.zielErreicht;
        if(this.ikea==null)return "Ikea: "+ "keins" + "\n" +
		"Gegenstand: " + "keins" + "\n" +
		"Interaktionstext: " + this.interaktionstext + "\n" +
		"Ziel erreicht? " + this.zielErreicht;
        return "Ikea: "+ this.ikea.getName() + "\n" +
		"Gegenstand: " + this.gegenstand.getName() + "\n" +
		"Interaktionstext: " + this.interaktionstext + "\n" +
		"Ziel erreicht? " + this.zielErreicht;
    }

    public boolean istInteragierbar(Gegenstand gegenstand,Ikea ikea){
        boolean result = false;
        if(gegenstand != null && ikea != null){
            if(ikea.getInteraktionsmoeglichkeit().getIkea() == ikea && ikea.getInteraktionsmoeglichkeit().getGegenstand() == gegenstand){
                result = true;
            }
        }
        return result;
    }

//-get-Methoden----------------------------------------------------------------
    /**
     * 
     * @return Zur Interaktionsmöglichkeit gehörendes Ikea
     */
    public Ikea getIkea(){
	return ikea;
    }
    /**
     *
     * @return Zur Interaktionsmöglichkeit gehörender Gegenstand
     */
    public Gegenstand getGegenstand(){
	return gegenstand;
    }

    /**
     *
     * @return Interaktionstext
     */
    public String getInteraktionstext(){
	return interaktionstext;
    }

    /**
     *
     * @return true, sobald das letzte Rätsel gelöst wurde
     */
    public boolean getZielErreicht(){
	return zielErreicht;
    }

    public Gegenstand getNeuerGegenstand() {
        return neuerGegenstand;
    }


//-set-Methoden----------------------------------------------------------------
    /**
     *
     * @param ikea Ikea der Interaktion setzen
     */
    public void setIkea(Ikea ikea){
	this.ikea = ikea;
    }

    /**
     *
     * @param gegenstand Gegenstand der Interaktion setzen
     */
    public void setGegenstand(Gegenstand gegenstand){
	this.gegenstand = gegenstand;
    }

    /**
     *
     * @param interaktionsText Text der bei Ausführung der Interaktion erscheint
     */
    public void setInteraktionstext(String interaktionstext){
	this.interaktionstext = interaktionstext;
    }

    public void setNeuerGegenstand(Gegenstand neuerGegenstand) {
        this.neuerGegenstand = neuerGegenstand;
    }

    /**
     * @return the sound
     */
    public File getSound() {
        return sound;
    }

    /**
     * @param sound the sound to set
     */
    public void setSound(File sound) {
        this.sound = sound;
    }
}
