package adventurespiel.fachklassen;

import adventurespiel.GUI.DialogLaden;
import adventurespiel.handler.XMLHandlingLesen;
import adventurespiel.steuerung.Bild;
import adventurespiel.steuerung.SpielLoop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class LadeMenue implements ActionListener{

    SpielLoop spiel;
    XMLHandlingLesen xml;
    Menuebutton spielstand01 = new Menuebutton("Leer","1",153,204,740,74,"src/Bilder/menues/spielLaden/buttonHintergrund.JPG", 150,"braun");
    Menuebutton spielstand02 = new Menuebutton("Leer","2",143,320,740,74,"src/Bilder/menues/spielLaden/buttonHintergrund.JPG",150,"braun");
    Menuebutton spielstand03 = new Menuebutton("Leer","3",155,450,740,74,"src/Bilder/menues/spielLaden/buttonHintergrund.JPG",150,"braun");
    Menuebutton spielstand04 = new Menuebutton("Leer","4",155,575,740,74,"src/Bilder/menues/spielLaden/buttonHintergrund.JPG",150,"braun");
    private ArrayList<Menuebutton> spielstaende = new ArrayList<Menuebutton>();
    private ArrayList<Bild> ladenMenueBilder = new ArrayList<Bild>();
    private boolean wirdGeladen=false;
    Bild hintergrund, zurueck;
    private int spielstandLaden;
    DialogLaden dialog;
    private ActionEvent ae;

    public LadeMenue(SpielLoop spiel){
        this.spiel = spiel;
        this.xml = new XMLHandlingLesen(spiel);
        hintergrund= new Bild(new Koordinaten(6,26),"src/Bilder/menues/spielLaden/Hintergrund.JPG",1012, 735, "hintergrundLadenMenue");
        zurueck = new Bild(new Koordinaten(675,695),"src/Bilder/menues/optionen/btnZurueck.png", 166, 34, "LadenZurueck");
        ladenMenueBilder.add(hintergrund);
        ladenMenueBilder.add(zurueck);
        spielstaende.add(spielstand01);
        spielstaende.add(spielstand02);
        spielstaende.add(spielstand03);
        spielstaende.add(spielstand04);
        dialog = new DialogLaden(this);
      
        for (int i = 0; i < spielstaende.size(); i++) {
            spielstaende.get(i).addActionListener(this);
        }

        
                       
                       
   }

    public void actionPerformed(ActionEvent e) {                //hier später die XML ladefunktion reinschreiben

            for (int i = 0; i < spielstaende.size(); i++) {
                if(e.getSource()==spielstaende.get(i)){
                    if(!spielstaende.get(i).getAufschrift().equals("Leer")){
                        spiel.getKa().setKlickenMoeglich(false);
                        //Buttons entfernen
                        for (int j = 0; j < spielstaende.size(); j++){
                            spiel.remove(spielstaende.get(j));
                        }
                        this.ae=e;
                        spielstandLaden=i+1;
                        spiel.setWirdGeladen(true);
                        spiel.repaint();                       
                        if(isWirdGeladen()){
                            spiel.laden = true;
                            xml.ladeSpielstand(spiel, "spielstand"+(spielstandLaden));
                            spiel.laden = false;
                            spiel.setSpielstand("spielstand"+(spielstandLaden));
                            spiel.setStatusMenue(false);
                            spiel.getRaumsteuerung().ladeRaum(spiel.getSpeicherraum());
                            spiel.setWirdGeladen(false);
                            spiel.getKa().soundAbspielen(spiel.getHundebellen());
                        }
                        
                    }
                    //wenn der Spielstand leer ist
                    else{
                        dialog.setLocation((spiel.getLocation().x + 700), (spiel.getLocation().y+30 ));
                        dialog.setVisible(true);
                    }
                }

            }         
            spiel.repaint();
            spiel.getKa().setKlickenMoeglich(true);
        }

    public ArrayList<Bild> getLadeMenueBilder() {
        return ladenMenueBilder;
    }


    public Menuebutton getSpielstand01() {
        return spielstand01;
    }

    public Menuebutton getSpielstand02() {
        return spielstand02;
    }

    public Menuebutton getSpielstand03() {
        return spielstand03;
    }

    public Menuebutton getSpielstand04() {
        return spielstand04;
    }

    public ArrayList<Menuebutton> getSpielstaende() {
        return spielstaende;
    }

    public ActionEvent getAe() {
        return ae;
    }

    public void setAe(ActionEvent ae) {
        this.ae = ae;
    } 

    public void setSpielstaende(ArrayList<Menuebutton> spielstaende) {
        this.spielstaende = spielstaende;
    }

    public boolean isWirdGeladen() {
        return wirdGeladen;
    }

    public void setWirdGeladen(boolean wirdGeladen) {
        this.wirdGeladen = wirdGeladen;
    }

    public SpielLoop getSpiel() {
        return spiel;
    }

    
    



}
