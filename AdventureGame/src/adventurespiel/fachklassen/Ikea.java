
package adventurespiel.fachklassen;

import adventurespiel.steuerung.Bild;
import java.awt.Polygon;
import java.io.File;
import java.util.ArrayList;
/**
 *
 * @author Alena
 *
 *
 */
public class Ikea extends Ding

{
    private ArrayList<String> hinweistext;
    private boolean istVoll;
    private Interaktionsmoeglichkeit interaktionsmoeglichkeit;
    private Raum raum;
    private String quelleLeerBild;
    ArrayList<Gegenstand> gegenstaende = new ArrayList<Gegenstand>();
    private File infosound;
     /**
     *
     * überladener Konstruktor für Klasse Raum
     *
     * @param nameIkea: Name des Ikeas
     * @param infoIkea: Beschreibung
     * @param Koordinaten X- und Y-Koordinaten des Ikea
     * @param hinweistext: text, der asugegeben wird, wenn man auf Ikea klickt
     * @param istVoll: die boolische Variable, ob Ikea Gegenstände enthält oder leer ist
     */

    public Ikea(String name, Koordinaten koordinaten, ArrayList<String> hinweistext, boolean istVoll, int breite, int hoehe, String quelleRaumbild, ArrayList<Gegenstand> gegenstaende, boolean istSichtbar, Polygon flaeche, String quelleLeerBild, File sammelSound, File infosound)

    {
        super(name, breite, hoehe, koordinaten, quelleRaumbild, istSichtbar, flaeche, sammelSound);
        this.hinweistext = hinweistext;
        this.istVoll = istVoll;
        this.gegenstaende = gegenstaende;
        this.setRaumbild(new Bild(this));
        this.quelleLeerBild = quelleLeerBild;
        this.infosound=infosound;
    }

    /**
     *
     * get- Methoden für Klasse Ikea
     *
     * übergibt einen Wert von der Variable
     *
     * @return
     */
    public ArrayList<String> getHinweistext ()

    {
        return hinweistext;
    }

    public boolean getIstVoll ()

    {
        return istVoll;
    }
    public Raum getRaum ()

    {
        return raum;
    }


    public ArrayList<Gegenstand> getGegenstaende() {
        return gegenstaende;
    }

     public Interaktionsmoeglichkeit getInteraktionsmoeglichkeit ()

    {
        return interaktionsmoeglichkeit;
    }



    public String getQuelleLeerBild() {
        return quelleLeerBild;
    }

    public File getInfosound() {
        return infosound;
    }


     /**
     *
     * set- Methoden für Klasse Ikea
     *
     * setzt einen Wert für Varialbe ein
     *
     */
     public void setHinweistext(ArrayList<String> hinweistext)

    {
        this.hinweistext = hinweistext;
    }

     public void setIstVoll(boolean istVoll)

    {
        this.istVoll = istVoll;
    }

    public void setInteraktionsmoeglichkeit(Interaktionsmoeglichkeit interaktionsmoeglichkeit)
    {
	this.interaktionsmoeglichkeit = interaktionsmoeglichkeit;
    }

    public void setRaum(Raum raum)

    {
        this.raum = raum;
    }


    public void setGegenstaende(ArrayList<Gegenstand> gegenstaende) {
        this.gegenstaende = gegenstaende;
    }

 

    public void setQuelleLeerBild(String quelleLeerBild) {
        this.quelleLeerBild = quelleLeerBild;
    }

    public void setRaumbild(String quelleRaumbild) {
        this.setQuelleRaumbild(quelleRaumbild);
        super.setRaumbild(new Bild(this));
    }

    public void setInfosound(File infosound) {
        this.infosound = infosound;
    }
    
}

    