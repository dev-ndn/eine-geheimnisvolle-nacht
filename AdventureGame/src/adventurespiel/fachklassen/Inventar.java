//Inventar
package adventurespiel.fachklassen;

import java.util.ArrayList;
/**
 * @author Jessi
 */
public class Inventar {
    //Variablen
    private Spielfigur figur;
    //ArrayList fuer alle Gegenstaende, die sich in Inventar befinden
    private ArrayList<Gegenstand> gegenstaende = new ArrayList<Gegenstand>();
     /**
     * @param figur             Spielfigur, zu der das Inventar gehoert
     * @param gegenstaende      Liste aller enthaltenen Gegenstaende
     */

    //ueberladener Konstruktor der Inventarklasse, uebergebene Parameter werden den Klassenvariablen zugewiesen
    public Inventar(Spielfigur figur){
        this.figur = figur;
    }

    //liefert die zugehoerige Figur vom Typ Spielfigur zurueck
    public Spielfigur getFigur(){
        return figur;
    }

    //liefert die gesamte, aus Gegenstand bestehende ArrayList zurueck
    public ArrayList<Gegenstand> getGegenstaende(){
        return gegenstaende;
    }

    //setzt die Spielfigur
    public void setFigur(Spielfigur figur){
        this.figur = figur;
    }

    //setzt die ArrayList gegenstaende auf die uebergebene ArrayList
    public void setGegenstaende(ArrayList<Gegenstand> gegenstaende){
        this.gegenstaende = gegenstaende;
    }

    //fuegt einen Gegenstand zur ArrayList hinzu
    public void addGegenstand(Gegenstand g){
        gegenstaende.add(g);
    }

    //entfernt den Gegenstand aus der ArrayList
    public void removeGegenstand(Gegenstand g){
        gegenstaende.remove(g);
    }
}

