/*
 * Hintergrundmusik
 * musikthread fuer hintergrundmusik. wird im spielloop ausgefuehrt
 */

package adventurespiel.fachklassen;

import adventurespiel.steuerung.SpielLoop;
import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;

/**
 *
 * @author Simon Jokuschies
 */
public class Hintergrundmusik implements Runnable {

    SpielLoop spiel;
    public File sound01 = new File( "src/sounds/hgmusik.wav" );
    boolean dateiGefunden=true;
    AudioClip sound;

    public Hintergrundmusik(SpielLoop spiel){

       /**
        * @param spiel spiel auf das verwiesen wird
        * @param sound01 sounddatei die abgespielt wird
        * @param dateiGefunden boolean fuer abfrage, ob datei beimm laden gefunden wurde
        * @param sound audioclip, in dem sound01 geladen werden muss
        **/

        this.spiel = spiel;
    }

    public void run(){
        
    }

    public void start(){

    }

    public void soundLoopen(File loopinput){
         if(spiel.getHintergrundmusikAbspielen()){
            try {
                soundStop();
                sound = Applet.newAudioClip(loopinput.toURI().toURL() );
                sound.loop();
                dateiGefunden = loopinput.exists();
                     if(dateiGefunden==false){
                        System.out.println("Sound " + loopinput +  " konnte nicht gefunden werden.");
                     }
                     else{

                     }
            }
            catch(Exception e){
                e.printStackTrace();
            }
         }
    }

    public void soundStop(){
        if(sound!=null)sound.stop();
    }

    public void soundLoop(AudioClip audio){
        sound = audio;
        soundStop();
        if(audio!=null && spiel.getHintergrundmusikAbspielen())sound.loop();
    }

}
