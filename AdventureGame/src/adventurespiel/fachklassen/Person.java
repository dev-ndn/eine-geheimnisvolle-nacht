//Person
package adventurespiel.fachklassen;

import adventurespiel.steuerung.Bild;
import java.awt.Polygon;
import java.io.File;
import java.util.ArrayList;
/**
 * @author Jessi
 */
public class Person extends Ikea{
    //Variablen
    private String vollText;
    private String leerText;
    private ArrayList<File>sounds = new ArrayList<File>();
    /**
     * @param nameIkea
     * @param infoIkea
     * @param ikeaKoordinaten
     * @param hinweistext
     * @param istVoll
     * @param bild
     * @param vollText
     * @param leerText
     */

    public Person(String nameIkea, Koordinaten ikeaKoordinaten, ArrayList<String> hinweistext, boolean istVoll,  int breite, int hoehe, String quelle,
            ArrayList<Gegenstand> gegenstaende, String vollText, String leerText, boolean istSichtbar, Polygon flaeche, String quelleLeerBild, File sammelSound, ArrayList<File>sounds)
    {
        super(nameIkea, ikeaKoordinaten, hinweistext, istVoll,  breite, hoehe,quelle, gegenstaende, istSichtbar, flaeche, quelleLeerBild, sammelSound, null); // Hinweistext Array fuer Geschwafel vom Butler
        this.vollText = vollText; //fuer Personen wie die Gruselbraut
        this.leerText = leerText;
        this.sounds = sounds;
        super.setRaumbild(new Bild(this));
    }

    //get- Methoden
    public String getVollText() {
        return vollText;
    }

    public String getLeerText() {
        return leerText;
    }

    public ArrayList<File> getSounds() {
        return sounds;
    }

    //set- Methoden
    public void setVollText(String vollText) {
        this.vollText = vollText;
    }

    public void setLeerText(String leerText) {
        this.leerText = leerText;
    }

    public void setSounds(ArrayList<File> sounds) {
        this.sounds = sounds;
    }
    
    public void setRaumbild(String quelleRaumbild) {
        this.setQuelleRaumbild(quelleRaumbild);
        super.setRaumbild(new Bild(this));
    }
}

