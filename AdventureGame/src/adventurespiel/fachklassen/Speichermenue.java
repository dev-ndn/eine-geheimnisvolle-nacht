package adventurespiel.fachklassen;

import adventurespiel.GUI.DialogSpeichern;
import adventurespiel.steuerung.Bild;
import adventurespiel.steuerung.SpielLoop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author Simon
 */
public class Speichermenue implements ActionListener{


    Menuebutton spielstand01 = new Menuebutton("Leer","1",600,150, 360,70, "src/Bilder/menues/spielSpeichernMenue/hintergrundSpeicherbutton.jpg",20,"gelb");
    Menuebutton spielstand02 = new Menuebutton("Leer","2",600,250, 360,70,"src/Bilder/menues/spielSpeichernMenue/hintergrundSpeicherbutton.jpg",20,"gelb");
    Menuebutton spielstand03 = new Menuebutton("Leer","3",600,350, 360,70,"src/Bilder/menues/spielSpeichernMenue/hintergrundSpeicherbutton.jpg",20,"gelb");
    Menuebutton spielstand04 = new Menuebutton("Leer","4",600,450, 360,70,"src/Bilder/menues/spielSpeichernMenue/hintergrundSpeicherbutton.jpg",20,"gelb");
    Menuebutton gewaehlterButton = new Menuebutton("","",0,0,0,0,null,0,"gelb");  //diesem Button wird übergeben, welcher Button gedrückt wurde
    private ArrayList<Menuebutton> spielstaende = new ArrayList<Menuebutton>();

    private ArrayList<Bild> speichermenueBilder = new ArrayList<Bild>(); 
    Bild hintergrund, zurueck;
    DialogSpeichern dialog;
    SpielLoop spiel;
 
    public Speichermenue(SpielLoop spiel){
        this.spiel = spiel;
        hintergrund= new Bild(new Koordinaten(6,26),"src/Bilder/menues/spielSpeichernMenue/Hintergrund.jpg",1024, 768, "hintergrundSpeilSpeichern");
        zurueck = new Bild(new Koordinaten(740,620),"src/Bilder/menues/optionen/btnZurueck.png", 166, 34, "spielSpeichernMenueZurueck");
        dialog = new DialogSpeichern(this);
        speichermenueBilder.add(hintergrund);
        speichermenueBilder.add(zurueck);
        spielstaende.add(spielstand01);
        spielstaende.add(spielstand02);
        spielstaende.add(spielstand03);
        spielstaende.add(spielstand04);

        for (int i = 0; i < spielstaende.size(); i++) {
            spielstaende.get(i).addActionListener(this);
        }
    }

    public void actionPerformed(ActionEvent e) {                

        System.out.println(spielstand01.getName());
            if(e.getSource()==spielstand01){
                dialog.setLocation((spiel.getLocation().x + 700), (spiel.getLocation().y+30 ));
                dialog.setVisible(true);
                gewaehlterButton=spielstand01;
                
            }
            
            if(e.getSource()==spielstand02){                
                dialog.setLocation((spiel.getLocation().x + 700), (spiel.getLocation().y+30 ));
                dialog.setVisible(true);
                gewaehlterButton=spielstand02;
                
            }
            
            if(e.getSource()==spielstand03){ 
                dialog.setLocation((spiel.getLocation().x + 700), (spiel.getLocation().y+30 ));
                dialog.setVisible(true);
                gewaehlterButton=spielstand03;
               
            }
            
            if(e.getSource()==spielstand04){ 
                dialog.setLocation((spiel.getLocation().x + 700), (spiel.getLocation().y+30 ));
                dialog.setVisible(true);
                gewaehlterButton=spielstand04;
            
            }



            
        }

    

    public ArrayList<Bild> getSpeichermenueBilder() {
        return speichermenueBilder;
    }

   
    public Menuebutton getSpielstand01() {
        return spielstand01;
    }

    public Menuebutton getSpielstand02() {
        return spielstand02;
    }

    public Menuebutton getSpielstand03() {
        return spielstand03;
    }

    public Menuebutton getSpielstand04() {
        return spielstand04;
    }

    public Menuebutton getGewaehlterButton() {
        return gewaehlterButton;
    }

    public SpielLoop getSpiel() {
        return spiel;
    }

    

    public void setGewaehlterButton(Menuebutton gewaehlterButton) {
        this.gewaehlterButton = gewaehlterButton;
    }

    /**
     * @return the spielstaende
     */
    public ArrayList<Menuebutton> getSpielstaende() {
        return spielstaende;
    }

    /**
     * @param spielstaende the spielstaende to set
     */
    public void setSpielstaende(ArrayList<Menuebutton> spielstaende) {
        this.spielstaende = spielstaende;
    }


}
