package adventurespiel.fachklassen;

import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.util.ArrayList;

/**
 * @author Alisa 18.10.09
 */
public class Spielfigur extends Panel {

    private String name;
    private Image aussehen;
    private Koordinaten spielerKoordinaten;
    private Raum raum;
    private Inventar inventar;
    public ArrayList<Image> bewegungRechts = new ArrayList<Image>();
    public ArrayList<Image> bewegungLinks = new ArrayList<Image>();
    public ArrayList<Image> bewegungOben = new ArrayList<Image>();
    public ArrayList<Image> bewegungUnten = new ArrayList<Image>();
    Toolkit tk = getToolkit();

    /**
     * @param name Name der Spielfigur
     * @param aussehen Bild der Spielfigur
     * @param spielerKoordinaten X- und Y-Koordinaten der Spielfigur
     * @param raum Raum in der sich die Spielfigur befindet
     * @param inventar Inventar, das zur Spielfigur gehoert
     */
    public Spielfigur(String name, Image aussehen, Koordinaten spielerKoordinaten, Inventar inventar) {
        this.name = name;
        this.aussehen = aussehen;
        this.spielerKoordinaten = spielerKoordinaten;
        this.inventar = inventar;

        //Spielfigurbilder in arraylists laden
        this.bewegungRechts.add(tk.getImage("src/Bilder/Spielfigur/rechts_01.PNG"));
        this.bewegungRechts.add(tk.getImage("src/Bilder/Spielfigur/rechts_02.PNG"));
        this.bewegungRechts.add(tk.getImage("src/Bilder/Spielfigur/rechts_03.PNG"));
        this.bewegungRechts.add(tk.getImage("src/Bilder/Spielfigur/rechts_04.PNG"));
        this.bewegungLinks.add(tk.getImage("src/Bilder/Spielfigur/links_01.PNG"));
        this.bewegungLinks.add(tk.getImage("src/Bilder/Spielfigur/links_02.PNG"));
        this.bewegungLinks.add(tk.getImage("src/Bilder/Spielfigur/links_03.PNG"));
        this.bewegungLinks.add(tk.getImage("src/Bilder/Spielfigur/links_04.PNG"));
        this.bewegungOben.add(tk.getImage("src/Bilder/Spielfigur/oben_01.PNG"));
        this.bewegungOben.add(tk.getImage("src/Bilder/Spielfigur/oben_02.PNG"));
        this.bewegungOben.add(tk.getImage("src/Bilder/Spielfigur/oben_03.PNG"));
        this.bewegungUnten.add(tk.getImage("src/Bilder/Spielfigur/unten_01.PNG"));
        this.bewegungUnten.add(tk.getImage("src/Bilder/Spielfigur/unten_02.PNG"));
        this.bewegungUnten.add(tk.getImage("src/Bilder/Spielfigur/unten_03.PNG"));
    }

    
    //Liefert einen Absatz auf der Konsole mit allen Werten der Attribute des aufrufenden Objekts.
    @Override
    public String toString() {
        return "Name: " + this.name + "\n" +
                "Koordinaten: " + this.spielerKoordinaten;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAussehen(Image aussehen) {
        this.aussehen = aussehen;
    }

    public void setSpielerKoordinate(Koordinaten spielerKoordinaten) {
        this.spielerKoordinaten = spielerKoordinaten;
    }

    public void setRaum(Raum raum) {
        this.raum = raum;
    }

    public void setInventar(Inventar inventar) {
        this.inventar = inventar;
    }

    public void setBewegungRechts(ArrayList<Image> bewegungRechts) {
        this.bewegungRechts = bewegungRechts;
    }

    public void setBewegungLinks(ArrayList<Image> bewegungLinks) {
        this.bewegungLinks = bewegungLinks;
    }

    public void setBewegungOben(ArrayList<Image> bewegungOben) {
        this.bewegungOben = bewegungOben;
    }

    public void setBewegungUnten(ArrayList<Image> bewegungUnten) {
        this.bewegungUnten = bewegungUnten;
    }

    public String getName() {
        return name;
    }

    public Image getAussehen() {
        return aussehen;
    }

    public Koordinaten getSpielerKoordinate() {
        return this.spielerKoordinaten;
    }

    public Raum getRaum() {
        return raum;
    }

    public Inventar getInventar() {
        return inventar;
    }

    public ArrayList<Image> getBewegungRechts() {
        return this.bewegungRechts;
    }

    public ArrayList<Image> getBewegungLinks() {
        return this.bewegungLinks;
    }

    public ArrayList<Image> getBewegungOben() {
        return this.bewegungOben;
    }

    public ArrayList<Image> getBewegungUnten() {
        return this.bewegungUnten;
    }
    
}
