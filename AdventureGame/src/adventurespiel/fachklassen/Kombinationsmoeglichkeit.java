//Kombinationsmoeglichkeit
package adventurespiel.fachklassen;

import java.io.File;
/**
 * @author Mounir
 * @author Jessica
 */
public class Kombinationsmoeglichkeit {
    //Variablen
    private Gegenstand[] gegenstand;
    private String kombinationstext;
    private Gegenstand neuerGegenstand;
    private File sound;

    //ueberladener Konstruktor
    public Kombinationsmoeglichkeit(Gegenstand gegenstand0, Gegenstand gegenstand1, String kombinationstext, Gegenstand neuerGegenstand){
        this.gegenstand = new Gegenstand[2];
	this.gegenstand[0] = gegenstand0;
	this.gegenstand[1] = gegenstand1;
	this.kombinationstext = kombinationstext;
        this.neuerGegenstand = neuerGegenstand;
    }

    public Kombinationsmoeglichkeit(String kombinationstext, File sound) {
        this.kombinationstext = kombinationstext;
        this.gegenstand = new Gegenstand[2];
        this.sound = sound;
    }

    //Methode prueft, ob Gegenstaende miteinander kombinierbar sind
    public boolean istKombinierbar(Gegenstand gegenstand0, Gegenstand gegenstand1){
	boolean result = false;
	if(gegenstand0 == gegenstand[0]){
	    if(gegenstand1 == gegenstand[1]){
		result = true;
	    }
	}
	else if(gegenstand0 == gegenstand[1]){
	    if(gegenstand1 == gegenstand[0]){
		result = true;
	    }
	}
	return result;
    }

    //get- Methoden
    public Gegenstand[] getGegenstand(){
    return gegenstand;
    }

    public String getString(){
    return kombinationstext;
    }

    public Gegenstand getNeuerGegenstand(){
        return neuerGegenstand;
    }

    public String getKombinationstext() {
        return kombinationstext;
    }

    public File getSound() {
        return sound;
    }

    //set-Methoden
    public void setGegenstandArray(Gegenstand[] gegenstand){
	this.gegenstand = gegenstand;
    }

    public void setString(String kombinationstext){
	this.kombinationstext = kombinationstext;
    }

    public void setNeuerGegenstand(Gegenstand neuerGegenstand) {
        this.neuerGegenstand = neuerGegenstand;
    }

    public void setSound(File sound) {
        this.sound = sound;
    }
}

