//Koordinaten
package adventurespiel.fachklassen;
/**
 * @author Jessica Ullrich
 * @version 1   05.10.2009
 */
public class Koordinaten{
    //Variablen
    private int xKoordinate, yKoordinate;
    /**
     * @param xKoordinate
     * @param yKoordinate
     *
     * ueberladener Konstruktor fuer Klasse Koordinaten
     */
    public Koordinaten (int xKoordinate, int yKoordinate){
        this.xKoordinate = xKoordinate;
        this.yKoordinate = yKoordinate;
    }

    //get- Methoden
    public int getXKoordinate(){
        return xKoordinate;
    }

    public int getYKoordinate(){
        return yKoordinate;
    }

    //set- Methoden
    public void setXKoordinate(int xKoordinate){
       this.xKoordinate = xKoordinate;
    }

    public void setYKoordinate(int yKoordinate){
       this.yKoordinate = yKoordinate;
    }
}
