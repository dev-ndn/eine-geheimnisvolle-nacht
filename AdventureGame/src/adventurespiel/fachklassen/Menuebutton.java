package adventurespiel.fachklassen;

import java.awt.Button;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

/**
 *
 * @author Simon
 */
public class Menuebutton extends Button {

        String aufschrift, buttonBildquelle, index;
        int x, y, breite, hoehe, versatz;
        Image buttonImage;
        Toolkit bTk = getToolkit();
        Font schriftTextleiste = new Font("Arial", Font.BOLD, 34);
        Color braun = new Color(125,77,39);
        Color gelb = new Color(223,242,161);
        String buttonFarbe;
        Cursor aufheben = Toolkit.getDefaultToolkit().createCustomCursor(
            bTk.getImage("src/AllgemeinBilder/cursor0.png"),
            new Point(0, 0), "Cursor");

        public Menuebutton(String aufschrift, String index, int x, int y, int breite, int hoehe, String buttonBildquelle, int versatz, String buttonFarbe){

            this.aufschrift=aufschrift;
            this.index=index;
            this.x=x;
            this.y=y;
            this.breite=breite;
            this.hoehe=hoehe;
            this.setBounds(x, y, breite, hoehe);
            this.buttonBildquelle=buttonBildquelle;
            this.versatz=versatz;
            this.buttonFarbe=buttonFarbe;
            this.setBackground(new Color(125,77,39));
            this.buttonImage=bTk.getImage(buttonBildquelle);
            setFont(schriftTextleiste);
            this.setCursor(aufheben);
            if(buttonFarbe.equals("gelb")){
                 this.setBackground(new Color(223,242,161));//125,77,39
            }
            if (buttonFarbe.equals("braun")){
                this.setBackground(new Color(125,77,39));
            }
            else{
                this.setBackground(Color.white);
            }
           
        }

    @Override
    public void paint(Graphics g) {
        g.drawImage(buttonImage, 0,0,this);
        g.drawString(aufschrift, versatz, 50);
        
    }

    public String getAufschrift() {
        return aufschrift;
    }

    public void setAufschrift(String aufschrift) {
        this.aufschrift = aufschrift;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    

    

}