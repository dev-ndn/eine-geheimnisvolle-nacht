//Gegenstand
package adventurespiel.fachklassen;

import adventurespiel.steuerung.Bild;
import java.awt.Polygon;
import java.io.File;

/**
 * @author Simon
 * @author Jessica
 */
public class Gegenstand extends Ding {
    //Variablen
    private String infoGegenstand;
    private Interaktionsmoeglichkeit interaktionsmoeglichkeit;
    private Kombinationsmoeglichkeit kombinationsmoeglichkeit;
    private Inventar inventar;
    private String quelleInventar;
    private Bild inventarbild;
    private File infosound;

    /**
     * überladener Konstruktor für Klasse Gegenstand
     *
     * @param name Name des Gegenstands
     * @param infoIkea Beschreibung des Gegenstandes
     * @param Koordinaten X- und Y- Koordinaten des Gegenstandes
     */

    
    public Gegenstand(String name, String infoGegenstand, Koordinaten koordinaten, Inventar inventar, int breite, int hoehe, String quelleRaumbild, String quelleInventar, boolean istSichtbar, Polygon flaeche, File sammelSound, File infosound)
    {
        super(name, breite, hoehe, koordinaten, quelleRaumbild, istSichtbar, flaeche, sammelSound);
        this.infoGegenstand = infoGegenstand;
        this.inventar = inventar;
        this.quelleInventar = quelleInventar;
        this.setRaumbild(new Bild(this));
        this.setInventarbild(new Bild(this));
        this.infosound=infosound;
    }

    //get- Methoden
    public Bild getInventarbild() {
        return inventarbild;
    }

    public String  getInfoGegenstand() {
        return infoGegenstand;
    }

    public Inventar getInventar()
    {
        return this.inventar;
    }

    public String getQuelleInventar() {
        return quelleInventar;
    }

    public Interaktionsmoeglichkeit getInteraktionsmoeglichkeit() {
        return interaktionsmoeglichkeit;
    }

    public Kombinationsmoeglichkeit getKombinationsmoeglichkeit() {
        return kombinationsmoeglichkeit;
    }


    public File getInfosound() {
        return infosound;
    }

    public void setInfosound(File infosound) {
        this.infosound = infosound;
    }

    

    public void setKombinationsmoeglichkeit(Kombinationsmoeglichkeit kombinationsmoeglichkeit)
    {
        this.kombinationsmoeglichkeit = kombinationsmoeglichkeit;
    }

    //set- Methoden
    public void setInventarbild(Bild inventarbild) {
        this.inventarbild = inventarbild;

    }

    public void setInfoGegenstand(String infoGegenstand) {
        this.infoGegenstand = infoGegenstand;
    }
   
    public void setQuelleInventar(String quelleInventar) {
        this.quelleInventar = quelleInventar;
    }


    public void setInventar (Inventar inventar){
        this.inventar = inventar;
    }

    /**
     * prüft, ob Gegenstand und Ikea interagieren können
     *
     * @param ikea Ikea, mit der der Gegenstand interagieren soll
     * @return interaktion moeglich: Interaktionstext wird ausgegeben und Gegenstand aus Inventar gestrichen
     *         interaktion nicht moeglich: Hinweis wird angezeigt
     */
    public void interagiere(Ikea ikea){
	if( this.interaktionsmoeglichkeit != null){
	    if( this.interaktionsmoeglichkeit.getIkea() == ikea){
                inventar.removeGegenstand(this);
	    }
	}
    }

    /**
     * setzen der Interaktionsmöglichkeit für ein Paar
     *
     * @param ikea Ikea, mit der interagiert werden soll
     */
    public void interagiertMit(Ikea ikea, String text, Gegenstand neuerGegenstand){
	this.setInteraktionsmoeglichkeit(new Interaktionsmoeglichkeit(ikea, this, text, false, neuerGegenstand));
	ikea.setInteraktionsmoeglichkeit(this.interaktionsmoeglichkeit);
    }

    /**
     * prüft die Kombinierbarkeit von 2 Gegenständen
     *
     * @param gegenstand Gegenstand mit dem kombiniert wird
     * @return true, wenn Gegenstände kombinierbar sind
     */
    public void kombiniere(Gegenstand gegenstand){
	if( this.kombinationsmoeglichkeit != null){
	    if( this.kombinationsmoeglichkeit.istKombinierbar(this, gegenstand)){
		inventar.removeGegenstand(this);
                inventar.removeGegenstand(gegenstand);
                inventar.addGegenstand(this.kombinationsmoeglichkeit.getNeuerGegenstand());
	    }
	}
    }
    /**
     * erzeugt ein Kombinationsmöglichkeitsobjekt für 2 Gegenstände
     * @param gegenstand Gegenstand, mit dem kombiniert werden kann
     */
    public void kombiniertMit(Gegenstand gegenstand, Gegenstand neuerGegenstand, String text){
	this.kombinationsmoeglichkeit = new Kombinationsmoeglichkeit(gegenstand, this, text, neuerGegenstand);
	gegenstand.setKombinationsmoeglichkeit(kombinationsmoeglichkeit);
    }

    /**
     * @param interaktionsmoeglichkeit the interaktionsmoeglichkeit to set
     */
    public void setInteraktionsmoeglichkeit(Interaktionsmoeglichkeit interaktionsmoeglichkeit) {
        this.interaktionsmoeglichkeit = interaktionsmoeglichkeit;
    }
}
