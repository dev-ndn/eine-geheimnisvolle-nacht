//Tuer
package adventurespiel.fachklassen;

import adventurespiel.steuerung.Bild;
import java.awt.Polygon;
import java.io.File;
import java.util.ArrayList;
/**
 * @author Jessi 17.11.09
 */
public class Tuer extends Ikea {
    //Variablen
    private String wunschort;
    private Koordinaten figurposition;
    private String startbild;
    /**
     * @param nameIkea
     * @param infoIkea
     * @param ikeaKoordinaten
     * @param hinweistext
     * @param istVoll
     * @param bild
     * @param wunschort
     * @param neu
     */

    //ueberladener Konstruktor
    public Tuer(String nameIkea, Koordinaten ikeaKoordinaten, ArrayList<String> hinweistext, boolean istVoll,
            int breite,int hoehe,  String quelle, ArrayList<Gegenstand> gegenstaende, String wunschort,
            boolean istSichtbar, Polygon flaeche, Koordinaten figurposition, String startbild, File sammelSound)
    {
        super(nameIkea, ikeaKoordinaten, null, istVoll, breite, hoehe,  quelle, gegenstaende, istSichtbar, flaeche, null, sammelSound, null);
        this.wunschort = wunschort;
        this.figurposition= figurposition;
        this.startbild = startbild;
        super.setRaumbild(new Bild(this));
    }

    //get- Methoden
    public String getWunschort() {
        return wunschort;
    }

    public Koordinaten getFigurposition() {
        return figurposition;
    }

    public String getStartbild() {
        return startbild;
    }

    //set- Methoden
    public void setWunschort(String wunschort) {
        this.wunschort = wunschort;
    }

    public void setFigurposition(Koordinaten figurposition) {
        this.figurposition = figurposition;
    }
  
    public void setStartbild(String startbild) {
        this.startbild = startbild;
    }
}
