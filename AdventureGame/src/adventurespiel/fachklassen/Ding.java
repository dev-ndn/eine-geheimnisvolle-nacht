
package adventurespiel.fachklassen;

import adventurespiel.steuerung.Bild;
import java.awt.Polygon;
import java.io.File;

/**
 *
 * @author e.Wagner
 */
public class Ding

{
    private String name;
    private int breite, hoehe; 
    private Koordinaten koordinaten;
    private Bild raumbild;
    private String quelleRaumbild;
    private boolean istSichtbar;
    private Polygon flaeche;
    private File sammelSound;

    public Ding (String name, int breite, int hoehe, Koordinaten koordinaten, String quelleRaumbild, boolean istSichtbar, Polygon flaeche, File sammelSound)

    {
        this.name=name;
        this.breite=breite;
        this.hoehe=hoehe;
        this.koordinaten=koordinaten;
        this.quelleRaumbild=quelleRaumbild;
        this.istSichtbar=istSichtbar;
        this.flaeche = flaeche;
        this.sammelSound = sammelSound;
    }


    @Override
    public String toString(){
	return "Name: "+ this.name + "\n" +
		"Koordinaten: " + this.koordinaten;
    }
     public Bild getRaumbild() {

        return raumbild;
    }

    public void setRaumbild(Bild bild) {
        this.raumbild = bild;

    } 
    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;

    }
    public int getBreite() {

        return breite;
    }

    public void setBreite(int breite) {
        this.breite = breite;

    }
       public int getHoehe() {

        return hoehe;
    }

    public void setHoehe(int hoehe) {
        this.hoehe = hoehe;

    }
       public Koordinaten getKoordinaten() {

        return koordinaten;
    }

    public void setKoordinaten(Koordinaten koordinaten) {
        this.koordinaten = koordinaten;

    }
     public String getQuelleRaumbild() {
        return quelleRaumbild;
    }

    public void setQuelleRaumbild(String quelleRaumbild) {
        this.quelleRaumbild = quelleRaumbild;
    }

    public boolean getIstSichtbar() {
        return istSichtbar;
    }

    public void setIstSichtbar(boolean istSichtbar) {
        this.istSichtbar = istSichtbar;
    }

    public Polygon getFlaeche() {
        return flaeche;
    }

    public void setFlaeche(Polygon flaeche) {
        this.flaeche = flaeche;
    }
    public File getSammelSound() {
        return sammelSound;
    }

    public void setSammelSound (File sammelSound) {
        this.sammelSound = sammelSound;
    }
    
}



