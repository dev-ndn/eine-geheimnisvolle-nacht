//Klasse Raum
package adventurespiel.fachklassen;

import adventurespiel.steuerung.Bild;
import java.io.File;
import java.util.ArrayList;
/**
 * @author Jessica Ullrich
 * @version 1   02.10.2009
 */
public class Raum
{
    //Attribute
    private String raumName;
    private Koordinaten koordinaten;
    private int hoehe, breite;
    private Spielfigur figur;
    //Liste aller Ikeas in einem Raum
    private ArrayList<Ikea> ikea;
    //Liste aller Gegenstaende in einem Raum
    private ArrayList<Gegenstand> gegenstand;
    private Bild bild;
    private String quelle;
    //Liste aller Tueren in einem Raum
    private ArrayList<Tuer> tuer;
    //Liste aller Personen in einem Raum
    private ArrayList<Person> person;
    private Wegfindung wegfindung;
    private File sound;
    
    //ueberladener Konstruktor fuer Klasse Raum
    public Raum (String raumName, Koordinaten koordinaten,  int breite,int hoehe,
            Spielfigur figur, ArrayList<Ikea> ikea, ArrayList<Gegenstand> gegenstand,
            ArrayList<Tuer> tuer, ArrayList<Person> person, String quelle, File sound)
    {
        this.raumName = raumName;
        this.koordinaten = koordinaten;
        this.hoehe = hoehe;
        this.breite = breite;
        this.figur = figur;
        this.ikea = ikea;
        this.gegenstand = gegenstand;
        this.quelle=quelle;
        this.tuer = tuer;
        this.person = person;
        this.sound = sound;
        this.bild =  new Bild(this);        
    }
    /**
     * get- Methoden fuer Klasse Raum
     * uebergibt einen Wert von der Variable
     */
    public String getRaumName(){
        return this.raumName;
    }

    public Koordinaten getKoordinaten(){
        return koordinaten;
    }

    public int getHoehe(){
        return this.hoehe;
    }

    public int getBreite(){
        return this.breite;
    }

    public Spielfigur getSpielfigur(){
        return this.figur;
    }

    public ArrayList<Ikea> getIkea(){
        return this.ikea;
    }

    public ArrayList<Gegenstand> getGegenstand(){
        return this.gegenstand;
    }

    public Bild getBild(){
        return this.bild;
    }

    public String getQuelle(){
        return this.quelle;
    }

    public ArrayList<Tuer> getTuer(){
        return tuer;
    }

    public ArrayList<Person> getPerson(){
        return person;
    }

    public File getSound() {
        return sound;
    }

    public Wegfindung getWegfindung() {
        return wegfindung;
    }

    /**
     * set- Methoden fuer Klasse Raum
     * setzt einen Wert fuer Varialbe ein
     */

    public void setRaumName(String raumName){
        this.raumName = raumName;
    }

    public void setKoordinaten(Koordinaten koordinaten){
        this.koordinaten = koordinaten;
    }

    public void setHoehe(int hoehe){
        this.hoehe = hoehe;
    }

    public void setBreite(int breite){
        this.breite = breite;
    }

    public void setSpielfigur(Spielfigur figur){
        this.figur = figur;
    }

    public void setIkea(ArrayList<Ikea> ikea){
        this.ikea = ikea;
    }

    public void setGegenstand(ArrayList<Gegenstand> gegenstand){
        this.gegenstand = gegenstand;
    }

    public void setBild(Bild bild){
        this.bild = bild;
    }

    public void setQuelle(String quelle){
        this.quelle = quelle;
    }

    public void setTuer(ArrayList<Tuer> tuer){
        this.tuer = tuer;
    }

    public void setPerson(ArrayList<Person> person){
        this.person = person;
    }

    public void setWegfindung(Wegfindung wegfindung) {
        this.wegfindung = wegfindung;
    }

    public void setSound(File sound) {
        this.sound = sound;
    }
}

