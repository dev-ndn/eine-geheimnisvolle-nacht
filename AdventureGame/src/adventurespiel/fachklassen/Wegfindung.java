package adventurespiel.fachklassen;

import adventurespiel.handler.XMLHandling;
import java.awt.Color;
import java.awt.Graphics;
import org.dom4j.Document;

/**
 * im raster:
 * 1 = begehbares feld
 * 0 = nicht begehbares feld
 *
 * @author Nadine
 */
public class Wegfindung
{
    public Raum raum;
//groesse der quadratischen felder
    private int hoehe;
    private int breite;
    private int startx;
    private int starty;
    private int zellengroesse;
//matrix mit 0en und 1en für die gehflaeche
    private int[][] raster;
    private int anzahlSpalten;
    private int anzahlZeilen;

    /**
     * konstruktor
     */
    public Wegfindung(int zellengroesse, int hoehe, int breite,
            int spalten, int zeilen, int[][] raster, int startx, int starty){
        this.zellengroesse = zellengroesse;
        this.breite = breite;
        this.hoehe = hoehe;
        this.anzahlSpalten = spalten;
        this.anzahlZeilen = zeilen;
        this.raster = raster;
        this.startx = startx;
        this.starty = starty;
    }

    //prueft, die entfernung zwischen 2 punkten/ 2 x-y-paaren groesser ist als 50
    public static boolean bewegungNoetig(int startx, int starty, int zielx, int ziely){
        double entfernung = Math.sqrt(Math.pow((zielx-startx),2)+Math.pow((ziely-starty),2));
        if(entfernung>50)return true;
        return false;
    }

    //ermittelt die koordinaten des feldes
    public Koordinaten ermittleFeldkoordinaten(Koordinaten koordinaten){
        int tempspalte = ((koordinaten.getXKoordinate()-getStartx())/getZellengroesse());
        int tempzeile = ((koordinaten.getYKoordinate()-getStarty())/getZellengroesse());
        return new Koordinaten((tempspalte*getZellengroesse())+getStartx(), (tempzeile*getZellengroesse())+getStarty());
    }

    //ermittelt die spalte der koordinaten
    public int ermittleFeldspalte(Koordinaten koordinaten){
        float temp =(koordinaten.getXKoordinate()-getStartx())/getZellengroesse();
        if(temp<0)return -1;
        return ((koordinaten.getXKoordinate()-getStartx())/getZellengroesse());
    }

    //ermittelt die feldspalte nur durch uebergabe von x
    public int ermittleFeldspalte(int x){
        return ermittleFeldspalte(new Koordinaten(x, 0));
    }

    //ermittelt die zeile der koordinaten
    public int ermittleFeldzeile(Koordinaten koordinaten){
        float temp =(koordinaten.getYKoordinate()-getStarty())/getZellengroesse();
        if(temp<0)return -1;
        return (koordinaten.getYKoordinate()-getStarty())/getZellengroesse();
    }

    //ermittelt die feldzeile nur durch uebergabe von y
    public int ermittleFeldzeile(int y){
        return ermittleFeldzeile(new Koordinaten(0, y));
    }

    //prueft ob der punkt innerhalb eines begehbaren feldes liegt
    public boolean isBegehbar(Koordinaten punkt){
        int tempspalte = ermittleFeldspalte(punkt);
        int tempzeile = ermittleFeldzeile(punkt);
        //wenn die ermittelte zeile und spalte in den zulaessigen groessen ist
        if(tempspalte<getAnzahlSpalten() && tempzeile<getAnzahlZeilen() &&
                tempspalte>=0 && tempzeile>=0){
           if(getRaster()[tempspalte][tempzeile] == 1)return true;
        }
        return false;
    }

    //uebergabe von x und y statt direkt koordinaten
    public boolean isBegehbar(int x, int y){
        return isBegehbar(new Koordinaten(x, y));
    }

    //ermittelt ob das naechste feld in der richtung begehbar ist
    public boolean isBegehbar(Koordinaten punkt, String richtung){
        //spalte und zeile im raster des aktuellen punkts ermitteln
        int tempspalte = ermittleFeldspalte(punkt);
        int tempzeile = ermittleFeldzeile(punkt);
        //wenn die ermittelten zeilen und spalten innerhalb der zulaessigen groessen sind
        if(tempspalte<getAnzahlSpalten() && tempzeile<getAnzahlZeilen()&&
                tempspalte-1>=0 && tempzeile-1>=0){
            //wenn die richtung links oder rechts ist
            if("links".equals(richtung)){
                //wenn das nachbarfeld im raster den wert 1 hat, gebe true zurueck
                if(getRaster()[tempspalte-1][tempzeile] == 1)return true;
            }
            if("rechts".equals(richtung)){
                if(getRaster()[tempspalte+1][tempzeile] == 1)return true;
            }
        }
        return false;
    }

    /**
     * zeichnet an der entsprechenden stelle im raster ein farbiges rechteck
     *
     * @param spalte zu findende spalte im raster
     * @param zeile zu findende zeile im raster
     * @param color farbe des rechtecks
     */
    public void zeichneRechteck(Graphics g, int spalte, int zeile, Color color){
        g.setColor(color);
        g.drawRect((getZellengroesse()*spalte)+getStartx(), (getZellengroesse()*zeile)+getStarty(),getZellengroesse(), getZellengroesse());
    }

    /**
     * zeichnet an der entsprechenden stelle im raster ein farbiges rechteck
     *
     * @param spalte zu findende spalte im raster
     * @param zeile zu findende zeile im raster
     * @param color farbe des rechtecks
     */
    public void schreibeStatus(Graphics g, int spalte, int zeile){
        g.setColor(Color.LIGHT_GRAY);
        g.drawString(String.valueOf(getRaster()[spalte][zeile]),((getZellengroesse())*spalte)+getStartx()+6, (getZellengroesse()*zeile)+getStarty()+12);
    }


    /**
     * setzt den inhalt des raster auf 1en zurück
     */
    public void initGehflaeche(){

        for(int i = 0; i < (getAnzahlSpalten()); i++){
            for(int j = 0; j < (getAnzahlZeilen()); j++){
                this.getRaster()[i][j] = 1;
            }
        }
    }

    /**
     * zeichnet jede stelle im raster, die begehbar sein soll(den wert 1 hat) grün
     */
    public void zeichneGehflaeche(Graphics g){

        for(int i = 0; i < getAnzahlSpalten(); i++){
            for(int j = 0; j < getAnzahlZeilen(); j++){
                if(this.getRaster()[i][j] == 1) zeichneRechteck(g, i, j, Color.green);
                schreibeStatus(g, i, j);
            }
        }
    }

    //liest ein int[][] aus einer xml ein und speichert es als raster
    public void ladeGehflaeche(String pfad){
        Document doc = XMLHandling.parse("src/xmls/default/gehflaeche"+pfad);
        int zaehler = 0;
        for(int i = 0; i < (getAnzahlSpalten()); i++){
            for(int j = 0; j < (getAnzahlZeilen()); j++){
                if(doc.selectSingleNode("//feld"+zaehler+"/@status") == null){
                    this.getRaster()[i][j] =1;
                }else {
                    this.getRaster()[i][j] = Integer.parseInt(doc.selectSingleNode("//feld"+zaehler+"/@status").getText());
                }
                 zaehler++;
            }
        }
        rasterAufKonsole();
    }

    //gibt das raster aus 0en und 1en auf der konsole aus
    public void rasterAufKonsole(){
        for (int i = 0; i < getAnzahlZeilen(); i++) {
            System.out.println("\t");
            for (int j = 0; j < getAnzahlSpalten(); j++) {
                System.out.print(getRaster()[j][i]+" ");
            }
        }
    }

    /**
     * @return the raster
     */
    public int[][] getRaster() {
        return this.raster;
    }

    /**
     * @param raster the raster to set
     */
    public void setRaster(int[][] raster) {
        this.setRaster(raster);
    }

    /**
     * @return the hoehe
     */
    public int getHoehe() {
        return this.hoehe;
    }

    /**
     * @param hoehe the hoehe to set
     */
    public void setHoehe(int hoehe) {
        this.hoehe = hoehe;
    }

    /**
     * @return the breite
     */
    public int getBreite() {
        return this.breite;
    }

    /**
     * @param breite the breite to set
     */
    public void setBreite(int breite) {
        this.breite = breite;
    }

    /**
     * @return the startx
     */
    public int getStartx() {
        return this.startx;
    }

    /**
     * @param startx the startx to set
     */
    public void setStartx(int startx) {
        this.startx = startx;
    }

    /**
     * @return the starty
     */
    public int getStarty() {
        return this.starty;
    }

    /**
     * @param starty the starty to set
     */
    public void setStarty(int starty) {
        this.starty = starty;
    }

    /**
     * @return the anzahlSpalten
     */
    public int getAnzahlSpalten() {
        return this.anzahlSpalten;
    }

    /**
     * @param anzahlSpalten the anzahlSpalten to set
     */
    public void setAnzahlSpalten(int anzahlSpalten) {
        this.anzahlSpalten = anzahlSpalten;
    }

    /**
     * @return the anzahlZeilen
     */
    public int getAnzahlZeilen() {
        return this.anzahlZeilen;
    }

    /**
     * @param anzahlZeilen the anzahlZeilen to set
     */
    public void setAnzahlZeilen(int anzahlZeilen) {
        this.anzahlZeilen = anzahlZeilen;
    }

    /**
     * @return the zellengroesse
     */
    public int getZellengroesse() {
        return this.zellengroesse;
    }

    /**
     * @param zellengroesse the zellengroesse to set
     */
    public void setZellengroesse(int zellengroesse) {
        this.zellengroesse = zellengroesse;
    }
}