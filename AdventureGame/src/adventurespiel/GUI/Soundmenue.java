/*
 * Soundmenue
 * menue in dem man einstellen kann, ob Hintergrundmusik
 * und andere Sounds (Sprache, Ggenstand einsameln etc.)
 * abgespielt werden soll oder nicht. 
 */

package adventurespiel.GUI;

import adventurespiel.fachklassen.Koordinaten;
import adventurespiel.fachklassen.Menuebutton;
import adventurespiel.steuerung.Bild;
import adventurespiel.steuerung.SpielLoop;
import java.awt.Button;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * @author Simon Jokuschies
 */
public class Soundmenue implements ActionListener {

     SpielLoop spiel;
     Menuebutton hgmusikSchalter = new Menuebutton("", "", 750,280,100,50,"src/Bilder/menues/sound/hgbutton.JPG",10,"braun");
     Menuebutton soundSchalter = new Menuebutton("","",750,490,100,50,"src/Bilder/menues/sound/hgbutton.JPG",10,"braun");
     Font stil1 = new Font("Arial", Font.BOLD, 20);
     Bild hintergrund, zurueck;
     private ArrayList<Bild> soundmenueBilder = new ArrayList<Bild>();

     public Soundmenue(SpielLoop spiel){

        /**
         * @param spiel spiel auf das verwiesen wird
         * @param hgmusikSchalter button zum ein/ausschalten der hintergrundmusik
         * @param soundSchalter button zum ein/ausschalten der sounds
         * @param stil1 font-darstellung1
         * @param hintergrund hintergrundbild
         * @param zurueck zurueck-button
         * @param soundmenueBilder menuebilder
         **/

         this.spiel = spiel;

         hgmusikSchalter.addActionListener(this);
         soundSchalter.addActionListener(this);

         //aufschrift der buttons bestimmen
         if(spiel.getHintergrundmusikAbspielen()){
            hgmusikSchalter.setAufschrift("an");
         }
         if(!(spiel.getHintergrundmusikAbspielen())){
            hgmusikSchalter.setAufschrift("aus");
         }

         if(spiel.getSoundsAbspielen()){
            soundSchalter.setAufschrift("an");
         }
         if(!spiel.getSoundsAbspielen()){
            soundSchalter.setAufschrift("aus");
         }

         hintergrund= new Bild(new Koordinaten(6,26),"src/Bilder/menues/sound/Hintergrund.JPG",1012, 735, "hintergrundSoundMenue");
         zurueck = new Bild(new Koordinaten(675,695),"src/Bilder/menues/optionen/btnZurueck.png", 166, 34, "SoundZurueck");
          
         soundmenueBilder.add(hintergrund);
         soundmenueBilder.add(zurueck);       
    }
  
    public void actionPerformed(ActionEvent e) {

        if (e.getSource()==hgmusikSchalter){
            spiel.getKa().soundAbspielen(spiel.getKlickSound());
            //wenn aufschrift vorher auf "an" war, dann nun auf "aus" setzen
            if (spiel.getHintergrundmusikAbspielen()){
                spiel.setHintergrundmusikAbspielen(false);
                spiel.getKa().setSoundeinstellungenGeaendert(true);
                hgmusikSchalter.setAufschrift("aus");
                hgmusikSchalter.repaint();
                return;
            }
            //wenn aufschrift vorher auf "aus" war, dann nun auf "an" setzen
            if (!spiel.getHintergrundmusikAbspielen()){
                spiel.setHintergrundmusikAbspielen(true);
                spiel.getKa().setSoundeinstellungenGeaendert(true);
                hgmusikSchalter.setAufschrift("an");
                hgmusikSchalter.repaint();
                return;
            }
        }

        if (e.getSource()==soundSchalter){
            //wenn aufschrift vorher auf "an" war, dann nun auf "aus" setzen
            spiel.getKa().soundAbspielen(spiel.getKlickSound());
            if (spiel.getSoundsAbspielen()){
                spiel.setSoundsAbspielen(false);
                soundSchalter.setAufschrift("aus");
                soundSchalter.repaint();
                return;
            }
            //wenn aufschrift vorher auf "aus" war, dann nun auf "an" setzen
            if (!spiel.getSoundsAbspielen()){
                spiel.setSoundsAbspielen(true);
                soundSchalter.setAufschrift("an");
                soundSchalter.repaint();
                return;
            }
        }
    }

    public ArrayList<Bild> getSoundmenueBilder() {
        return soundmenueBilder;
    }

    public Button getSoundSchalter() {
        return soundSchalter;
    }

    public Button getHgmusikSchalter() {
        return hgmusikSchalter;
    }
    
}

