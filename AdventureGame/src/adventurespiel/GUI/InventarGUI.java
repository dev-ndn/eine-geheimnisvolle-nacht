//InventarGUI
package adventurespiel.GUI;

import adventurespiel.fachklassen.Koordinaten;
import adventurespiel.fachklassen.Inventar;
import adventurespiel.steuerung.Bild;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.util.ArrayList;
/**
 * @author Jessica
 */
public class InventarGUI extends Panel{
    //Variablen
    //Bild des Inventarhintergrunds
    public Bild bildInventar = new Bild(new Koordinaten(30,655),"src/Bilder/allgemein/inventar.jpg");
    //Bildliste aller Bilder, die im Inventar gezeichnet werden
    public ArrayList<Bild> bildliste = new ArrayList<Bild>();
    //Images erstellen
    public Image inventarbild;
    //Inventar erstellen (auch leer vorhanden)
    public Inventar inventar = null;
    Toolkit tk;
    public Bild angeklickt;

    //Konstruktor für Hauptmenue
    public InventarGUI(Inventar i) {
        super();
        this.inventar = i;
        //Groeße der InventarGUI
        this.setSize(1024, 768);
        tk = getToolkit();
        // alle Gegenstaende aus der ArrayList gegenstaende der Klasse Inventar in for- Schleife aufgerufen
        for(int akt=0 ; akt < inventar.getGegenstaende().size() ; akt++){
            //das Inventarbild von Gegenstand aus Inventar geholt und der Bildliste hinzugefuegt
            bildliste.add(inventar.getGegenstaende().get(akt).getInventarbild());
        }
    }
    //get- Methode fuer die Bildliste
    public ArrayList<Bild> getBildliste() {
        return bildliste;
    }

}
