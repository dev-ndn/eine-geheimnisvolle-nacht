/*
 * Preloader
 * preloader für die bilder. bilder werden in arraylists geladen und
 * in spielloop angezeigt, bevor sie zum ersten mal anfangs eigentlich angezeigt
 * werden muessen, damit es zu keiner verzoegerung kommt.
 * in preloadliste werden die introanimationsbilder und raumbilder(hintergrund,
 * gegenstaende, ikeas, personen) und in menuepreloader alle menuebilder geladen.
 * Bei Spielstart werden alle menuebilder kurz angezeigt, damit diese im
 * arbeitsspeicher geladen sind. spielbilder werden geladen und angezeigt, wenn
 * man im menue auf neues spiel klickt. 
 */

package adventurespiel.GUI;

import adventurespiel.fachklassen.Koordinaten;
import adventurespiel.steuerung.Bild;
import adventurespiel.steuerung.SpielLoop;
import java.util.ArrayList;

/**
 *
 * @author Simon Jokuschies
 */
public class Preloader{

    private ArrayList<Bild> preloadListe = new ArrayList<Bild>();
    private ArrayList<Bild> menuePreloadListe = new ArrayList<Bild>();
    SpielLoop spiel;
    Bild schwarz = new Bild(new Koordinaten(0,0),"src/Bilder/menues/preload/preload.JPG");
    Bild wirdGeladen = new Bild(new Koordinaten(0,0),"src/Bilder/menues/preload/spielwirdgeladen.JPG");

    public Preloader(SpielLoop spiel){
        /**
         * @param preloadListe ArrayList, in der alle Animations und Spielbilder reingeladen werden
         * @param menuePreloadListe ArrayList, in der alle Menuegrafiken reingeladen werden
         * @param spiel Spiel auf das verwiesen wird
         * @param schwarz Schwarzbild zur optimalen Darstellung (wird als letztes angezeigt)
         * @param wird geladen spiel wird geladen bild
         **/
        this.spiel=spiel;


        //spielbilder und animationsbilder in ArrayList laden
        for (int i=0; i<spiel.getIntroBilder().size();i++){
            preloadListe.add(spiel.getIntroBilder().get(i));
        }
        for(int i=0; i<spiel.getOutroBilder().size(); i++){
            preloadListe.add(spiel.getOutroBilder().get(i));
        }
        for(int i=0; i<spiel.getRaumliste().size();i++){
            for(int j=0; j<spiel.getRaumliste().get(i).getGegenstand().size(); j++){ //alle gegenstände laden
                preloadListe.add(spiel.getRaumliste().get(i).getGegenstand().get(j).getRaumbild());
            }
             for(int j=0; j<spiel.getRaumliste().get(i).getIkea().size(); j++){      //alle ikeas laden
                preloadListe.add(spiel.getRaumliste().get(i).getIkea().get(j).getRaumbild());
            }
             for(int j=0; j<spiel.getRaumliste().get(i).getPerson().size(); j++){//alle personen laden
                preloadListe.add(spiel.getRaumliste().get(i).getPerson().get(j).getRaumbild());
            }
             for(int j=0; j<spiel.getRaumliste().get(i).getTuer().size(); j++){  //alle türen laden
                preloadListe.add(spiel.getRaumliste().get(i).getTuer().get(j).getRaumbild());
            }
            preloadListe.add(spiel.getRaumliste().get(i).getBild());
        }
        preloadListe.add(schwarz);


        //menuebilder in ArrayList laden
        for(int i=0; i<spiel.getSpielstartbilder().size(); i++){
            menuePreloadListe.add(spiel.getSpielstartbilder().get(i));
        }
        for(int i=0; i<spiel.getAnleitung().size(); i++){
            menuePreloadListe.add(spiel.getAnleitung().get(i));
        }
        for(int i=0; i<spiel.getCredits().size(); i++){
            menuePreloadListe.add(spiel.getCredits().get(i));
        }
        for(int i=0; i<spiel.getHauptmenue().size(); i++){
            menuePreloadListe.add(spiel.getHauptmenue().get(i));
        }
        for(int i=0; i<spiel.getOptionen().size(); i++){
            menuePreloadListe.add(spiel.getOptionen().get(i));
        }
        for(int i=0; i<spiel.getSoundMenue().getSoundmenueBilder().size(); i++){
            menuePreloadListe.add(spiel.getSoundMenue().getSoundmenueBilder().get(i));
        }
        for(int i=0; i<spiel.getLadenMenue().getLadeMenueBilder().size(); i++){
            menuePreloadListe.add(spiel.getLadenMenue().getLadeMenueBilder().get(i));
        }
         for(int i=0; i<spiel.getSpeicherMenue().getSpeichermenueBilder().size(); i++){
            menuePreloadListe.add(spiel.getSpeicherMenue().getSpeichermenueBilder().get(i));
        }
        for(int i=0; i<spiel.getOnline().size(); i++){
            menuePreloadListe.add(spiel.getOnline().get(i));
        }
           
        //personenbilder
        for(int i=0; i<spiel.getRaumliste().size();i++){
             for(int j=0; j<spiel.getRaumliste().get(i).getPerson().size(); j++){
                menuePreloadListe.add(spiel.getRaumliste().get(i).getPerson().get(j).getRaumbild());
            }
        }
        menuePreloadListe.add(wirdGeladen);
        menuePreloadListe.add(schwarz);
    }

    public ArrayList<Bild> getPreloadListe() {
        return preloadListe;
    }

    public ArrayList<Bild> getMenuePreloadListe() {
        return menuePreloadListe;
    }

    public Bild getWirdGeladen() {
        return wirdGeladen;
    }
}
