/*
 * DialogSpeichern
 * Dialog zum speichern des aktuellen spielstandes. der spieler drueckt
 * im speicherbuch auf einen der vier speicherstaende. daraufhin erscheint
 * der dialog. der spieler bekommt den hinweis, das ein besteheneder spiel-
 * stand ueberschrieben wird.
 */

package adventurespiel.GUI;

import adventurespiel.fachklassen.Speichermenue;
import adventurespiel.handler.XMLHandlingSchreiben;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Simon
 */
public class DialogSpeichern extends Frame implements ActionListener {

    Toolkit tk=getToolkit();
    Image butler;
    Speichermenue sp;
    Button speichern = new Button("speichern");
    Button abbrechen = new Button("abbrechen");
    TextField eingabe = new TextField();
    Font stil1 = new Font("Arial", Font.BOLD, 12);
    Font stil2 = new Font("Arial", Font.BOLD, 20);
    String nameSpielstand, zusatztext="";
    private XMLHandlingSchreiben xml = new XMLHandlingSchreiben();
    boolean eingabeAusgefuellt=false, spielSpeichern=false;


    public DialogSpeichern(Speichermenue sp){
         /**
         * @param tk toolkit fuer Grafiken
         * @param butler Butlerbild´
         * @param speichermeneu speichermenue auf das verwiesen wird
         * @param speichern button fuer spielstand speichern
         * @param abbrechen button fuer vorgang abbrechen
         * @param eingabe textfeld zur eingabe des namens des spielstandes
         * @param stil1 font-darstellung1
         * @param stil2 font-darstellung2
         * @param nameSpielstand zur uebermittlung der neuen aufschrift an speicherbuch
         * @param zusatztext textliche bestaetigung, dass gespeichert wird
         * @param xml xml zur speicherung
         * @param eingabeAusgefuellt ueberpruefung ob beim klick auf speichern im eingabefeld etwas eingetragen wurde
         * @param spielSpeichern inhalt des fensters je nachdem ob spielSpeichern true oder false
         **/
        
        setTitle("Wirklich hier speichern??");
        setIconImage(tk.getImage("src/AllgemeinBilder/cursor0.png"));
        this.sp=sp;
        setSize(400, 240);
        setAlwaysOnTop(true);
        setEnabled(true);
        setLayout(null);
        setResizable(false);
        setBackground(new Color(125,77,39));
        setCursor(sp.getSpiel().getStandardCursor());
        butler=tk.getImage("src/Bilder/person/speicherbutler.PNG");
        
        //eingabe-textfeld
        eingabe.setForeground(new Color(85, 40, 20));
        eingabe.setFont(stil2);
        eingabe.setBounds(20, 150, 230, 30);
        
        //speichern-button
        speichern.setBounds(20, 200, 110, 30);
        speichern.setBackground(new Color(125, 90, 60));
        speichern.addActionListener(this);
        speichern.setCursor(sp.getSpiel().getAufheben());
        //abbrechen-button
        abbrechen.setBounds(140, 200, 110, 30);
        abbrechen.setBackground(new Color(125, 90, 60));
        abbrechen.addActionListener(this);
        abbrechen.setCursor(sp.getSpiel().getAufheben());
   
        add(speichern);
        add(abbrechen);
        add(eingabe);
        
        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                //schließe fenster, wenn klick auf x
                dispose();
            }
        });

        //zeicheneingabenbeschraenkung - eigegebener name kann nicht laenger als 16 zeichen sein
        eingabe.addKeyListener(new KeyAdapter(){
            String maximalAngezeigt="";     //String, der maximal angezeigt werden kann
            String hilfsspeicher="";        //hilfsstring zur zwischenspeicherung

            public void keyTyped(KeyEvent e){
                maximalAngezeigt="";
                if(eingabe.getText().length()>16){
                    for(int i=0; i<16; i++){
                        hilfsspeicher = maximalAngezeigt;
                        maximalAngezeigt=String.valueOf(hilfsspeicher+eingabe.getText().charAt(i));
                    }
                    eingabe.setText(maximalAngezeigt);
                }               
            }
        });
    }

    @Override
    public void paint(Graphics g) {
       if(!spielSpeichern){
           g.drawImage(butler, 260,40,this);
           speichern.setVisible(true);
           abbrechen.setVisible(true);
           eingabe.setVisible(true);
           g.setFont(stil1);
           g.setColor(Color.BLACK);
           g.drawString("Möchtest du hier wirklich speichern?", 20,50);
           g.drawString("Bedenke, dass ein bestehender Stand", 20,65);
           g.drawString("überschrieben wird.", 20,80);
           g.drawString("Zum Speichern gib einen Namen unten", 20,95);
           g.drawString("ein und klicke auf speichern.", 20,110);

           //infotext, wenn das speichern gedrueckt und eingabefeld leer ist.
           g.setColor(Color.red);
           g.drawString (zusatztext, 20, 135);
       }
       if(spielSpeichern){
           g.drawImage(butler, 260,40,this);
           speichern.setVisible(false);
           abbrechen.setVisible(false);
           eingabe.setVisible(false);
           g.setFont(stil2);
           g.drawString("Der Spielstand" , 20,60);
           g.drawString(getNameSpielstand(),20,90);
           g.drawString("wird geschrieben",20,120);
           //daten in ausgewaehlte xml schreiben
           xml.speicherSpielstand(sp.getSpiel(),sp.getGewaehlterButton().getIndex(), sp.getSpiel().getInventar());
           try {
               //fenster schließen
               Thread.sleep(3000);
           } catch (InterruptedException ex) {
               Logger.getLogger(DialogSpeichern.class.getName()).log(Level.SEVERE, null, ex);
             }
           this.dispose();
           eingabe.setText("");
           zusatztext="";
           spielSpeichern=false;
        }
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==speichern){                            
            //abfrage, ob das eingabefeld (sinnvoll) ausgefüllt wurde
            if (eingabe.getText().equals("") ||
                eingabe.getText().equals("Leer") ||
                eingabe.getText().equals("leer") ||
                eingabe.getText().equals(" ")){
                   eingabeAusgefuellt=false;
            }        
            else{             
                eingabeAusgefuellt=true;
            }

            if(!eingabeAusgefuellt){
               if (eingabe.getText().equals("Leer") ||
                   eingabe.getText().equals("leer") ||
                   eingabe.getText().equals(" ") ){
                      zusatztext="unzulässiger Name!";
               }
               else{
                  zusatztext="Du musst schon einen Namen eingeben!";
               }
               
               this.repaint();
            }

            if(eingabeAusgefuellt){
                this.setNameSpielstand(eingabe.getText());
                zusatztext="";
                //auf dem angeklickten Button den neuen Namen zeigen
                if(sp.getGewaehlterButton().getIndex().equals("1")){
                     sp.getSpielstand01().setAufschrift(nameSpielstand);
                     sp.getSpiel().getLadenMenue().getSpielstand01().setAufschrift(nameSpielstand);
                     sp.getSpielstand01().repaint();
                }

                if(sp.getGewaehlterButton().getIndex().equals("2")){
                     sp.getSpielstand02().setAufschrift(nameSpielstand);
                     sp.getSpiel().getLadenMenue().getSpielstand02().setAufschrift(nameSpielstand);
                     sp.getSpielstand02().repaint();
                }

                if(sp.getGewaehlterButton().getIndex().equals("3")){
                     sp.getSpielstand03().setAufschrift(nameSpielstand);
                     sp.getSpiel().getLadenMenue().getSpielstand03().setAufschrift(nameSpielstand);
                     sp.getSpielstand03().repaint();
                }

                if(sp.getGewaehlterButton().getIndex().equals("4")){
                     sp.getSpielstand04().setAufschrift(nameSpielstand);
                     sp.getSpiel().getLadenMenue().getSpielstand04().setAufschrift(nameSpielstand);
                     sp.getSpielstand04().repaint();
                }
               spielSpeichern=true;
               this.repaint();
            }
       }
        if(e.getSource()==abbrechen){
            this.dispose();
            zusatztext="";
        }
    }

    public TextField getEingabe() {
        return eingabe;
    }

    public String getNameSpielstand() {
        return nameSpielstand;
    }

    public void setNameSpielstand(String nameSpielstand) {
        this.nameSpielstand = nameSpielstand;
    }

    private static class KeyAdapterImpl extends KeyAdapter {
        public KeyAdapterImpl() {
        }
    }
}
