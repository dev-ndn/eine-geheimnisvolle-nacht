/*
 * DialogLaden
 * dialog, der erscheint, wenn der spieler im Lademenue auf einen leeren
 * Spielstand klickt. Er wird darauf hingewiesen, dass der von ihm
 * gewaehlte spielstand nicht geladen werden kann, weil er leer ist.
 */

package adventurespiel.GUI;

import adventurespiel.fachklassen.LadeMenue;
import adventurespiel.steuerung.SpielLoop;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author Simon Jokuschies
 */
public class DialogLaden extends Frame implements ActionListener {

    Toolkit tk=getToolkit();
    Image butler;
    LadeMenue lademenue;
    Button ok = new Button("ok");
    Font stil1 = new Font("Arial", Font.BOLD, 12);

    public DialogLaden(LadeMenue lademenue){
        /**
         * @param tk toolkit fuer Grafiken
         * @param butler Butlerbild´
         * @param lademenue lademenue auf das verwiesen wird
         * @param ok button fuer registrierung
         * @param stil1 font-darstellung1
         **/
        setTitle("Der Spielstand ist leer");
        setIconImage(tk.getImage("src/AllgemeinBilder/cursor0.png"));
        this.lademenue=lademenue;
        setSize(400, 240);
        setAlwaysOnTop(true);
        setEnabled(true);
        setLayout(null);
        setResizable(false);
        setBackground(new Color(125,77,39));      

        ok.setBounds(20, 200, 110, 30);
        ok.setBackground(new Color(125, 90, 60));
        ok.addActionListener(this);   
        add(ok);
        ok.setCursor(lademenue.getSpiel().getAufheben());
        setCursor(lademenue.getSpiel().getStandardCursor());
        butler=tk.getImage("src/Bilder/person/speicherbutler.PNG");

        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                //schließe fenster, wenn klick auf x
                dispose();
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        g.setFont(stil1);
        g.drawImage(butler, 260,40,this);
        g.setColor(Color.BLACK);
        g.drawString("Der von dir gewählte Spielstand ist leer.", 20,50);
        g.drawString("Wähle bitte einen anderen aus.", 20,65);    
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==ok){
            //fenster schliessen wenn ok-button gedrueckt
            this.dispose();
       }
    }

  
}
