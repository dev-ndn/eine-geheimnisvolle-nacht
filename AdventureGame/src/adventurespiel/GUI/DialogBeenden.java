/*
 * DialogBeenden
 * kleines Fenster, das erscheint, wenn man im Hauptmenue auf beenden
 * drueckt. Man wird gefragt, ob man die Anwendung wirklich beenden
 * moechte und wird darauf hingewiesen, dass ein nichtgespeicherter
 * Spielstand verloren gehen wird. Dies dient der Sicherung, dass der
 * Spieler nicht ungewollt ohne speicherung das Spiel verlaesst.
 */

package adventurespiel.GUI;

import adventurespiel.steuerung.SpielLoop;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Simon Jokuschies
 */
public class DialogBeenden extends Frame implements ActionListener {

    Toolkit tk=getToolkit();
    Image butler;
    SpielLoop spiel;

    Button beenden = new Button("beenden");
    Button abbrechen = new Button("abbrechen");
    Font stil1 = new Font("Arial", Font.BOLD, 12);
    Font stil2 = new Font("Arial", Font.BOLD, 16);
    String nameSpielstand, zusatztext;
    boolean spielBeenden=false;

    public DialogBeenden(SpielLoop spiel){

        /**
         * @param tk toolkit fuer Grafiken
         * @param butler Butlerbild´
         * @param spiel spiel auf das verwiesen wird
         * @param beenden beenden-button
         * @param abbrechen abbrechen-button
         * @param stil1 font-darstellung1
         * @param stil2 font-darstellung2
         * @param nameSpielstand name des spielstandes zur weitergabe an menueSpielstand
         * @param zusatztext Text, der geaendert wird, wenn man auf beenden klickt
         * @param spielBeenden spiel wird bei true beendet
         **/
        setTitle("Wirklich beenden");
        setIconImage(tk.getImage("src/AllgemeinBilder/cursor0.png"));
        this.spiel=spiel;
        //Fenster positionieren (abhaengig von Location des Hauptfensters)
        setLocation((spiel.getLocation().x+700), (spiel.getLocation().y+30));
        setSize(400, 240);
        setAlwaysOnTop(true);
        setEnabled(true);
        setLayout(null);
        setResizable(false);
        setBackground(new Color(90,103,57));
        setCursor(spiel.getStandardCursor());
        butler=tk.getImage("src/Bilder/person/speicherbutler.PNG");       

        //beenden-button
        beenden.setBounds(20, 200, 110, 30);
        beenden.setBackground(new Color(70, 85, 40));
        beenden.addActionListener(this);
        beenden.setCursor(spiel.getAufheben());
        //abbrechen-button
        abbrechen.setBounds(140, 200, 110, 30);
        abbrechen.setBackground(new Color(70, 85, 40));
        abbrechen.addActionListener(this);
        abbrechen.setCursor(spiel.getAufheben());

        add(beenden);
        add(abbrechen);

        addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                dispose();
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        //solang nicht auf spiel beenden geklickt wurde
        if (!spielBeenden){
            g.setFont(stil1);
            g.drawImage(butler, 260,40,this);
            g.setColor(Color.BLACK);
            g.drawString("Möchten Sie das Spiel wirklich beenden?", 20,50);
            g.drawString("Bedenken Sie, dass ein nicht gespeicherter", 20,65);
            g.drawString("Spielstand verloren geht.", 20,80);
        }
        //sobald auf spiel beenden geklickt wurde
        //fontstil aendert sich, z.106 wird 3 sek. angezeigt, dann System.exit(0)
        if (spielBeenden){
            g.setFont(stil2);
            g.drawImage(butler, 260,40,this);
            g.drawString("Wie Sie wünschen, Sir.", 20, 70);
            g.drawString("Kommen Sie bald mal wieder!", 20, 90);
            beenden.setVisible(false);
            abbrechen.setVisible(false);
            spiel.gestartet = false;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DialogBeenden.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.exit(0);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==beenden){                     
            spielBeenden=true;
            this.repaint();           
        }
        if(e.getSource()==abbrechen){
            this.dispose();
        }
    }

    public String getZusatztext() {
        return zusatztext;
    }

    public void setZusatztext(String zusatztext) {
        this.zusatztext = zusatztext;
    }
}
