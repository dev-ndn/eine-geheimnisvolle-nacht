/**
 * THIS SOFTWARE IS PROVIDED BY METASTUFF, LTD. AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL METASTUFF, LTD. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * Copyright 2001-2005 (C) MetaStuff, Ltd. All Rights Reserved.
 */

package adventurespiel.handler;

import adventurespiel.fachklassen.Raum;
import adventurespiel.fachklassen.Inventar;
import adventurespiel.fachklassen.Ikea;
import adventurespiel.fachklassen.Gegenstand;
import adventurespiel.fachklassen.Menuebutton;
import adventurespiel.fachklassen.Person;
import adventurespiel.fachklassen.Tuer;
import adventurespiel.steuerung.SpielLoop;
import java.util.ArrayList;
import org.dom4j.Document;
import org.dom4j.Element;

/**
 *
 * @author Nadine, Mounir
 */
public class XMLHandlingSchreiben extends XMLHandling {

    private Document document;

    public XMLHandlingSchreiben() {
    }

    public XMLHandlingSchreiben(SpielLoop spiel) {
        super(spiel);
    }

    //einstellungsdatei schreiben mit spielstandsnamen und soundeinstellungen
    public Document einstellungenSchreiben(SpielLoop spiel, ArrayList<Menuebutton> spielstaende) {
        //neues Document erzeugen
        document = erzeugeDocument("root");
        //neues unterelement hinzufuegen
        Element einstellungenElement = document.getRootElement().addElement("einstellungen");
        //fuer jeden spielstand ein neues tag dass in einem attribut namen die aufschrift speichert
        for (int i = 0; i < spielstaende.size(); i++) {
            Element spielstandElement = einstellungenElement.addElement("spielstand");
            spielstandElement.addAttribute("name", spielstaende.get(i).getAufschrift());
        }
        //tag hinzufugen fuer soundeinstellungen
        Element sound = einstellungenElement.addElement("sound");
        sound.addAttribute("sounds","true");
        sound.addAttribute("hintergrund", "true");
        return document;
    }

    //einstellungen zum sound speichern
    public static void speicherSoundeinstellungen(SpielLoop spiel) {
        Document document = parse(xmlpfad+"default/einstellungen.xml");
        schreibBoole(document, "//einstellungen/sound/@sounds", spiel.getSoundsAbspielen());
        schreibBoole(document, "//einstellungen/sound/@hintergrund", spiel.getHintergrundmusikAbspielen());
        ueberschreibeDatei("src/xmls/default/einstellungen.xml", document);
    }

    //inventar schreiben
    public Document inventarSchreiben(Inventar inventar) {
        document = erzeugeDocument("root");
        Element inventarElement = document.getRootElement().addElement("inventar");
        //wenn das inventar gegenstaende enthaelt
        if (inventar.getGegenstaende() != null) {
            //fuer jeden gegenstand ein tag mit einem attribut das den namen enthaelt
            for (int i = 0; i < inventar.getGegenstaende().size(); i++) {
                Element gegenstandElement = inventarElement.addElement("gegenstand");
                gegenstandElement.addAttribute("name", inventar.getGegenstaende().get(i).getName());
            }
        }
        return document;
    }

    /**
     * schreibt den zustand eines raums innerhalb des laufendes spiels in ein document
     * @param raum der geschrieben werden soll
     * @param datei die ueberschrieben werden soll und die raumdaten enthaelt
     * @param inventarliste kontrollliste mit den namen der gegenstaende, die sich im inventar befinden
     * @param gegenstandsliste kontrollliste mit den namen der gegenstaende, die sich im raum befinden
     * @param tuerliste kontrollliste mit den tueren des raums
     * @param ikealiste kontrollliste mit den im raum befindlichen ikeas
     * @param personliste kontrollliste mit den im raum befindlichen personen
     * @return gibt das document mit dem aktuellen spielstand zurueck
     */
    public Document raumSchreiben(Raum raum, String datei, ArrayList<String> inventarliste, ArrayList<String> gegenstandsliste,
            ArrayList<String> tuerliste, ArrayList<String> ikealiste, ArrayList<String> personliste) {
        document = parse(datei);
        //anzahl der tueren in der xml
        int zahl = document.selectNodes("//raum/tuer").size();
        Tuer tuer = null;
        String path;
        //fuer jede tuer das gleiche ueberpruefen
        for(int i = 0; i < zahl; i++){
            path = "//raum/tuer["+(i+1)+"]";
            //tuer objekt erzeugen
            tuer = spiel.getKa().sucheTuer(lies(document, path + "/@name"), raum);
            //wenn der name der i.tuer im dokument in der tuerliste enthalten ist
            if(istEnthalten(lies(document, path + "/@name"), tuerliste)){
                //..schreib in den spielstand, ob sie voll ist
                schreibBoole(document, path + "/@istvoll", tuer.getIstVoll());
                //..schreib in den spielstand, ob sie sichtbar ist
                schreibBoole(document, path + "/@istsichtbar", tuer.getIstSichtbar());
            }
        }
        //anzahl personen im document
        zahl = document.selectNodes("//raum/person").size();
        Person person = null;
        //fuer jede person das gleiche ueberpruefen
        for(int i = 0; i < zahl; i++){
            path = "//raum/person["+(i+1)+"]";
            //person objekt erzeugen
            person = spiel.getKa().suchePerson(lies(document, path + "/@name"), raum);
            //wenn der name der i.person in der personliste enthalten ist
            if(istEnthalten(lies(document, path + "/@name"), personliste)){
                //..schreib in den spielstand, ob sie voll ist
                schreibBoole(document, path + "/@istvoll", person.getIstVoll());
                //..schreib in den spielstand, ob sie sichtbar ist
                schreibBoole(document, path + "/@istsichtbar", person.getIstSichtbar());
            }
            //wenn die person leer ist und der String in quelleleerbild der person nicht leer ist
            if(!person.getIstVoll() && !person.getQuelleLeerBild().equals("")){
                //..schreib den pfad der quelleleerbild ins attribut quelle
                schreib(document, path + "/@quelle", person.getQuelleLeerBild());
            }
        }
        //anzahl ikeas im document
        zahl = document.selectNodes("//raum/ikea").size();
        Ikea ikea = null;
        //fuer jedes ikea das gleiche ueberpruefen
        for(int i = 0; i < zahl; i++){
            path = "//raum/ikea["+(i+1)+"]";
            //ikea objekt erzeugen
            ikea = spiel.getKa().sucheIkea(lies(document, path + "/@name"), raum);
            //wenn der name des i.ikea in der ikealiste enthalten ist
            if(istEnthalten(lies(document, path + "/@name"), ikealiste)){
                //..schreib in den spielstand, ob es voll ist
                schreibBoole(document, path + "/@istvoll", ikea.getIstVoll());
                //..schreib in den spielstand, ob es sichtbar ist
                schreibBoole(document, path + "/@istsichtbar", ikea.getIstSichtbar());
            }
            //wenn der name des i.ikea nicht in der ikealiste enthalten ist
            if(!istEnthalten(lies(document, path + "/@name"), ikealiste)){
                //..setze es auf unsichtbar
                schreibBoole(document, path + "/@istsichtbar", false);
            }
            //wenn das ikea nicht null ist und das ikea leer ist und der string leerbildes nicht leer ist
            if(ikea!= null && !ikea.getIstVoll() && !ikea.getQuelleLeerBild().equals("")){
                //..setze den string aus quelleleerbild als wert des attributs quelle
                schreib(document, path + "/@quelle", ikea.getQuelleLeerBild());
            }
        }
        //anzahl gegenstaende im raum
        zahl = document.selectNodes("//raum/gegenstand").size();
        Gegenstand gegenstand = null;
        //fuer jeden gegenstand das gleiche ueberpruefen
        for(int i = 0; i < zahl; i++){
            path = "//raum/gegenstand["+(i+1)+"]";
            //gegenstandsobjekt erzeugen
            gegenstand = spiel.getKa().sucheGegenstand(lies(document, path + "/@name"), raum);
            //wenn der gegenstand in der gegenstandsliste enthalten ist
            if(istEnthalten(lies(document, path + "/@name"), gegenstandsliste)){
                //..schreib in den spielstand, ob er sichtbar ist
                schreibBoole(document, path + "/@istsichtbar", gegenstand.getIstSichtbar());
                //..setz das attribut iminventar auf false
                schreibBoole(document, path + "/@iminventar", false);
            }
            //wenn der gegenstand in der inventarliste enthalten ist
            if(istEnthalten(lies(document, path + "/@name"), inventarliste)){
                //..setz ihn auf unsichtbar
                schreibBoole(document, path + "/@istsichtbar", false);
                //..setz das attribut iminventar auf true
                schreibBoole(document, path + "/@iminventar", true);
            }
            //wenn der gegenstand weder in der gegenstandsliste noch in der inventarliste enthalten ist
            if(!istEnthalten(lies(document, path + "/@name"), gegenstandsliste) &&
                    !istEnthalten(lies(document, path + "/@name"), inventarliste)){
                //..setz ihn auf unsichtbar
                schreibBoole(document, path + "/@istsichtbar", false);
                //..setz das attribut iminventar auf false
                schreibBoole(document, path + "/@iminventar", false);
            }
        }
        return document;
    }

    /**
     * speichert einen neuen spielstand in einer xml
     * @param spiel objekt des spiels
     * @param spielstandindex stelle des spielstands
     * @param inventar auszulesendes inventar
     * @return true, wenn erfolgreich ueberschrieben wurde
     */
    public boolean speicherSpielstand(SpielLoop spiel, String spielstandindex, Inventar inventar){
	XMLHandlingSchreiben xmlhandling = new XMLHandlingSchreiben(spiel);
        //arraylist mit raeumen anlegen
        ArrayList<Raum> raeume = new ArrayList<Raum>();
        raeume.add(spiel.getAbstellkammer());
        raeume.add(spiel.getFoyer());
        raeume.add(spiel.getInnenhof());
        raeume.add(spiel.getKaminzimmer());
        raeume.add(spiel.getSpeicherraum());
        raeume.add(spiel.getHunderaum());
        //kontrollliste mit allen gegenstaenden des inventars anlegen
        ArrayList<String> inventarliste = new ArrayList<String>();
        for(int i = 0; i < spiel.getInventar().getGegenstaende().size(); i++){
            //schreib den namen des gegenstands in die kontrollliste fuers speichern
            inventarliste.add(spiel.getInventar().getGegenstaende().get(i).getName());
        }
        //ordner in dem sich die spielstanddateien befinden
	String ordner = new String(xmlpfad+"spielstand"+spielstandindex+"/");
        String datei = "";
        //kontrolllisten mit namen der dinge anlegen
        ArrayList<String> gegenstandsliste = new ArrayList<String>();
        ArrayList<String> tuerliste = new ArrayList<String>();
        ArrayList<String> ikealiste = new ArrayList<String>();
        ArrayList<String> personliste = new ArrayList<String>();
        for (int i = 0; i < raeume.size(); i++) {
            for(int j = 0; j < raeume.get(i).getGegenstand().size(); j++){
                //schreib den namen des gegenstands in die kontrollliste fuers speichern
                gegenstandsliste.add(raeume.get(i).getGegenstand().get(j).getName());
            }
            for(int j = 0; j < raeume.get(i).getTuer().size(); j++){
                //schreib den namen der tuer in die kontrollliste fuers speichern
                tuerliste.add(raeume.get(i).getTuer().get(j).getName());
            }
            for(int j = 0; j < raeume.get(i).getIkea().size(); j++){
                //schreib den namen des ikea in die kontrollliste fuers speichern
                ikealiste.add(raeume.get(i).getIkea().get(j).getName());
            }
            for(int j = 0; j < raeume.get(i).getPerson().size(); j++){
                //schreib den namen der person in die kontrollliste fuers speichern
                personliste.add(raeume.get(i).getPerson().get(j).getName());
            }
            datei = new String(ordner+"Raum"+(i+1)+".xml");
            //resette die daten des spielstands, zuruecksetzen auf daten wie beim ueberschreiben eines neuen spiels
            ueberschreibeDatei(datei, XMLHandling.parse(xmlpfad+"default/"+"Raum"+(i+1)+".xml"));
            //schreibe den tatsaechlichen spielstand in die xml
            ueberschreibeDatei(datei, xmlhandling.raumSchreiben(raeume.get(i), datei,
                    inventarliste, gegenstandsliste, tuerliste, ikealiste, personliste));
            //listen clearen fuer den naechsten schleifen durchlauf
            gegenstandsliste.clear();
            tuerliste.clear();
            ikealiste.clear();
            personliste.clear();
        }

        datei = new String(xmlpfad+"default/einstellungen.xml");
        //derzeitige einstellungen ueberschreiben
        boolean ueberschrieben = ueberschreibeDatei(datei, xmlhandling.einstellungenSchreiben(spiel, spiel.getLadenMenue().getSpielstaende()));
        XMLHandlingSchreiben.speicherSoundeinstellungen(spiel);
        //inventar als kontrollinstrument ueberschreiben
        datei = new String(ordner+"Inventar.xml");
        ueberschreibeDatei(datei, xmlhandling.inventarSchreiben(inventar));
        inventarliste.clear();
	return ueberschrieben;
    }
}

