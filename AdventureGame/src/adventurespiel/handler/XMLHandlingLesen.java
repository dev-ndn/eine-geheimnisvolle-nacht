/**
 * THIS SOFTWARE IS PROVIDED BY METASTUFF, LTD. AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL METASTUFF, LTD. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * Copyright 2001-2005 (C) MetaStuff, Ltd. All Rights Reserved.
 */

package adventurespiel.handler;

import adventurespiel.fachklassen.Spielfigur;
import adventurespiel.fachklassen.Raum;
import adventurespiel.fachklassen.Koordinaten;
import adventurespiel.fachklassen.Inventar;
import adventurespiel.fachklassen.Ikea;
import adventurespiel.fachklassen.Gegenstand;
import adventurespiel.fachklassen.Interaktionsmoeglichkeit;
import adventurespiel.fachklassen.Kombinationsmoeglichkeit;
import adventurespiel.fachklassen.Person;
import adventurespiel.fachklassen.Tuer;
import adventurespiel.steuerung.Bild;
import adventurespiel.steuerung.SpielLoop;
import java.awt.Polygon;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.dom4j.Document;
import adventurespiel.fachklassen.Wegfindung;

/**
 * Klasse in der Funktionen zum Laden und Auslesen der xmls stehen
 *
 * @author Nadine, Mounir
 */
public class XMLHandlingLesen extends XMLHandling {
    
    private int[][] gehflaeche;
    private Document document;
    private ArrayList<String> infotexte;
    private ArrayList<File> sounds;
    private ArrayList<Bild> bilder;
    private ArrayList<Interaktionsmoeglichkeit> interaktionen;
    private ArrayList<Kombinationsmoeglichkeit> kombinationen;
    private ArrayList<Person> personliste;
    private ArrayList<Tuer> tuerliste;
    private ArrayList<Gegenstand> gegenstandsliste;
    private ArrayList<Ikea> ikealiste;
    private List punkte, list;
    private Polygon flaeche;
    private Raum raum;
    private Koordinaten koordinaten;

    public XMLHandlingLesen() {
    }

    public XMLHandlingLesen(SpielLoop spiel) {
        super(spiel);
    }

    /**
     * liefert den namen des spielstands aus den einstellungen
     * @param zahl des spielstands
     * @return name des spielstands
     */
    public String ladeSpielstandname(int zahl) {
        document = parse(xmlpfad + "default/einstellungen.xml");
        return lies(document, "//einstellungen/spielstand[" + zahl + "]/@name");

    }

    /**
     * aktuelle soundeinstellungen laden aus den einstellungen
     * @param spiel
     */
    public void ladeSoundeinstellungen(SpielLoop spiel) {
        document = parse(xmlpfad+"default/einstellungen.xml");
        spiel.setSoundsAbspielen(liesBoole(document, "//einstellungen/sound/@sounds"));
        spiel.setHintergrundmusikAbspielen(liesBoole(document, "//einstellungen/sound/@hintergrund"));
    }

    /**
     * liest einen kompletten Raum aus einer xml aus
     * @param spielstand der geladen werden soll
     * @param datei xml in der sich die daten des raums befinden
     * @param spielfigur
     * @param inventar notwendig fuer das einlesen der gegenstaende
     * @param kombinationen um gegenstaende den kombis zuzuordnen
     * @param interaktionen um gegenstaende und ikeas interaktionen zuzuordnen
     * @return liefert das fertige objekt des eingelesenen raums
     */
    public Raum raumAuslesen(String spielstand, String datei, Spielfigur spielfigur, Inventar inventar,
            ArrayList<Kombinationsmoeglichkeit> kombinationen,
            ArrayList<Interaktionsmoeglichkeit> interaktionen) {
        gegenstandsliste = gegenstaendeAuslesen(spielstand, datei, inventar,
                kombinationen, interaktionen);
        ikealiste = ikeasAuslesen(spielstand, datei, interaktionen);
        tuerliste = tuerAuslesen(spielstand, datei, interaktionen);
        personliste = personAuslesen(spielstand, datei, interaktionen);
        koordinaten = new Koordinaten(liesInt(document, "//raum/koordinaten/@x"),
                liesInt(document, "//raum/koordinaten/@y"));

        raum = new Raum(lies(document, "//raum/@name"),koordinaten, liesInt(document, "//raum/@breite"),
                liesInt(document, "//raum/@hoehe"),spielfigur, ikealiste, gegenstandsliste, tuerliste, personliste,
                lies(document, "//raum/@quelle"), new File(lies(document, "//raum/@sound")));
        raum.setWegfindung(wegfindungAuslesen(raum, datei));
        return raum;
    }

    //liest eine Wegfindung inkl. eines int[][]-Rasters ein
    public Wegfindung wegfindungAuslesen(Raum raum, String datei) {
        document = parse(xmlpfad + "default/gehflaeche" + datei);
        int zellengroesse = liesInt(document, "//root/@zellengroesse");
        int spalten = liesInt(document, "//root/@anzahlSpalten");
        int zeilen = liesInt(document, "//root/@anzahlZeilen");
        int hoehe = liesInt(document, "//root/@hoehe");
        int breite = liesInt(document, "//root/@breite");
        int starty = liesInt(document, "//root/@abstandOben");
        int startx = liesInt(document, "//root/@abstandLinks");

        gehflaeche = new int[spalten][zeilen];
        int zaehler = 0;
        for (int i = 0; i < (spalten); i++) {
            for (int j = 0; j < (zeilen); j++) {
                //wenn es kein solches feld gibt, setzte den wert im int[][] auf 1
                if (document.selectSingleNode("//feld" + zaehler + "/@status") == null) {
                    gehflaeche[i][j] = 1;
                    //wenn es ein solches feld gibt, setzte den wert aus der xml ins int[][]
                } else {
                    gehflaeche[i][j] = liesInt(document, "//feld" + zaehler + "/@status");
                }
                zaehler++;
            }
        }
        return new Wegfindung(zellengroesse, hoehe, breite, spalten, zeilen, gehflaeche, startx, starty);
    }

    /**
     * liest eine Arraylist mit Bilder ein, repraesentiert ein Menue
     * @param menue name des zu ladenden menues
     * @return arraylist mit bildern
     */
    public ArrayList<Bild> menueAuslesen(String menue) {

        document = parse(xmlpfad+"default/menue.xml");
        bilder = new ArrayList<Bild>();
        Bild bild = null;
        String path;
        list = document.selectNodes("//" + menue + "/bild"); //+menue+ um die einzelnen menues variable auszulesen, hauptmenue, optionen
        for (int i = 0; i < list.size(); i++) {
            int zahl = i + 1;
            path = "//" + menue + "/bild[" + zahl + "]";
            bild = new Bild(new Koordinaten(liesInt(document, path + "/koordinaten/@x"),
                    liesInt(document, path + "/koordinaten/@y")),lies(document, path + "/@quelle"),
                    liesInt(document, path +"/@breite"),liesInt(document, path + "/@hoehe"),
                    lies(document, path + "/@name"));
            if("introBilder".equals(menue) || "outroBilder".equals(menue) || "Spielstartbilder".equals(menue)){
                bild.setAnzeigeDauer(liesInt(document, path + "/@anzeigeDauer"));
            }
            bilder.add(bild);
        }

        return bilder;
    }

    //inventar auslesen
    public Inventar inventarAuslesen(SpielLoop spiel, String spielstand, String datei, Spielfigur spielfigur,
            ArrayList<Kombinationsmoeglichkeit> kombinationen,
            ArrayList<Interaktionsmoeglichkeit> interaktionen) {

        Inventar inventar = new Inventar(spielfigur);
        Gegenstand gegenstand = null;

        inventar.setGegenstaende(gegenstaendeAuslesen(spielstand, datei, inventar,
                kombinationen,
                interaktionen));
        //alle inventarbilder zur inventarbildliste hinzufuegen
        for (int i = 0; i < inventar.getGegenstaende().size(); i++) {
            gegenstand = inventar.getGegenstaende().get(i);
            spiel.getInventarGUI().getBildliste().add(gegenstand.getInventarbild());
        }
        return inventar;
    }

    //gegenstaende aus xml auslesen
    public ArrayList<Gegenstand> gegenstaendeAuslesen(String spielstand, String datei, Inventar inventar,
            ArrayList<Kombinationsmoeglichkeit> kombinationen,
            ArrayList<Interaktionsmoeglichkeit> interaktionen) {

        document = parse(xmlpfad + spielstand + "/" + datei);
        gegenstandsliste = new ArrayList<Gegenstand>();
        Gegenstand gegenstand;
        int temp, zahl;
        String path;
        boolean imInventar = false; //gehoert der gegenstand ins inventar

        //list liefert die Anzahl an vorhandenen Gegenstaenden in der xml
        list = document.selectNodes("//raum/gegenstand");
        for (int i = 0; i < list.size(); i++) {
            zahl = i + 1;
            path = "//gegenstand[" + zahl + "]";
            imInventar = liesBoole(document, path + "/@iminventar");
            //anzahl punkte des polygons des gegenstands
            punkte = document.selectNodes(path + "/polygon/punkt");
            flaeche = new Polygon();
            //alle punkte des gegenstandspolygons dem polygon hinzufuegen
            for (int j = 0; j < punkte.size(); j++) {
                int nr = j + 1;
                flaeche.addPoint(liesInt(document, path + "/polygon/punkt[" + nr + "]/@x"),
                        liesInt(document, path + "/polygon/punkt[" + nr + "]/@y"));
            }
            //aktuellen gegenstand einlesen
            gegenstand = new Gegenstand(lies(document, path + "/@name"), lies(document, path + "/@infotext"),
                    new Koordinaten(liesInt(document, path + "/koordinaten/@x"),
                    liesInt(document, path + "/koordinaten/@y")), getSpiel().getInventar(),
                    liesInt(document, path + "/@breite"),liesInt(document, path + "/@hoehe"),
                    lies(document, path + "/@quelleRaum"),lies(document, path + "/@quelleInventar"),
                    liesBoole(document, path + "/@istsichtbar"), flaeche,new File(lies(document, path + "/@sammelSound")),
                    new File(lies(document, path + "/@infosound")));
            //wenn die arraylisten mit kombis und interaktionen nicht leer sind
            if (kombinationen != null || interaktionen != null) {
                //wenn das attribut zur interaktionsmoeglichkeit nicht null lautet
                if (!lies(document, path + "/@interaktionsmoeglichkeit").equals("null")) {
                    //..lies die zahl der interaktion ein
                    temp = liesInt(document, path + "/@interaktionsmoeglichkeit");
                    //..ordne den gegenstand der interaktion an der stelle temp-1 zu
                    gegenstand.setInteraktionsmoeglichkeit(interaktionen.get(temp - 1));
                    //..ordne der interaktion an der stelle temp-1 den gegenstand zu
                    interaktionen.get(temp - 1).setGegenstand(gegenstand);
                }
                //wenn das attribut interaktionsergebnis nicht null ist
                if (!lies(document, path + "/@interaktionsergebnis").equals("null")) {
                    //lies die zahl der interaktion ein
                    temp = liesInt(document, path + "/@interaktionsergebnis");
                    //ordne den gegenstand als ergebnis der interaktion an der telle temp-1 zu
                    interaktionen.get(temp - 1).setNeuerGegenstand(gegenstand);
                }
                //falls es mehrere interaktionsmoeglichkeitsattributte gibt lies auch diese ein
                //und ordne gegenstaende und interaktionen einander zu
                for (int j = 0; j < 4; j++) {
                    if (document.selectSingleNode(path + "/@interaktionsmoeglichkeit" + (j + 1)) != null) {
                        if (!lies(document, path + "/@interaktionsmoeglichkeit" + (j + 1)).equals("null")) {
                            temp = liesInt(document, path + "/@interaktionsmoeglichkeit" + (j + 1));
                            gegenstand.setInteraktionsmoeglichkeit(interaktionen.get(temp - 1));
                            interaktionen.get(temp - 1).setGegenstand(gegenstand);
                        }
                    }
                }
                //wenn das attribut zur kombinationsmoeglichkeit nicht null lautet
                if (!lies(document, path + "/@kombinationsmoeglichkeit").equals("null")) {
                    //..lies die zahl der kombi ein
                    temp = liesInt(document, path + "/@kombinationsmoeglichkeit");
                    //..ordne dem gegenstand die kombi an der stelle temp-1 zu
                    gegenstand.setKombinationsmoeglichkeit(kombinationen.get(temp - 1));
                    //..ordne der kombi an der stelle temp-1 den gegenstand
                    //an stelle 0 im array zu wenn die stelle 0 noch leer ist
                    if (kombinationen.get(temp - 1).getGegenstand()[0] == null) {
                        kombinationen.get(temp - 1).getGegenstand()[0] = gegenstand;
                        //an stelle 1 im array zu wenn die stelle 0 nicht nciht null ist und stelle 1 noch leer ist
                    } else if (kombinationen.get(temp - 1).getGegenstand()[0] != null) {
                        kombinationen.get(temp - 1).getGegenstand()[1] = gegenstand;
                    }
                }
                //wenn das attribut kombinationsergebnis nicht null ist
                if (!lies(document, path + "/@kombinationsergebnis").equals("null")) {
                    //lies die zahl der kombi ein
                    temp = liesInt(document, path + "/@kombinationsergebnis");
                    //ordne der kombi den gegenstand als kombiergebnis zu
                    kombinationen.get(temp - 1).setNeuerGegenstand(gegenstand);
                }
            }
            //wenn iminventar true ist, fuege den gegenstand dem inventar und der inventargui zu
            if (imInventar) {
                getSpiel().getInventar().addGegenstand(gegenstand);
                getSpiel().getInventarGUI().getBildliste().add(gegenstand.getInventarbild());
            }
            //wenn der gegenstand sichtbar ist, fuege ihn der kontrollliste zu und den images
            if (gegenstand.getIstSichtbar()) {
                getSpiel().getKontrollliste().add(gegenstand.getRaumbild());
                getSpiel().getImages().add(gegenstand.getRaumbild());
            }
            //wenn der gegenstand nicht im inventar ist und sichtbar, fuege ihn der gegenstandsliste hinzu
            if (!imInventar && gegenstand.getIstSichtbar()) {
                gegenstandsliste.add(gegenstand);
            }

        }
        return gegenstandsliste;
    }

    //ikeas aus xml einlesen
    public ArrayList<Ikea> ikeasAuslesen(String spielstand, String datei,
            ArrayList<Interaktionsmoeglichkeit> interaktionen) {
        document = parse(xmlpfad + spielstand + "/" + datei);
        ikealiste = new ArrayList<Ikea>();
        Ikea ikea;
        int temp;
        String path;
        list = document.selectNodes("//raum/ikea"); // anzahl an ikeas in der xml

        //alle ikeas einlesen
        for (int i = 0; i < list.size(); i++) {
            int zahl = i + 1;
            path = "//ikea[" + zahl + "]";
            infotexte = new ArrayList<String>();
            infotexte.add(lies(document, path + "/@infotext"));
            infotexte.add(lies(document, path + "/@infotext2"));
            //anzahl der punkte des polygons des ikea
            punkte = document.selectNodes(path + "/polygon/punkt");
            flaeche = new Polygon();
            //jeden punkt des ikeapolygons dem polygon hinzufuegen
            for (int j = 0; j < punkte.size(); j++) {
                int nr = j + 1;
                flaeche.addPoint(liesInt(document, path + "/polygon/punkt[" + nr + "]/@x"),
                       liesInt(document, path + "/polygon/punkt[" + nr + "]/@y"));
            }
            //neues ikea erzeugen
            ikea = new Ikea(lies(document, path + "/@name"),
                    new Koordinaten(liesInt(document, path + "/koordinaten/@x"),
                    liesInt(document, path + "/koordinaten/@y")), infotexte,
                    liesBoole(document, path + "/@istvoll"),liesInt(document, path + "/@breite"),
                    liesInt(document, path + "/@hoehe"),lies(document, path + "/@quelle"),
                    new ArrayList<Gegenstand>(), liesBoole(document, path + "/@istsichtbar"),
                    flaeche, lies(document, path + "/@quelleleerbild"),
                    new File(lies(document, path + "/@sammelSound")),
                    new File(lies(document, path + "/@infosound")));
            //wenn die interaktionen nicht null sind
            if (interaktionen != null) {
                //wenn im attribut interaktionsmoeglichkeit nicht null steht
                if (!lies(document, path + "/@interaktionsmoeglichkeit").equals("null")) {
                    //lies die zahl der interaktion
                    temp = liesInt(document, path + "/@interaktionsmoeglichkeit");
                    //ordne ikea die interaktion an der stelle temp-1 zu
                    ikea.setInteraktionsmoeglichkeit(interaktionen.get(temp - 1));
                    //ordne der interaktion an der stelle temp-1 das ikea zu
                    interaktionen.get(temp - 1).setIkea(ikea);
                }
                //wenn es mehrere attribute gibt lies sie auch ein und ordne sie zu
                for (int j = 0; j < 4; j++) {
                    if (document.selectSingleNode(path + "/@interaktionsmoeglichkeit" + (j + 1)) != null) {
                        if (!document.selectSingleNode(path + "/@interaktionsmoeglichkeit" + (j + 1)).getText().equals("null")) {
                            temp = Integer.parseInt(document.selectSingleNode(path + "/@interaktionsmoeglichkeit" + (j + 1)).getText());
                            ikea.setInteraktionsmoeglichkeit(interaktionen.get(temp - 1));
                            interaktionen.get(temp - 1).setIkea(ikea);
                        }
                    }
                }
            }
            //wenn das ikea sichtbar ist, ordne es der kontrollliste und images zu
            if (ikea.getIstSichtbar()) {
                getSpiel().getKontrollliste().add(ikea.getRaumbild());
                getSpiel().getImages().add(ikea.getRaumbild());
            }
            ikealiste.add(ikea); //fuege das ikea der ikealiste hinzu
        }
        return ikealiste;
    }

    //tueren aus xml auslesen
    public ArrayList<Tuer> tuerAuslesen(String spielstand, String datei,
            ArrayList<Interaktionsmoeglichkeit> interaktionen) {
        document = parse(xmlpfad + spielstand + "/" + datei);
        tuerliste = new ArrayList<Tuer>();
        Tuer tuer;
        int temp;
        String path;
        list = document.selectNodes("//raum/tuer"); //anzahl tueren in der xml
        //tueren einlesen
        for (int i = 0; i < list.size(); i++) {
            int zahl = i + 1;
            path = "//tuer[" + zahl + "]";
            infotexte = new ArrayList<String>();
            infotexte.add(lies(document, path + "/@infotext"));
            //anzahl punkte des tuerpolygons
            punkte = document.selectNodes(path + "/polygon/punkt");
            flaeche = new Polygon();
            //alle punkte hinzufuegen zum polygon
            for (int j = 0; j < punkte.size(); j++) {
                int nr = j + 1;
                flaeche.addPoint(liesInt(document, path + "/polygon/punkt[" + nr + "]/@x"),
                        liesInt(document, path + "/polygon/punkt[" + nr + "]/@y"));
            }

            Koordinaten figurposition = new Koordinaten(liesInt(document, path + "/figurposition/@x"),
                    liesInt(document, path + "/figurposition/@y"));
            //neue tuer erzeugen
            tuer = new Tuer(lies(document, path + "/@name"),
                    new Koordinaten(liesInt(document, path + "/koordinaten/@x"),
                    liesInt(document, path + "/koordinaten/@y")),infotexte,liesBoole(document, path + "/@istvoll"),
                    liesInt(document, path + "/@breite"),liesInt(document, path + "/@hoehe"),
                    lies(document, path + "/@quelle"),new ArrayList<Gegenstand>(),
                    lies(document, path + "/wunschort/@raum"),liesBoole(document, path + "/@istsichtbar"),
                    flaeche, figurposition,lies(document, path + "/@startbild"),
                    new File(lies(document, path + "/@sammelSound")));
            //wenn interaktionen nicht null ist
            if (interaktionen != null) {
                //wenn die tuer ne interaktion hat also nicht null steht in der xml
                if (!lies(document, path + "/@interaktionsmoeglichkeit").equals("null")) {
                    //lies die stelle der interaktion ein
                    temp = liesInt(document, path + "/@interaktionsmoeglichkeit");
                    //ordne der tuer die interaktion an der stelle temp-1 zu
                    tuer.setInteraktionsmoeglichkeit(interaktionen.get(temp - 1));
                    //ordne der interaktion an der stelle temp-1 die tuer als ikea zu
                    interaktionen.get(temp - 1).setIkea(tuer);
                }
            }
            //wenn die tuer sichtbar ist, fuege sie der kontrollliste und images hinzu
            if (tuer.getIstSichtbar()) {
                getSpiel().getKontrollliste().add(tuer.getRaumbild());
                getSpiel().getImages().add(tuer.getRaumbild());
            }
            //fuege die tuer der tuerliste hinzu
            tuerliste.add(tuer);
        }
        return tuerliste;
    }

    //personen aus der xml auslesen
    public ArrayList<Person> personAuslesen(String spielstand, String datei,
            ArrayList<Interaktionsmoeglichkeit> interaktionen) {
        document = parse(xmlpfad + spielstand + "/" + datei);
        personliste = new ArrayList<Person>();
        Person person;
        int temp;
        String path;
        list = document.selectNodes("//raum/person");

        //erzeugen und zur Arraylist hinzufÃ¼gen der GegenstÃ¤nde
        for (int i = 0; i < list.size(); i++) {
            int zahl = i + 1;
            path = "//person[" + zahl + "]";
            infotexte = new ArrayList<String>();
            sounds = new ArrayList<File>();
            //anzahl der punkte des polygons
            punkte = document.selectNodes(path + "/polygon/punkt");
            flaeche = new Polygon();
            //punkte dem polygon hinzufuegen
            for (int j = 0; j < punkte.size(); j++) {
                int nr = j + 1;
                flaeche.addPoint(liesInt(document, path + "/polygon/punkt[" + nr + "]/@x"),
                        liesInt(document, path + "/polygon/punkt[" + nr + "]/@y"));
            }

            //anzahl hinweistexte und anschließendes hinzufugen zur arraylist
            List list2 = document.selectNodes(path + "/hinweistext");
            for (int k = 0; k < list2.size(); k++) {
                int anzahl = k + 1;
                infotexte.add(lies(document, path + "/hinweistext[" + anzahl + "]/@text"));
                sounds.add(new File(lies(document, path + "/hinweistext[" + anzahl + "]/@sound")));
            }
            //Gegenstand der ArrayList zufÃ¼gen
            person = new Person(lies(document, path + "/@name"), new Koordinaten(liesInt(document, path + "/koordinaten/@x"),
                    liesInt(document, path + "/koordinaten/@y")),infotexte, liesBoole(document, path + "/@istvoll"),
                    liesInt(document, path + "/@breite"), liesInt(document, path + "/@hoehe"),
                    lies(document, path + "/@quelle"), new ArrayList<Gegenstand>(),
                    lies(document, path + "/@volltext"), lies(document, path + "/@leertext"),
                    liesBoole(document, path + "/@istsichtbar"), flaeche, lies(document, path + "/@quelleleerbild"),
                    new File(lies(document, path + "/@sammelSound")), sounds);
            //wenn die interaktionen nicht null sind
            if (interaktionen != null) {
                //im attribut interaktionen nicht null steht
                if (!lies(document, path + "/@interaktionsmoeglichkeit").equals("null")) {
                    //lies die stelle der interaktion
                    temp = liesInt(document, path + "/@interaktionsmoeglichkeit");
                    //ordne die interaktion an der stelle temp-1 der person zu
                    person.setInteraktionsmoeglichkeit(interaktionen.get(temp - 1));
                    //ordne der interaktion an der stelle temp-1 als ikea die person zu
                    interaktionen.get(temp - 1).setIkea(person);
                }
            }
            //wenn die person sichtbar ist, fuege sie der kontrollliste und der zeichenliste zu
            if (person.getIstSichtbar()) {
                getSpiel().getKontrollliste().add(person.getRaumbild());
                getSpiel().getImages().add(person.getRaumbild());
            }
            //fuege die person der personenliste hinzu
            personliste.add(person);
        }
        return personliste;
    }

    public ArrayList<Kombinationsmoeglichkeit> kombinationenAuslesen(String pfad) {
        document = parse(xmlpfad + "default/" + pfad);
        kombinationen = new ArrayList<Kombinationsmoeglichkeit>();
        //anzahl kombinationen
        list = document.selectNodes("//kombination");
        //kombinationen der arraylist hinzufuegen
        for (int x = 0; x < list.size(); x++) {
            kombinationen.add(new Kombinationsmoeglichkeit(lies(document, "//kombination[" + (x + 1) + "]/@text"),
                    new File(lies(document, "//kombination[" + (x + 1) + "]/@sound"))));
        }
        return kombinationen;
    }

    public ArrayList<Interaktionsmoeglichkeit> interaktionenAuslesen(String pfad) {
        document = parse(xmlpfad + "default/" + pfad);
        interaktionen = new ArrayList<Interaktionsmoeglichkeit>();
        //anzahl interaktionen
        list = document.selectNodes("//interaktion");
        //interaktionen der arraylist hinzufuegen
        for (int y = 0; y < list.size(); y++) {
            interaktionen.add(new Interaktionsmoeglichkeit(lies(document, "//interaktion[" + (y + 1) + "]/@text"),
                    liesBoole(document, "//interaktion[" + (y + 1) + "]/@zielErreicht"),
                    new File(lies(document, "//interaktion[" + (y + 1) + "]/@sound"))));
        }
        return interaktionen;
    }

    public boolean ladeSpielstand(SpielLoop spiel, String spielstand){

        XMLHandlingLesen daten = new XMLHandlingLesen(spiel);
        //raeume zuruecksetzen
        spiel.setAktuellerRaum(null);
        spiel.setAbstellkammer(null);
        spiel.setFoyer(null);
        spiel.setInnenhof(null);
        spiel.setKaminzimmer(null);
        spiel.setSpeicherraum(null);
        spiel.setHunderaum(null);

        //listen resetten
        spiel.getRaumliste().clear();
        spiel.getInventar().getGegenstaende().clear();
        spiel.getKontrollliste().clear();
        spiel.getInventarGUI().getBildliste().clear();
        spiel.getImages().clear();
        spiel.getImagesVordergrund().clear();

        //aufschriften der spielstaende laden
        for (int i = 0; i < spiel.getLadenMenue().getSpielstaende().size(); i++) {
            spiel.getLadenMenue().getSpielstaende().get(i).setAufschrift(daten.ladeSpielstandname(i+1));
            spiel.getSpeicherMenue().getSpielstaende().get(i).setAufschrift(daten.ladeSpielstandname(i+1));
        }
        //interaktionen und kombinationen einlesen
        kombinationen = daten.kombinationenAuslesen("moeglichkeiten.xml");
        interaktionen = daten.interaktionenAuslesen("moeglichkeiten.xml");

        //raumdaten einlesen und den raeumen zuordnen
        spiel.setAbstellkammer(daten.raumAuslesen(spielstand,"Raum1.xml", spiel.getSpielfigur(), spiel.getInventar(),
                kombinationen, interaktionen));

        spiel.setFoyer(daten.raumAuslesen(spielstand,"Raum2.xml", spiel.getSpielfigur(), spiel.getInventar(),
                kombinationen, interaktionen));

        spiel.setInnenhof(daten.raumAuslesen(spielstand,"Raum3.xml", spiel.getSpielfigur(), spiel.getInventar(),
                kombinationen, interaktionen));

        spiel.setKaminzimmer(daten.raumAuslesen(spielstand, "Raum4.xml", spiel.getSpielfigur(), spiel.getInventar(),
                kombinationen, interaktionen));

        spiel.setSpeicherraum(daten.raumAuslesen(spielstand, "Raum5.xml", spiel.getSpielfigur(), spiel.getInventar(),
                kombinationen, interaktionen));

        spiel.setHunderaum(daten.raumAuslesen(spielstand, "Raum6.xml", spiel.getSpielfigur(), spiel.getInventar(),
                kombinationen, interaktionen));
        //raeume der arraylist der raeume hinzufuegen
        spiel.getRaumliste().add(spiel.getAbstellkammer());
        spiel.getRaumliste().add(spiel.getFoyer());
        spiel.getRaumliste().add(spiel.getInnenhof());
        spiel.getRaumliste().add(spiel.getKaminzimmer());
        spiel.getRaumliste().add(spiel.getSpeicherraum());
        spiel.getRaumliste().add(spiel.getHunderaum());
        //startraum festlegen
        if("default".equals(spielstand))spiel.setAktuellerRaum(spiel.getAbstellkammer());
        else {
            spiel.setAktuellerRaum(spiel.getSpeicherraum());
            spiel.getKa().setAltX(724);
            spiel.getKa().setGeklicktX(724);
            spiel.getKa().setGeklicktY(500);
            spiel.getKa().setAltY(500);
        }

        daten = null;
	return true;
    }
}

