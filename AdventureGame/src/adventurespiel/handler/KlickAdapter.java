
package adventurespiel.handler;

import adventurespiel.GUI.Preloader;
import adventurespiel.fachklassen.Gegenstand;
import adventurespiel.fachklassen.Ikea;
import adventurespiel.fachklassen.Koordinaten;
import adventurespiel.fachklassen.Person;
import adventurespiel.fachklassen.Raum;
import adventurespiel.fachklassen.Tuer;
import adventurespiel.steuerung.SpielLoop;
import adventurespiel.steuerung.Bild;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import adventurespiel.fachklassen.Wegfindung;
/**
 *
 * @author Simon, Jessica, Nadine
 *
 */
public class KlickAdapter extends MouseAdapter {

    private SpielLoop spiel;
    
    private Bild angeklickt, gewaehltesBild;
    private Bild angeklicktesBild = new Bild();

    private MouseEvent maus;

    private int altX = 145;
    private int altY = 595;
    private int geklicktX = 145;
    private int geklicktY = 595;
    private int urspruenglichX, urspruenglichY;
    private int actX;
    private int actY;
    private int button;
    private int BILDDAUER = 100;
    private int bildfrequenz = 5;
    private int gewaehltesBildX, gewaehltesBildY;
    private int gewaehltesBildHoehe, gewaehltesBildBreite;
    private int bildNummer;

    private long startzeit, endzeit, gesamtzeit;
    
    private boolean controlDown;
    private boolean geklickt = false;
    private boolean bewegungFertig = false;
    private boolean klickenMoeglich = true;
    private boolean soundeinstellungenGeaendert = false;
    
    private String richtungH, richtungV;
    
    private ArrayList<File> ikeatexteSounds = new ArrayList<File>();
    private ArrayList<File> tuertexteSounds = new ArrayList<File>();
    private ArrayList<File> gegenstandstexteSounds = new ArrayList<File>();
    private ArrayList<File> henriHardSounds = new ArrayList<File>();

    private ArrayList<String> gegenstandstexte = new ArrayList<String>();
    private ArrayList<String> tuertexte = new ArrayList<String>();
    private ArrayList<String> ikeatexte = new ArrayList<String>();
    
    private AudioClip sound, sound01;

    public KlickAdapter() {
        //ArrayLists fuer verschiedene negative Ereignisse
        ikeatexte.add("Das kann ich wohl noch lange versuchen!");
        ikeatexte.add("Das ist nicht möglich!");
        ikeatexte.add("Was für ein Schwachfug!");
        ikeatexte.add("Das klappt so nicht!");
        ikeatexte.add("So wäre ich übermorgen noch dabei!");
        ikeatexteSounds.add(new File("src/sounds/henriTexte/NochLangeVersuchen.wav"));
        ikeatexteSounds.add(new File("src/sounds/henriTexte/NichtMoeglich.wav"));
        ikeatexteSounds.add(new File("src/sounds/henriTexte/Schwachfug.wav"));
        ikeatexteSounds.add(new File("src/sounds/henriTexte/KlapptSoNicht.wav"));
        ikeatexteSounds.add(new File("src/sounds/henriTexte/Uebermorgen.wav"));

        tuertexte.add("So komme ich da nicht durch!");
        tuertexte.add("So lässt sich die Tür nicht öffnen!");
        tuertexte.add("Das klappt doch nie!");
        tuertexte.add("Hah! Welch absurder Gedanke!");
        tuertexte.add("Eher kann ich die Tür aufbrechen!");
        tuertexteSounds.add(new File("src/sounds/henriTexte/SoKommeIchDaNichtDurch.wav"));
        tuertexteSounds.add(new File("src/sounds/henriTexte/SoTuerNichtOeffnen.wav"));
        tuertexteSounds.add(new File("src/sounds/henriTexte/SoKlapptDasNie.wav"));
        tuertexteSounds.add(new File("src/sounds/henriTexte/AbsurderGedanke.wav"));
        tuertexteSounds.add(new File("src/sounds/henriTexte/EherAufbrechen.wav"));

        gegenstandstexte.add("Und was soll eine solche Kombination ergeben!?");
        gegenstandstexte.add("Selbst geklebt würde das nie halten!");
        gegenstandstexte.add("Das geht beim besten Willen nicht!");
        gegenstandstexte.add("Das würde mir nicht helfen!");
        gegenstandstexte.add("Ich glaube, das ist sinnlos!");
        gegenstandstexteSounds.add(new File("src/sounds/henriTexte/WasSollKombiErgeben.wav"));
        gegenstandstexteSounds.add(new File("src/sounds/henriTexte/SelbstGeklebt.wav"));
        gegenstandstexteSounds.add(new File("src/sounds/henriTexte/BeimBestenWillenNicht.wav"));
        gegenstandstexteSounds.add(new File("src/sounds/henriTexte/NichtHelfen.wav"));
        gegenstandstexteSounds.add(new File("src/sounds/henriTexte/Sinnlos.wav"));

        henriHardSounds.add(new File("src/sounds/henriTexte/KlickGemacht.wav"));
        henriHardSounds.add(new File("src/sounds/henriTexte/SoEtwasBraucheIchNicht.wav"));
        henriHardSounds.add(new File("src/sounds/henriTexte/TascheBelastetWegDamit.wav"));
        henriHardSounds.add(new File("src/sounds/henriTexte/OstenSonneAufWestenLauf.wav"));
        henriHardSounds.add(new File("src/sounds/henriTexte/VerschlossenMondGesehen.wav"));
        henriHardSounds.add(new File("src/sounds/henriTexte/TuerIstVerschlossen.wav"));
        henriHardSounds.add(new File("src/sounds/henriTexte/GimmigerButler.wav"));

    }

    /**
     * Es wird eine Person im aktuellen Raum gesucht.
     *
     * @param name der zu suchenden Person
     * @return null, wenn keine Person mit dem gesuchten Namen gefunden wird
     */
    public Person suchePerson(String name){
        Person p = null;
        for (int x = 0; x < spiel.getAktuellerRaum().getPerson().size(); x++) {
            if (spiel.getAktuellerRaum().getPerson().get(x).getName().equals(name)) {
                p = spiel.getAktuellerRaum().getPerson().get(x);
            }
        }
        return p;
    }

    /**
     * Es wird eine Person in einem bestimmten Raum gesucht.
     *
     * @param name der zu suchenden Person
     * @param raum in dem die Person gesucht werden soll
     * @return null, wenn keine Person mit dem gesuchten Namen in dem Raum gefunden wird
     */
    public Person suchePerson(String name, Raum raum) {
        Person p = null;
        for (int x = 0; x < raum.getPerson().size(); x++) {
            if (raum.getPerson().get(x).getName().equals(name)) {
                p = raum.getPerson().get(x);
            }
        }
        return p;
    }

    /**
     * Es wird ein Ikea im aktuellen Raum gesucht.
     *
     * @param name des zu suchenden Ikeas
     * @return null, wenn kein Ikea mit dem gesuchten Namen gefunden wird
     */
    public Ikea sucheIkea(String name){
        Ikea i = null;
        for (int x = 0; x < spiel.getAktuellerRaum().getIkea().size(); x++) {
            if (spiel.getAktuellerRaum().getIkea().get(x).getName().equals(name)) {
                i = spiel.getAktuellerRaum().getIkea().get(x);
            }
        }
        return i;
    }

    /**
     * Es wird ein Ikea in einem bestimmten Raum gesucht.
     *
     * @param name des zu suchenden Ikeas
     * @param raum in dem das Ikea gesucht werden soll
     * @return null, wenn kein Ikea mit dem gesuchten Namen in dem Raum gefunden wird
     */
    public Ikea sucheIkea(String name, Raum raum){
        Ikea i = null;
        for (int x = 0; x < raum.getIkea().size(); x++) {
            if (raum.getIkea().get(x).getName().equals(name)) {
                i = raum.getIkea().get(x);
            }
        }
        return i;
    }

    /**
     * Es wird eine Tuer im aktuellen Raum gesucht.
     *
     * @param name der zu suchenden Tuer
     * @return null, wenn keine Tuer mit dem gesuchten Namen gefunden wird
     */
    public Tuer sucheTuer(String name){
        Tuer t = null;
        for (int x = 0; x < spiel.getAktuellerRaum().getTuer().size(); x++) {
            if (spiel.getAktuellerRaum().getTuer().get(x).getName().equals(name)) {
                t = spiel.getAktuellerRaum().getTuer().get(x);
            }
        }
        return t;
    }

    /**
     * Es wird ein Tuer in einem bestimmten Raum gesucht.
     *
     * @param name der zu suchenden Tuer
     * @param raum in dem die Tuer gesucht werden soll
     * @return null, wenn keine Tuer mit dem gesuchten Namen in dem Raum gefunden wird
     */
    public Tuer sucheTuer(String name, Raum raum){
        Tuer t = null;
        for (int x = 0; x < raum.getTuer().size(); x++) {
            if (raum.getTuer().get(x).getName().equals(name)) {
                t = raum.getTuer().get(x);
            }
        }
        return t;
    }

    /**
     * Es wird ein Gegenstand im aktuellen Raum gesucht.
     *
     * @param name des zu suchenden Gegenstands
     * @return null, wenn kein Gegenstand mit dem gesuchten Namen gefunden wird
     */
    public Gegenstand sucheGegenstand(String name){
        Gegenstand g = null;
        for (int x = 0; x < spiel.getInventar().getGegenstaende().size(); x++) {
            if (spiel.getInventar().getGegenstaende().get(x).getName().equals(name)) {
                g = spiel.getInventar().getGegenstaende().get(x);
            }
        }
        return g;
    }

    /**
     * Es wird ein Gegenstand in einem bestimmten Raum gesucht.
     *
     * @param name des zu suchenden Gegenstands
     * @param raum in dem der Gegenstand gesucht werden soll
     * @return null, wenn kein Gegenstand mit dem gesuchten Namen in dem Raum gefunden wird
     */
    public Gegenstand sucheGegenstand(String name, Raum raum){
        Gegenstand g = null;
        for (int x = 0; x < raum.getGegenstand().size(); x++) {
            if (raum.getGegenstand().get(x).getName().equals(name)) {
                g = raum.getGegenstand().get(x);
            }
        }
        return g;
    }

    //speichert die atuellen Koordinaten der Maus
    @Override
    public void mouseMoved(MouseEvent e) {
        setActX(e.getX());
        setActY(e.getY());
    }

    //speichert beim Drücken der Maustaste die Position in Variablen
    @Override
    public void mousePressed(MouseEvent e) {
        //hilfreiches system out um polygone zu erstellen, einfach auf der flaeche klicken
        //und output dann in die xml kopieren
        System.out.println("<punkt x=\"" + e.getX() + "\" y=\"" + e.getY() + "\" />");
        //moeglichkeit das veraendern der variablen zu verhindern über boolsche variable
        if (klickenMoeglich) {
            //gedrückte maustaste zur unterscheidung von links- und rechtsklicks
            button = e.getButton();
            //registrieren ob Strg/Ctrl- Taste gedrückt wurde
            controlDown = e.isControlDown();
            //tatsaechlich geklickte Position
            urspruenglichX = e.getX();
            urspruenglichY = e.getY();
            //zu veraendernde Punkte für die Laufbewegung
            geklicktX = e.getX();
            geklicktY = e.getY();
            //wenn man sich nicht in einem menue befindet und innerhalb des spielfeldes geklickt ht
            if (!spiel.getStatusMenue() && (urspruenglichX > spiel.getGrenzeLinks() && urspruenglichX < spiel.getGrenzeRechts()) &&
                    (urspruenglichY > spiel.getGrenzeOben() && urspruenglichY < spiel.getGrenzeUnten())) {
                //zielX auf einen Wert setzen zu dem sich die Figur bewegen darf, ohne außerhalb des spielfeldes zu landen
                if (geklicktX <= spiel.getGrenzeLinks() + 45) {
                    geklicktX = spiel.getGrenzeLinks() + 45;
                }
                if (geklicktX >= spiel.getGrenzeRechts() - 45) {
                    geklicktX = spiel.getGrenzeRechts() - 45;
                }
                //zielY auf einen Wert setzen zu dem sich die Figur bewegen darf, ohne außerhalb des spielfeldes zu landen
                if (geklicktY >= spiel.getGrenzeUnten() - 10) {
                    geklicktY = spiel.getGrenzeUnten() - 10;
                }
                if (geklicktY <= spiel.getGrenzeOben() + 195) {
                    geklicktY = spiel.getGrenzeOben() + 195;
                }
                //solange der wert vom ziely sich nicht innerhalb eines begehbaren feldes der gehflaeche befindet, erhoehe ihn um 1
                while (!spiel.getAktuellerRaum().getWegfindung().isBegehbar(geklicktX, geklicktY) ||
                        geklicktY <= spiel.getAktuellerRaum().getWegfindung().getStarty()) {
                    geklicktY++;
                }
                //solange der wert vom zielx sich nicht innerhalb eines begehbaren feldes der gehflaeche befindet, erhoehe ihn um 1
                while (!spiel.getAktuellerRaum().getWegfindung().isBegehbar(geklicktX, geklicktY) ||
                        geklicktX <= spiel.getAktuellerRaum().getWegfindung().getStartx()) {
                    geklicktX++;
                }
                //solange der wert vom zielx sich nicht innerhalb eines begehbaren feldes der gehflaeche befindet, reduziere ihn um 1
                while (!spiel.getAktuellerRaum().getWegfindung().isBegehbar(geklicktX, geklicktY) ||
                        geklicktX >= spiel.getAktuellerRaum().getWegfindung().getStartx() +
                        spiel.getAktuellerRaum().getWegfindung().getBreite()) {
                    geklicktX--;
                }
                //solange der wert vom ziely sich nicht innerhalb eines begehbaren feldes der gehflaeche befindet, reduziere ihn um 1
                while (!spiel.getAktuellerRaum().getWegfindung().isBegehbar(geklicktX, geklicktY) ||
                        geklicktY >= spiel.getAktuellerRaum().getWegfindung().getStarty() +
                        spiel.getAktuellerRaum().getWegfindung().getHoehe()) {
                    geklicktY--;
                }

            }
            maus = e;
            geklickt = true;

            //bei Klick auf Bild, Datenspeicherung
            if (getGeklicktesBild(urspruenglichX, urspruenglichY, spiel.getKontrollliste()) != null) {
                gewaehltesBild = getGeklicktesBild(urspruenglichX, urspruenglichY, spiel.getKontrollliste());
                angeklicktesBild = gewaehltesBild;
                gewaehltesBildX = gewaehltesBild.getKoordinaten().getXKoordinate();
                gewaehltesBildY = gewaehltesBild.getKoordinaten().getYKoordinate();
                gewaehltesBildBreite = gewaehltesBild.getBreite();
                gewaehltesBildHoehe = gewaehltesBild.getHoehe();

            }
            if (getGeklicktesBild(urspruenglichX, urspruenglichY, spiel.getKontrollliste()) == null) {
                gewaehltesBild = null;
                angeklicktesBild = null;
                gewaehltesBildX = 0;
                gewaehltesBildY = 0;
                gewaehltesBildBreite = 0;
                gewaehltesBildHoehe = 0;
            }
        }
    }

    //funktion entscheidet, ob funktionen innerhalb des spiels oder innerhalb eines menues ausgefuehrt werden sollen
    public void KlickListener() {

        //Klickabfragen im Menue
        if (spiel.getStatusMenue() == true) {
            if (geklickt) {
                geklickt = false;
                menueKlickListener();
            }
        }

        //Klickabfragen im Spiel
        if (spiel.getStatusMenue() == false) {
            if (geklickt) {
                geklickt = false;
                //1.) wenn im Spielbildschirm geklickt wurde. generell wird ersteinmal die spielfigur in bewegung gesetzt. wenn sie am geklickten punkt angekommen ist
                //wird abgefragt, ob sie auf einem bild steht. wenn ja, dann wird mausereignis ausgeführt
                if ((urspruenglichX > spiel.getGrenzeLinks() && urspruenglichX < spiel.getGrenzeRechts())
                        && (urspruenglichY > spiel.getGrenzeOben() && urspruenglichY < spiel.getGrenzeUnten())) {
                    Spielfigurbewegung(geklicktX, geklicktY);
                } //2.) wenn im Inventar geklickt wurde
                else if ((urspruenglichX > 25 && urspruenglichX < 995) && (urspruenglichY > 655 && urspruenglichY < 735)) {
                    InventarKlick();
                } //3.) wenn irgendwo im weißen Feld geklickt wurde
                else {
                    Zuruecksetzen();
                }
                spiel.getSchrift().setVisible(true);
            }
        }

    }

    //Klick ins Spielfeld
    public void Mausereignis(){
        Ikea ikea = null;
        //1) Klick innerhalb des Raumbilds
        //Textfeld wird geleert
        spiel.getSchrift().setText("");
        //Bild, auf das geklickt wurde, wird ermittelt
        Bild gewaehltesBild = getGeklicktesBild(urspruenglichX, urspruenglichY, spiel.getKontrollliste());
        angeklicktesBild = gewaehltesBild;
        //1.1) Wenn auf ein Bild geklickt wurde
        if (gewaehltesBild != null) {
            //Wenn der Klick innerhalb der Flaeche ist
            if (gewaehltesBild.getFlaeche().contains(urspruenglichX, urspruenglichY)) {
                //1.1.1) Wenn zu dem gewaehlten Bild ein Gegenstand gehoert
                if (gewaehltesBild.getGegenstand() != null) {
                    //Bild aus der Zeichenliste des Raums entfernen
                    spiel.getImages().remove(gewaehltesBild);
                    spiel.getImagesVordergrund().remove(gewaehltesBild);
                    //Bild aus der Liste der Raumkontrollliste entfernen
                    spiel.getKontrollliste().remove(gewaehltesBild);
                    //Gegenstand aus der Gegenstandsliste des Raumes entfernen
                    spiel.getAktuellerRaum().getGegenstand().remove(gewaehltesBild.getGegenstand());
                    //Gegenstand dem Inventar hinzufuegen
                    spiel.getInventar().addGegenstand(gewaehltesBild.getGegenstand());
                    //Gegenstand der Zeichenliste des Inventars hinzufuegen
                    spiel.getInventarGUI().getBildliste().add(gewaehltesBild.getGegenstand().getInventarbild());
                    //Text ausgeben
                    spiel.getSchrift().setText("Ich habe den Gegenstand '" + gewaehltesBild.getGegenstand().getName() + "' eingesammelt.");
                    //neu zeichnen
                    spiel.repaint();
                    //sound abspielen
                    soundAbspielen(gewaehltesBild.getGegenstand().getSammelSound());
                }
                //1.1.2) Wenn zu dem gewaehlten Bild ein Ikea gehoert
                if (gewaehltesBild.getIkea() != null) {
                    //1.1.2.1) Wenn keine Interaktion noetig ist und ein Sonderfall besteht
                    //Sonderfall: Wenn auf das Speicherbuch geklickt wurde (Speichern)
                    if (gewaehltesBild.getName().equals("Speicherbuch")) {
                        spiel.getRaumsteuerung().wechselnMenue(spiel.getSpeicherMenue().getSpeichermenueBilder());
                        for (int i = 0; i < spiel.getSpeicherMenue().getSpielstaende().size(); i++) {
                            spiel.add(spiel.getSpeicherMenue().getSpielstaende().get(i));
                            spiel.getSpeicherMenue().getSpielstaende().get(i).repaint();
                        }
                        spiel.repaint();
                    }
                    //Sonderfall: Wenn auf das Sofa geklickt wird (Hauptmenue)
                    if (gewaehltesBild.getName().equals("Sofa")) {
                        spiel.getHauptmenue().add(6, spiel.getBtnWeiterspielen());
                        spiel.getRaumsteuerung().wechselnMenue(spiel.getHauptmenue());
                        spiel.getRaumsteuerung().setStandardsound(true);
                        spiel.repaint();
                    }
                    //Sonderfall: Regal in Raum 2
                    //Wenn auf das Buecherregal2 geklickt wurde und es leer ist
                    if (gewaehltesBild.getIkea().getName().equals("Bücherregal2") && gewaehltesBild.getIkea().getIstVoll() == false) {
                        //der 2. Hinweistext wird ausgegeben
                        spiel.getSchrift().setText(gewaehltesBild.getIkea().getHinweistext().get(1));
                        soundAbspielen(gewaehltesBild.getIkea().getInfosound());
                    }
                    //Sonderfall: Augenklappe in Regal in Raum 2
                    //Wenn auf das Buecherregal2 geklickt wurde und es voll ist
                    if (gewaehltesBild.getIkea().getName().equals("Bücherregal2") && gewaehltesBild.getIkea().getIstVoll() == true) {
                        //fuege Gegenstand und Gegenstandsbild in die Listen des Inventars hinzu
                        spiel.getInventar().getGegenstaende().add(sucheGegenstand("Augenklappe", spiel.getFoyer()));
                        spiel.getInventarGUI().getBildliste().add(sucheGegenstand("Augenklappe", spiel.getFoyer()).getInventarbild());
                        //setze Ikea auf leer
                        gewaehltesBild.getIkea().setIstVoll(false);
                        //der 1. Hinweistext wird ausgegeben
                        spiel.getSchrift().setText(gewaehltesBild.getIkea().getHinweistext().get(0));
                        soundAbspielen(gewaehltesBild.getIkea().getSammelSound(), new File("src/sounds/ikeahinweissounds/raum2/buecherregal2.wav"));
                    }
                    //1.1.2.2)Wenn eine Interaktion noetig (und es ein Sonderfall ist)
                    //Wenn das Ikea voll ist, eine Interaktion moeglich ist und etwas im Inventar ausgewaehlt ist
                    if (gewaehltesBild.getIkea().getIstVoll() == true && gewaehltesBild.getIkea().getInteraktionsmoeglichkeit() != null && angeklickt != null) {
                        //Wenn Ikea mit dem Gegenstand interagieren kann
                        if (gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().istInteragierbar(angeklickt.getGegenstand(), gewaehltesBild.getIkea())) {
                            //Wenn die Interaktion das Zielerreichen ausloest
                            if (gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getZielErreicht()) {
                                //setze StatusMenue auf true, damit nach dem Outro das Hauptmenue angezeigt wird
                                spiel.setStatusMenue(true);
                                //loesche alle Bilder aus allen Listen und entferne den ausgegebenen Text und zeichne neu
                                spiel.getSchrift().setVisible(false);
                                spiel.getImages().clear();
                                spiel.getImagesVordergrund().clear();
                                spiel.getKontrollliste().clear();
                                spiel.repaint();
                                //spiele Outro ab
                                soundAbspielen(spiel.getOutroSound());
                                System.out.println(spiel.getOutroSound().getName());
                                spiel.getRaumsteuerung().spieleAnimation(spiel.getOutroBilder(), "outro");
                                //beende Mausereignis
                                return;
                            }
                            //Sonderfall in Raum 1: Interaktion Schreichholzschachtel mit Kerzen
                            //Wenn man Streichhoelzer auf einer der vier Kerzen anwendet
                            if (angeklickt.getGegenstand().getName().equals("Streichholzschachtel") && gewaehltesBild.getIkea().getName().equals("KerzeNord")
                                    || gewaehltesBild.getIkea().getName().equals("KerzeOst1") || gewaehltesBild.getIkea().getName().equals("KerzeSüd1")
                                    || gewaehltesBild.getIkea().getName().equals("KerzeWest")) {
                                //gehe Ikealiste des aktuellen Raums durch
                                for (int i = 0; i < spiel.getAktuellerRaum().getIkea().size(); i++) {
                                    //wenn es das gewaehlte Bild mit dem Zusatz brennend gibt, so speichere dieses in der Variable ikea
                                    if (spiel.getAktuellerRaum().getIkea().get(i).getName().equals(gewaehltesBild.getIkea().getName() + "Brennend")) {
                                        ikea = spiel.getAktuellerRaum().getIkea().get(i);
                                    }
                                }            
                                //Interaktionstextext ausgeben
                                spiel.getSchrift().setText(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getInteraktionstext());
                                //Sound abspielen 
                                soundAbspielen(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getSound());
                                //das soeben gesuchte ikea sichtbar machen
                                ikea.setIstSichtbar(true);
                                //wenn es sich um die Kerze im Vordergrund handelt, fuege sie der Vordergrundbildliste hinzu
                                if (ikea.getName().equals("KerzeOst1Brennend")) {
                                    spiel.getImagesVordergrund().add(ikea.getRaumbild());
                                } else {
                                    //ansonsten fuege sie der "normalen" Bildliste hinzu
                                    spiel.getImages().add(ikea.getRaumbild());
                                }
                                //fuege das Bild der Raumkontrollliste hinzu
                                spiel.getKontrollliste().add(ikea.getRaumbild());
                                //setze das gewaehlte Bild auf unsichtbar
                                gewaehltesBild.getIkea().setIstSichtbar(false);
                                //Bild aus der Zeichenliste des Raums entfernen
                                spiel.getImages().remove(gewaehltesBild);
                                //Bild aus der Liste der Raumkontrollliste entfernen
                                spiel.getKontrollliste().remove(gewaehltesBild);
                                //angeklickt auf null setzen und neuzeichnen
                                angeklickt = null;
                                spiel.repaint();
                                //Wenn alle Kerzen brennen außer die noerdliche
                                if (sucheIkea("KerzeNord").getIstSichtbar() == true && sucheIkea("KerzeOst1Brennend").getIstSichtbar() == true
                                        && sucheIkea("KerzeSüd1Brennend").getIstSichtbar() == true && sucheIkea("KerzeWestBrennend").getIstSichtbar() == true) {
                                    //setze Tuer auf leer, um Raumwechsel zu aktivieren
                                    sucheTuer("Abstellkammertür").setIstVoll(false);
                                    //Sound abspielen                      
                                    //Text ausgeben
                                    spiel.getSchrift().setText("Was ist das für ein Geräusch? Irgendwo hat es Klick gemacht!");
                                    soundAbspielen(henriHardSounds.get(0));
                                }
                                //hier bei Sonderfall Kerze Mausereignis beenden
                                return;
                            }
                            //Sonderfall in Raum 1: Interaktion Fingerhut mit brennenden Kerzen
                            if (angeklickt.getGegenstand().getName().equals("Fingerhut") && gewaehltesBild.getIkea().getName().equals("KerzeNordBrennend")
                                    || gewaehltesBild.getIkea().getName().equals("KerzeOst1Brennend") || gewaehltesBild.getIkea().getName().equals("KerzeSüd1Brennend")
                                    || gewaehltesBild.getIkea().getName().equals("KerzeWestBrennend")) {
                                //gehe Ikealiste des aktuellen Raums durch
                                for (int i = 0; i < spiel.getAktuellerRaum().getIkea().size(); i++) {
                                    //wenn es das gewaehlte Bild OHNE Zusatz brennend gibt, so speichere dieses in der Variable ikea
                                    if (spiel.getAktuellerRaum().getIkea().get(i).getName().equals(gewaehltesBild.getIkea().getName().substring(0, 9))) {
                                        ikea = spiel.getAktuellerRaum().getIkea().get(i);
                                    }
                                }
                                //Interaktionstext ausgeben
                                spiel.getSchrift().setText(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getInteraktionstext());
                                soundAbspielen(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getSound());
                                //das soeben gesuchte ikea sichtbar machen
                                ikea.setIstSichtbar(true);
                                //Bild der Bilderliste hinzufuegen
                                spiel.getImages().add(ikea.getRaumbild());
                                //Bild der Raumkontrollliste hinzugfuegen
                                spiel.getKontrollliste().add(ikea.getRaumbild());
                                //gewaehltes Bild unsichtbar machen
                                gewaehltesBild.getIkea().setIstSichtbar(false);
                                //wenn es sich um die Kerze im Vordergrund handelt, entferne sie aus der Vordergrundbildliste
                                if (gewaehltesBild.getName().equals("KerzeOst1Brennend")) {
                                    spiel.getImagesVordergrund().remove(gewaehltesBild);
                                } else {
                                    //sonst: Bild aus der "normalen" Zeichenliste des Raums entfernen
                                    spiel.getImages().remove(gewaehltesBild);
                                }
                                //Bild aus der Liste der Raumkontrollliste entfernen
                                spiel.getKontrollliste().remove(gewaehltesBild);
                                //alles im Inventar abwaehlen und neuzeichnen
                                angeklickt = null;
                                spiel.repaint();
                                //Wenn alle Kerzen brennen außer die noerdliche
                                if (sucheIkea("KerzeNord").getIstSichtbar() == true && sucheIkea("KerzeOst1Brennend").getIstSichtbar() == true
                                        && sucheIkea("KerzeSüd1Brennend").getIstSichtbar() == true && sucheIkea("KerzeWestBrennend").getIstSichtbar() == true) {
                                    //setze Tuer auf leer, um Raumwechsel zu aktivieren
                                    sucheTuer("Abstellkammertür").setIstVoll(false);
                                    //Sound abspielen
                                    soundAbspielen(sucheTuer("Abstellkammertür").getSammelSound());
                                    //Text ausgeben
                                    spiel.getSchrift().setText("Was ist das für ein Geräusch? Irgendwo hat es Klick gemacht!");
                                    soundAbspielen(henriHardSounds.get(0));
                                }
                                //sound kerze nord ist erloschen ausgeben (wenn kerze nord erloschen wurde aber noch nicht alle anderen kerzen entzündet wurden)
                                if (sucheIkea("KerzeNord").getIstSichtbar() == true && (sucheIkea("KerzeOst1Brennend").getIstSichtbar() == false 
                                        || sucheIkea("KerzeSüd1Brennend").getIstSichtbar() == false || sucheIkea("KerzeWestBrennend").getIstSichtbar() == false)) {
                                    spiel.getSchrift().setText(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getInteraktionstext());
                                    soundAbspielen(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getSound());
                                }
                                //hier bei Sonderfall Kerze Mausereignis beenden
                                return;
                            }
                            //Sonderfall in Raum 4: Interaktion Schraubenschlüssel mit Feuer1
                            //Wenn man Kerzenstummel mit Feuer1 interagiert
                            if (angeklickt.getGegenstand().getName().equals("Kerzenstummel") && gewaehltesBild.getIkea().getName().equals("Feuer1")) {
                                //gewaehltesBild unsichtbar setzen und aus Listen entfernen
                                gewaehltesBild.getIkea().setIstSichtbar(false);
                                spiel.getImages().remove(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getKontrollliste().remove(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getAktuellerRaum().getIkea().remove(gewaehltesBild.getIkea());
                                spiel.getKaminzimmer().getIkea().remove(sucheIkea("Feuer1", spiel.getKaminzimmer()));
                                //setze anderes Ikea auf sichtbar und fuege es den Listen hinzu
                                sucheIkea("Feuer2").setIstSichtbar(true);
                                spiel.getImages().add(sucheIkea("Feuer2").getRaumbild());
                                spiel.getKontrollliste().add(sucheIkea("Feuer2").getRaumbild());
                            }
                            //Sonderfall in Raum 4: Interaktion gefuellte Wasserflasche mit Feuer2
                            if (angeklickt.getGegenstand().getName().equals("Volle Flasche") && gewaehltesBild.getIkea().getName().equals("Feuer2")) {
                                //setze Ikea auf unsichtbar und entferne es aus allen Listen
                                gewaehltesBild.getIkea().setIstSichtbar(false);
                                spiel.getImages().remove(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getKontrollliste().remove(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getAktuellerRaum().getIkea().remove(gewaehltesBild.getIkea());
                                spiel.getKaminzimmer().getIkea().remove(sucheIkea("Feuer2", spiel.getKaminzimmer()));
                                //fuege eine Tuer den Listen hinzu und setze diese sichtbar
                                spiel.getImages().add(sucheTuer("Kaminzimmergeheimtür").getRaumbild());
                                spiel.getKontrollliste().add(sucheTuer("Kaminzimmergeheimtür").getRaumbild());
                                sucheTuer("Kaminzimmergeheimtür").setIstSichtbar(true);
                            }
                            //Sonderfall in Raum 3: Interaktion Schraubenschluessel mit defektem Wasserhahn
                            if (angeklickt.getGegenstand().getName().equals("Schraubenschlüssel") && gewaehltesBild.getIkea().getName().equals("Defekter Wasserhahn")) {
                                //setze anderes Ikea auf sichtbar und fuege es den Listen hinzu
                                sucheIkea("Wasserhahn").setIstSichtbar(true);
                                spiel.getImages().add(sucheIkea("Wasserhahn").getRaumbild());
                                spiel.getKontrollliste().add(sucheIkea("Wasserhahn").getRaumbild());
                                //mache gewaehltes Bild unsichtbar und entferne es aus den Listen
                                gewaehltesBild.getIkea().setIstSichtbar(false);
                                spiel.getImages().remove(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getKontrollliste().remove(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getAktuellerRaum().getIkea().remove(gewaehltesBild.getIkea());
                            }
                            //entferne angeklicktes Bild aus Inventar
                            spiel.getInventarGUI().bildliste.remove(angeklickt);
                            spiel.getInventar().getGegenstaende().remove(angeklickt.getGegenstand());
                            //Interaktionstext ausgeben
                            spiel.getSchrift().setText(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getInteraktionstext());
                            soundAbspielen(gewaehltesBild.getIkea().getSammelSound(), gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getSound());
                            //Gegenstand in Inventar abwaehlen
                            angeklickt = null;
                            //Wenn eine Interaktion einen neuen Gegenstand hervorbringt
                            if (gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getNeuerGegenstand() != null) {
                                //fuege diesen dem Inventar hinzu
                                spiel.getInventar().addGegenstand(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getNeuerGegenstand());
                                spiel.getInventarGUI().getBildliste().add(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getNeuerGegenstand().getInventarbild());
                            } 
                            //wenn ein Leerbild vorhanden ist, tausche das Bild aus
                            if (gewaehltesBild.getIkea().getQuelleLeerBild() != null) {
                                spiel.getImages().remove(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getKontrollliste().remove(gewaehltesBild.getIkea().getRaumbild());
                                gewaehltesBild.getIkea().setRaumbild(gewaehltesBild.getIkea().getQuelleLeerBild());
                                spiel.getImages().add(gewaehltesBild.getIkea().getRaumbild());
                                spiel.getKontrollliste().add(gewaehltesBild.getIkea().getRaumbild());
                            } 
                            //Spiel neuzeichnen und gewaehltes Ikea auf leer setzen, damit keine Interaktion mehr moeglich ist
                            spiel.repaint();
                            gewaehltesBild.getIkea().setIstVoll(false);
                        } else {
                            //wenn die Interaktion nicht mit dem richtigen Gegenstand versucht wird
                            //Zufallszahl im Bereich der ArrayListgroesse
                            int zufall = (int) (Math.random() * (ikeatexte.size()));
                            //Zufallstext ausgeben
                            spiel.getSchrift().setText(ikeatexte.get(zufall));
                            soundAbspielen(ikeatexteSounds.get(zufall));
                            //Gegenstand im Inventar abwaehlen und erneut zeichnen
                            angeklickt = null;
                            spiel.repaint();
                        }
                    }
                    //wenn das Ikea noch voll ist, also man noch interagieren kann und eine Interaktion moeglich ist, man jedoch nichts im Inventar angewaehlt hat
                    //gebe den Hinweistext an 1. Stelle aus
                    else if (gewaehltesBild.getIkea().getIstVoll() == true && gewaehltesBild.getIkea().getInteraktionsmoeglichkeit() != null && angeklickt == null) {
                        spiel.getSchrift().setText(gewaehltesBild.getIkea().getHinweistext().get(0));
                        soundAbspielen(gewaehltesBild.getIkea().getInfosound());
                    } else if (gewaehltesBild.getIkea().getIstVoll() == false && angeklickt != null) {
                        //wenn die Interaktion nicht mit dem richtigen Gegenstand versucht wird
                        //Zufallszahl im Bereich der ArrayListgroesse
                        int zufall = (int) (Math.random() * (ikeatexte.size()));
                        //Zufallstext ausgeben
                        spiel.getSchrift().setText(ikeatexte.get(zufall));
                        soundAbspielen(ikeatexteSounds.get(zufall));
                        //Gegenstand im Inventar abwaehlen und erneut zeichnen
                        angeklickt = null;
                        spiel.repaint();
                    }
                    //1.1.2.2.2)Wenn keine Interaktion noetig ist und das Ikea voll ist
                    if (gewaehltesBild.getIkea().getIstVoll() == true && gewaehltesBild.getIkea().getInteraktionsmoeglichkeit() == null) {
                        // fuege neuen Gegenstand dem Inventar hinzu
                        spiel.getInventar().addGegenstand(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getNeuerGegenstand());
                        spiel.getInventarGUI().getBildliste().add(gewaehltesBild.getIkea().getInteraktionsmoeglichkeit().getNeuerGegenstand().getInventarbild());
                        //Sound ausgeben
                        soundAbspielen(gewaehltesBild.getIkea().getSammelSound());
                        //gebe von der ersten Haelfte der Arraytexte einen Zufallstext aus
                        int zufall = (int) (Math.random() * (gewaehltesBild.getIkea().getHinweistext().size() / 2));
                        spiel.getSchrift().setText(gewaehltesBild.getIkea().getHinweistext().get(zufall));
                        //setze Ikea auf leer
                        gewaehltesBild.getIkea().setIstVoll(false);
                    } else {
                        //int zufall = (int) (Math.random() * (gewaehltesBild.getIkea().getHinweistext().size()/2));
                        // spiel.getSchrift().setText(gewaehltesBild.getIkea().getHinweistext().get(zufall+(gewaehltesBild.getIkea().getHinweistext().size()/2)));
                        }
                }
                //1.1.3) Wenn zu dem gewaehlten Bild eine Person gehoert
                if (gewaehltesBild.getPerson() != null) {
                    //geklickte Person wird in eine temporaere Variable geschrieben
                    Person person = gewaehltesBild.getPerson();
                    //Bewegung in die Naehe der Person
                    geklicktX = person.getRaumbild().getKoordinaten().getXKoordinate() + 130;
                    geklicktY = person.getRaumbild().getKoordinaten().getYKoordinate() + 40;
                    //1.1.3.1) Wenn es sich bei der Person um den Butler handelt
                    if (person.getName().equals("Butler")) {
                        //Zufallszahl im Bereich der ArrayListgroesse
                        int zufall = (int) (Math.random() * (person.getHinweistext().size()));
                        //Zufallstext ausgeben
                        spiel.getSchrift().setText(person.getHinweistext().get(zufall));
                        //Zufallssound ausgeben
                        soundAbspielen(person.getSounds().get(zufall));
                        angeklickt = null;
                    }
                    //1.1.3.2) Wenn es eine andere Person ist (die Frau)
                    else {
                        //wenn die Person voll ist
                        if (gewaehltesBild.getPerson().getIstVoll() == true) {
                            //Volltext und -sound ausgeben
                            spiel.getSchrift().setText(gewaehltesBild.getPerson().getVollText());
                            soundAbspielen(gewaehltesBild.getPerson().getSounds().get(0));
                        } else {
                            //wenn die Person leer ist
                            //Leertext und -sound ausgeben
                            spiel.getSchrift().setText(gewaehltesBild.getPerson().getLeerText());
                            soundAbspielen(gewaehltesBild.getPerson().getSounds().get(2));
                        }
                        //wenn die Person voll ist, eine Ikteraktion moeglich und im Inventar ein Gegenstand angewaehlt ist
                        if (gewaehltesBild.getPerson().getIstVoll() == true && gewaehltesBild.getPerson().getInteraktionsmoeglichkeit() != null && angeklickt != null) {
                            //Wenn sich der Gegenstand zu der Persond passt und eine Interaktion moeglich ist
                            if (gewaehltesBild.getPerson().getInteraktionsmoeglichkeit().istInteragierbar(angeklickt.getGegenstand(), gewaehltesBild.getPerson())) {
                                //enferne den Gegenstand aus dem Inventar
                                spiel.getInventarGUI().bildliste.remove(angeklickt);
                                spiel.getInventar().getGegenstaende().remove(angeklickt.getGegenstand());
                                //Interaktionstext ausgeben
                                spiel.getSchrift().setText(gewaehltesBild.getPerson().getInteraktionsmoeglichkeit().getInteraktionstext());
                                soundAbspielen(gewaehltesBild.getPerson().getInteraktionsmoeglichkeit().getSound());
                                //neuen Gegenstand dem Inventar hinzufuegen
                                spiel.getInventar().addGegenstand(gewaehltesBild.getPerson().getInteraktionsmoeglichkeit().getNeuerGegenstand());
                                spiel.getInventarGUI().getBildliste().add(gewaehltesBild.getPerson().getInteraktionsmoeglichkeit().getNeuerGegenstand().getInventarbild());
                                //Sounds ausgeben
                                //soundAbspielen(gewaehltesBild.getPerson().getSammelSound());
                                soundAbspielen(gewaehltesBild.getPerson().getSammelSound(), gewaehltesBild.getPerson().getSounds().get(1));
                                //Gegenstand im Inventar abwaehlen
                                angeklickt = null;
                                //wenn ein Leerbild vorhanden ist, tausche das Bild aus
                                if (gewaehltesBild.getPerson().getQuelleLeerBild() != null) {
                                    spiel.getImages().remove(gewaehltesBild.getPerson().getRaumbild());
                                    spiel.getKontrollliste().remove(gewaehltesBild.getPerson().getRaumbild());
                                    gewaehltesBild.getPerson().setRaumbild(gewaehltesBild.getPerson().getQuelleLeerBild());
                                    spiel.getImages().add(gewaehltesBild.getPerson().getRaumbild());
                                    spiel.getKontrollliste().add(gewaehltesBild.getPerson().getRaumbild());
                                }
                                //wenn der neue Gegenstand ein Diadem ist
                                if (gewaehltesBild.getPerson().getInteraktionsmoeglichkeit().getNeuerGegenstand().getName().equals("Diadem")) {
                                    //suche spezielles Ikea in anderem Raum und setze dieses auf sichtbar
                                    sucheIkea("Bücherregal2", spiel.getFoyer()).setIstSichtbar(true);
                                    //setze altes Ikea an gleicher Stelle auf unsichtbar und entferne dieses aus den Listen
                                    sucheIkea("Bücherregal1", spiel.getFoyer()).setIstVoll(false);
                                    sucheIkea("Bücherregal1", spiel.getFoyer()).setIstSichtbar(false);
                                    spiel.getImages().remove(sucheIkea("Bücherregal1", spiel.getFoyer()).getRaumbild());
                                    spiel.getKontrollliste().remove(sucheIkea("Bücherregal1", spiel.getFoyer()).getRaumbild());
                                    spiel.getAktuellerRaum().getIkea().remove(sucheIkea("Bücherregal1", spiel.getFoyer()));
                                }
                                //zeichne Spiel neu und setze Person auf leer
                                spiel.repaint();
                                gewaehltesBild.getPerson().setIstVoll(false);
                            } else {
                                //wenn nicht passender Gegenstand mit Person interagiert wird
                                //Text ausgeben
                                spiel.getSchrift().setText("So etwas brauche ich nicht!");
                                soundAbspielen(henriHardSounds.get(1));
                                angeklickt = null;
                                spiel.repaint();
                            }
                        }
                    }
                }
                //1.1.4) Wenn zu dem gewaehlten Bild eine Tuer gehoert
                if (gewaehltesBild.getTuer() != null) {
                    //wenn die Tuer leer ist, also keine Interaktion noetig ist
                    if (gewaehltesBild.getTuer().getIstVoll() == false) {
                        setKlickenMoeglich(false);
                        //Position der Figur ändern um sie an der richtigen Stelle zu zeichnen
                        setUrspruenglichX(gewaehltesBild.getTuer().getFigurposition().getXKoordinate());
                        setUrspruenglichY(gewaehltesBild.getTuer().getFigurposition().getYKoordinate());
                        setGeklicktX(gewaehltesBild.getTuer().getFigurposition().getXKoordinate());
                        setGeklicktY(gewaehltesBild.getTuer().getFigurposition().getYKoordinate());
                        setAltX(gewaehltesBild.getTuer().getFigurposition().getXKoordinate());
                        setAltY(gewaehltesBild.getTuer().getFigurposition().getYKoordinate());
                        //fuehre wechselnZu- Methode aus
                        spiel.getRaumsteuerung().wechselnZu(gewaehltesBild.getTuer());
                        //alles im Inventar abwaehlen und neuzeichnen
                        angeklickt = null;
                        spiel.repaint();
                        setKlickenMoeglich(true);
                        //Sonderfall in Raum 1: Abstellkammertuer
                        if (gewaehltesBild.getTuer().getName().equals("Abstellkammertür")) {
                            //Variablen erstellen
                            Gegenstand fh = sucheGegenstand("Fingerhut");
                            Gegenstand sh = sucheGegenstand("Streichholzschachtel");
                            //wenn die Variablen nicht gleich null sind
                            if (fh != null && sh != null) {
                                //entferne die Gegenstaende aus dem Inventar und aus der Raumbildliste
                                spiel.getInventar().removeGegenstand(fh);
                                spiel.getInventarGUI().getBildliste().remove(fh.getInventarbild());
                                spiel.getImages().remove(fh.getRaumbild());
                                spiel.getInventar().removeGegenstand(sh);
                                spiel.getInventarGUI().getBildliste().remove(sh.getInventarbild());
                                spiel.getImages().remove(sh.getRaumbild());
                                //Text ausgeben
                                spiel.getSchrift().setText("Ein paar Dinge haben nur meine Taschen belastet! Weg damit!");
                                soundAbspielen(henriHardSounds.get(2));
                            }
                        }
                        //wenn Tuer voll ist
                        } else {
                        //und ein Gegenstand im Inventar angewaehlt ist
                        if (angeklickt != null) {
                            //wenn eine Interaktionsmoeglichkeit vorhanden ist
                            if (gewaehltesBild.getTuer().getInteraktionsmoeglichkeit() != null) {
                                //wenn man mit dem gewaehlten Gegenstand und der Tuer interagieren kann
                                if (gewaehltesBild.getTuer().getInteraktionsmoeglichkeit().istInteragierbar(angeklickt.getGegenstand(), gewaehltesBild.getTuer())) {
                                    //entferne Gegenstand aus dem Inventar
                                    spiel.getInventarGUI().bildliste.remove(angeklickt);
                                    spiel.getInventar().getGegenstaende().remove(angeklickt.getGegenstand());
                                    //Interaktionstext ausgeben
                                    spiel.getSchrift().setText(gewaehltesBild.getTuer().getInteraktionsmoeglichkeit().getInteraktionstext());
                                    soundAbspielen(gewaehltesBild.getTuer().getInteraktionsmoeglichkeit().getSound());
                                    //Tuer auf leer setzen, was Raumwechsel ermoeglicht
                                    gewaehltesBild.getTuer().setIstVoll(false);
                                    spiel.repaint();
                                    //wenn Interaktionsmoeglichkeit nicht mit Gegenstand funktioniert
                                    } else {
                                    //Zufallszahl im Bereich der ArrayListgroesse
                                    int zufall = (int) (Math.random() * (tuertexte.size()));
                                    //Zufallstext ausgeben
                                    spiel.getSchrift().setText(tuertexte.get(zufall));
                                    soundAbspielen(tuertexteSounds.get(zufall));
                                    //Gegenstand in Inventar abwaehlen und neuzeichnen
                                    angeklickt = null;
                                    spiel.repaint();
                                }
                                //wenn keine Interaktionsmoeglichkeit vorhanden ist
                                } else {
                                //Zufallszahl im Bereich der ArrayListgroesse
                                int zufall = (int) (Math.random() * (tuertexte.size()));
                                //Zufallstext ausgeben
                                spiel.getSchrift().setText(tuertexte.get(zufall));
                                soundAbspielen(tuertexteSounds.get(zufall));
                                //Gegenstand in Inventar abwaehlen und neuzeichnen
                                angeklickt = null;
                                spiel.repaint();
                            }
                            //Wenn nichts im Inventar ausgewaehlt ist 
                            } else {
                            //und es sich um den Sonderfall Abstellkammertuer aus Raum 1 handelt
                            if (gewaehltesBild.getTuer().getName().equals("Abstellkammertür")) {
                                //Text ausgeben
                                spiel.getSchrift().setText("Hier steht: Im Osten geht die Sonne auf, im Süden nimmt sie ihren Lauf, " +
                                        "im Westen wird sie untergehen, im Norden ist sie nie zu sehen.");
                                soundAbspielen(henriHardSounds.get(3));
                                //und es sich um den Sonderfall Kaminzimmer aus Raum 2 handelt
                                } else if (gewaehltesBild.getTuer().getName().equals("Kaminzimmertür")) {
                                //Text ausgeben
                                spiel.getSchrift().setText("Die Tür ist verschlossen! Mh... diesen Mond habe ich schon irgendwo gesehen!");
                                soundAbspielen(henriHardSounds.get(4));
                                //sonst diesen Text ausgeben
                                } else {
                                spiel.getSchrift().setText("Die Tür ist verschlossen!");
                                soundAbspielen(henriHardSounds.get(5));
                            }
                        }
                    }
                }
            }
        }
        //wenn das gewaehlteBild null ist
        else {
            //Textfeld leeren
            spiel.getSchrift().setText("");
            //Bilder abwaehlen
            angeklickt = null;
            //neu zeichnen
            spiel.repaint();
        }
    }
    //Klick ins Inventar
    public void InventarKlick(){
        //Klick innerhalb des Inventarbilds
        //Bild wird ermittelt
        Bild geklicktesBild = getBildAusInventar(urspruenglichX, urspruenglichY, spiel.getInventarGUI().getBildliste());
        //2.1) wenn auf ein Bild geklickt wurde
        if (!(geklicktesBild == null)) {
            //2.1.1) Funktion fuer Rechtsklick (fuer Windows) oder Alt + Linksklick (fuer Mac)
            if (button == maus.BUTTON3 || (controlDown && button == maus.BUTTON1)) {
                //Infotext des Gegenstands ausgeben
                spiel.getSchrift().setText(geklicktesBild.getGegenstand().getInfoGegenstand());
                soundAbspielen(geklicktesBild.getGegenstand().getInfosound());
                //Bild abwaehlen
                angeklickt = null;
                //neu zeichnen
                spiel.repaint();
            }
            //2.1.2) Funktion fuer Linksklick
            else if (button == maus.BUTTON1) {
                //2.1.2.1) Wenn ein Bild im Speicher 'angeklickt' liegt (also bisher eins angewaehlt ist)
                if (angeklickt != null) {
                    //2.1.2.1.1) Entspricht das nun geklickte Bild dem bereits Angewaehlten
                    if (angeklickt == geklicktesBild) {
                        //Bild abwaehlen und neuzeichnen
                        angeklickt = null;
                        spiel.repaint();
                    }
                    //2.1.2.1.2) Gibt es eine Kombinationsmoeglichkeit fuer das bereits angeklickte Bild und das momentan Gewaehlte
                    else if (geklicktesBild.getGegenstand().getKombinationsmoeglichkeit() != null) {
                        //ist eine Kombination zwischen beiden Gegenstaenden moeglich
                        if (geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().istKombinierbar(geklicktesBild.getGegenstand(), 
                                angeklickt.getGegenstand())) {
                            //beide Bilder werden aus dem Inventar entfernt
                            spiel.getInventarGUI().bildliste.remove(angeklickt);
                            spiel.getInventarGUI().bildliste.remove(geklicktesBild);
                            spiel.getInventar().getGegenstaende().remove(angeklickt.getGegenstand());
                            spiel.getInventar().getGegenstaende().remove(geklicktesBild.getGegenstand());
                            //neuer Gegenstand wird dem Inventar hinzugefuegt
                            spiel.getInventarGUI().bildliste.add(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getNeuerGegenstand().getInventarbild());
                            spiel.getInventar().getGegenstaende().add(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getNeuerGegenstand());
                            //Kombinationstext wird ausgegeben
                            spiel.getSchrift().setText(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getKombinationstext());
                            soundAbspielen(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getSound());
                            //Bild wird abgewaehlt
                            angeklickt = null;
                            //neu zeichnen
                            spiel.repaint();
                        }
                        //2.1.2.1.3)Geht diese Kombination nicht
                        else {
                            //Zufallszahl im Bereich der ArrayListgroesse
                            int zufall = (int) (Math.random() * (gegenstandstexte.size()));
                            //Zufallstext ausgeben
                            spiel.getSchrift().setText(gegenstandstexte.get(zufall));
                            soundAbspielen(gegenstandstexteSounds.get(zufall));
                            //Bild abwaehlen
                            angeklickt = null;
                            //neu zeichnen
                            spiel.repaint();
                        }
                    }
                    //2.1.2.1.2)Gibt es eine Kombinationsmoeglichkeit - Fall umgekehrt abgeprueft
                    else if (angeklickt.getGegenstand().getKombinationsmoeglichkeit() != null) {
                        if (angeklickt.getGegenstand().getKombinationsmoeglichkeit().istKombinierbar(geklicktesBild.getGegenstand(), angeklickt.getGegenstand())) {
                            //beide Bilder werden aus dem Inventar entfernt
                            spiel.getInventarGUI().bildliste.remove(angeklickt);
                            spiel.getInventarGUI().bildliste.remove(geklicktesBild);
                            spiel.getInventar().getGegenstaende().remove(angeklickt.getGegenstand());
                            spiel.getInventar().getGegenstaende().remove(geklicktesBild.getGegenstand());
                            //neuer Gegenstand wird hinzugefuegt
                            spiel.getInventarGUI().bildliste.add(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getNeuerGegenstand().getInventarbild());
                            spiel.getInventar().getGegenstaende().add(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getNeuerGegenstand());
                            //Kombinationstext wird ausgegeben
                            spiel.getSchrift().setText(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getKombinationstext());
                            soundAbspielen(geklicktesBild.getGegenstand().getKombinationsmoeglichkeit().getSound());
                            //Bild wird abgewaehlt
                            angeklickt = null;
                            //neu zeichnen
                            spiel.repaint();
                        }
                        //2.1.2.1.3)Geht diese Kombination nicht
                        else {
                            //Zufallszahl im Bereich der ArrayListgroesse
                            int zufall = (int) (Math.random() * (gegenstandstexte.size()));
                            //Zufallstext ausgeben
                            spiel.getSchrift().setText(gegenstandstexte.get(zufall));
                            soundAbspielen(gegenstandstexteSounds.get(zufall));
                            //Bild abwaehlen
                            angeklickt = null;
                            //neu zeichnen
                            spiel.repaint();
                        }
                    }
                    //wenn die Kombinationsoeglichkeit einses Gegenstands null ist, aber eine Kombination ausprobiert wird
                    else {
                        //Zufallszahl im Bereich der ArrayListgroesse
                        int zufall = (int) (Math.random() * (gegenstandstexte.size()));
                        //Zufallstext ausgeben
                        spiel.getSchrift().setText(gegenstandstexte.get(zufall));
                        soundAbspielen(gegenstandstexteSounds.get(zufall));
                        //Bild abwaehlen
                        angeklickt = null;
                        //neu zeichnen
                        spiel.repaint();
                    }
                }
                //2.1.2.2) Wenn kein Bild im Speicher 'angeklickt' liegt (also bisher keins angewaehlt ist)
                else {
                    //speichert das geklickte Bild in der Variable 'angeklickt'
                    angeklickt = geklicktesBild;
                    //zeichnet einen Rahmen um das Bild
                    zeichneRahmen(angeklickt);
                    //Name des Gegenstands ausgeben
                    spiel.getSchrift().setText(geklicktesBild.getGegenstand().getName());
                }
            }
        }
        //2.2) Wenn auf kein Bild im Inventar geklickt wurde
        else {
            //Bild abwaehlen, Text loeschen, neu zeichnen
            angeklickt = null;
            spiel.getSchrift().setText("");
            spiel.repaint();
        }
    }

    // wenn irgendwo im weißen Feld geklickt wurde
    public void Zuruecksetzen(){
        //Bild abwaehlen, Text loeschen, neu zeichnen
        spiel.getSchrift().setText("");
        angeklickt = null;
        spiel.repaint();
    }


    public void Spielfigurbewegung(int x, int y){
        //punkt mit dem die spielfigurbewegung gestartet wurde sichern
        Koordinaten punkt = new Koordinaten(x, y);

        //wenn sich der zielpunkt mit dem die bewegung gestartet wurde geaendert hat, beende die fkt
        if (punkt.getXKoordinate() != geklicktX || punkt.getYKoordinate() != geklicktY) {
            return;
        }

        //wenn die luftlinie zwischen start und ziel größer als 50pixel ist..
        if (Wegfindung.bewegungNoetig(altX, altY, geklicktX, geklicktY)) {
            boolean fertig = false;
            //..beweg dich solang bis start und zielpunkt gleich sind
            while ((geklicktX != altX) || (geklicktY != altY)) {

                //vertikale zielrichtung festlegen
                if (geklicktY > altY) {
                    richtungV = "unten";
                }
                if (geklicktY < altY) {
                    richtungV = "oben";
                }
                //horizontale zielrichtung festlegen
                if (geklicktX > altX) {
                    richtungH = "rechts";
                }
                if (geklicktX < altX) {
                    richtungH = "links";
                }
                //wenn sich der zielpunkt mit dem die bewegung gestartet wurde geaendert hat, beende die fkt
                if (punkt.getXKoordinate() != geklicktX || punkt.getYKoordinate() != geklicktY) {
                    return;
                }
                //laufprioritäten
                //wenn nach unten gegangen werden soll...
                if ("unten".equals(richtungV)) {
                    //.. lauf zuerst vertikal also nach unten
                    fertig = bewegen(punkt, richtungV);
                    //wenn du vertikal fertig bist..
                    if (fertig) {
                        //..gehe in die horizontale zielrichtung
                        bewegen(punkt, richtungH);
                    }
                    //am ende der bewegung lass die figur nach unten schauen
                    spiel.setAussehen(spiel.getSpielfigur().getBewegungUnten().get(0));
                    //wenn nach oben gegangen werden soll...
                } else if ("oben".equals(richtungV)) {
                    //.. lauf zuerst in die horizontale zielrichtung
                    fertig = bewegen(punkt, richtungH);
                    //wenn du horizontal fertig bist..
                    if (fertig) {
                        //..gehe in die vertikale laufrichtung also nach oben
                        bewegen(punkt, richtungV);
                    }
                }
            }
        }
        //wenn sich der zielpunkt mit dem die bewegung gestartet wurde geaendert hat, beende die fkt
        if (punkt.getXKoordinate() != geklicktX || punkt.getYKoordinate() != geklicktY) {
            return;
        }
        bewegungFertig = true;

        //wenn die bewegung beendet ist
        if (bewegungFertig == true) {

            bewegungFertig = false;

            System.out.println("bewegung fertig");

            //wenn ein bild angeklickt wurde
            if (gewaehltesBild != null) {
                System.out.println("geklickt auf: " + gewaehltesBild.getName());
                //wenn der klick innerhalb des gewaehlten bildes liegt
                if (urspruenglichX >= gewaehltesBildX && urspruenglichX <= (gewaehltesBildX + gewaehltesBildBreite) && urspruenglichY >= gewaehltesBildY && urspruenglichY <= (gewaehltesBildY + gewaehltesBildHoehe)) {
                    //..fuehre das mausereignis aus fuer die weiteren aktionen
                    Mausereignis();
                }
            }
            //wenn kein bild angeklickt wurde
            if (gewaehltesBild == null) {
                System.out.println("geklickt auf leere Fläche");
                spiel.getSchrift().setText("");//Textfeld leeren
                angeklickt = null;//Bilder abwaehlen
                spiel.repaint();//neu zeichnen
            }
        }
    }

    /**
     * die figur soll sich in eine bestimmte richtung bewegen
     * @param punkt kontrollpunkt mit dem die ganze bewegung gestartet wurde
     * @param richtung in die gegangen werden soll
     * @return true, wenn die bewegung in der richtung fertig ist
     */
    public boolean bewegen(Koordinaten punkt, String richtung){
        ArrayList<Image> animationsbilder = null;

        int zielpunkt = -1;
        int startpunkt = -1;
        int dauer = -1;

        boolean plus = false;

        bildNummer = 0;
        //werte setzen um allgemeine funktion für die bewegung nutzen zu können
        if (richtung.equals("rechts") || richtung.equals("links")) {
            startpunkt = altX;
            zielpunkt = geklicktX;
        }
        if (richtung.equals("rechts")) {
            animationsbilder = spiel.getSpielfigur().getBewegungRechts();
            plus = true;
        }
        if (richtung.equals("links")) {
            animationsbilder = spiel.getSpielfigur().getBewegungLinks();
            plus = false;
        }
        if (richtung.equals("oben") || richtung.equals("unten")) {
            startpunkt = altY;
            zielpunkt = geklicktY;
        }
        if (richtung.equals("oben")) {
            animationsbilder = spiel.getSpielfigur().getBewegungOben();
            plus = false;
        }
        if (richtung.equals("unten")) {
            animationsbilder = spiel.getSpielfigur().getBewegungUnten();
            plus = true;
        }
        //dauer der bewegung als positive zahl
        if (startpunkt < 0) {
            dauer = Math.abs(startpunkt - zielpunkt);
        }
        if (startpunkt > 0) {
            dauer = Math.abs(zielpunkt - startpunkt);
        }

        startzeit = System.currentTimeMillis();
        //anfangsaussehn der figur
        spiel.setAussehen((Image) animationsbilder.get(bildNummer));

        //bewegungsschleife starten
        bewegen(punkt, richtung, dauer, startpunkt, plus, animationsbilder);

        return true;
    }

    /**
     * allgemeine funktion für die bewegung, um nicht für jede richtung eine eigene funktion zu haben
     * @param punkt kontrollpunkt, um die bewegung abzubrechen, falls erneut geklickt wurde bevor die bewegung zu ende ist
     * @param richtung in die gegangen werden soll
     * @param dauer anzahl pixel des weges
     * @param startpunkt punkt, an dem die figur steht
     * @param plus boolsche variable die angibt ob der wert des startpunktes waehrend der bewegung hoch- oder runtergezaehlt werden muss
     * @param animationsbilder bilderliste fuer die laufanimation
     * @return true, wenn die bewegung fertig ist
     */
    public boolean bewegen(Koordinaten punkt, String richtung, int dauer, int startpunkt, boolean plus, ArrayList<Image> animationsbilder){
        startzeit = System.currentTimeMillis();
        //solang die anzahlpixel nicht gelaufen wurde
        while (dauer != 0) {
            //pro schleifendurchgang die dauer um 1 reduzieren da 1pixel gegangen wird
            dauer--;
            //wenn sich der zielpunkt mit dem die bewegung gestartet wurde geaendert hat, beende die fkt
            if (punkt.getXKoordinate() != geklicktX || punkt.getYKoordinate() != geklicktY) {
                return false;
            }
            //startpunkt erhoehen oder reduzieren um einen pixel
            if (plus) {
                startpunkt++;
            }
            if (!plus) {
                startpunkt--;
            }

            //
            try {
                Thread.sleep(bildfrequenz);
            } catch (InterruptedException ex) {
            }
            endzeit = System.currentTimeMillis(); //Endszeit der Schleife fuer Zeitmessung fuer Anfangsgrafik
            gesamtzeit = endzeit - startzeit; //Zeitdifferenz berechnen
            if (gesamtzeit > BILDDAUER) {
                bildNummer++; //BildNummer hoeher setzen, sodass neue Grafik erschent.
                if (bildNummer > animationsbilder.size() - 1) {
                    bildNummer = 0; //Ende der arrayList abfangen
                }
                spiel.setAussehen(animationsbilder.get(bildNummer)); //Grafik ‚aendern
                startzeit = System.currentTimeMillis(); // Startzeit auf aktuelle Zeit stellen
            }
                        //setzen des veraenderten startpunktes, um voran zu kommen
            if ((richtung.equals("rechts") || richtung.equals("links"))) {
                if (startpunkt <= spiel.getGrenzeRechts() - 45 && startpunkt >= spiel.getGrenzeLinks() + 45) {
                    setAltX(startpunkt);
                }
                spiel.getSpielfigur().getSpielerKoordinate().setXKoordinate(startpunkt);
            }
            if ((richtung.equals("oben") || richtung.equals("unten"))) {
                if (startpunkt <= spiel.getGrenzeUnten() - 10 && startpunkt >= spiel.getGrenzeOben() + 195) {
                    setAltY(startpunkt);
                }
                spiel.getSpielfigur().getSpielerKoordinate().setYKoordinate(startpunkt);
            }
            spiel.repaint(30, 80, 965, 550);
            //wenn das gewaehlte bild, also das angeklickte nicht null ist und auch keine tuer und die bewegung notwendig ist
            if (!(gewaehltesBild != null && gewaehltesBild.getTuer() != null && !Wegfindung.bewegungNoetig(startpunkt, altY, geklicktX, geklicktY))) {
                //fuehre solange eine bewegung nach unten aus wie das feld in der naechste feld in der eigentlichen gehrichtung nicht begehbar ist
                //wenn gleichzeitig die eigentliche gehfrichtung horizontal ist
                while (!spiel.getAktuellerRaum().getWegfindung().isBegehbar(new Koordinaten(altX, altY), richtung) &&
                        ("links".equals(richtung) || "rechts".equals(richtung))) {
                    //bewege dich 10 pixel nach unten
                    bewegen(punkt, "unten", 10, altY, true, spiel.getSpielfigur().getBewegungUnten());
                }
            }
        }

        spiel.setAussehen(animationsbilder.get(0)); // nach Bewegung auf Standgrafik zuruecksetzen
        //repainte nur das spielfeld, damit der rahmen um einen gewaehlten gegenstand im inventar nicht abgewaehlt wird
        spiel.repaint(30, 80, 965, 550);
        return true;

    }

    //entscheidet, was beim klick auf welches Bild in einem Menue geschehen soll
    public void menueKlickListener() {
        //wenn ein bild angeklickt wurde
        if (gewaehltesBild != null) {
            //1. neues spiel
            if (gewaehltesBild.getName().equals("NeuesSpiel")) {
                spiel.getIntroUeberspringen().setLabel("Intro überspringen"); //buttonlabel zuruecksetzen
                spiel.setIntroAbspielen(true); //intro abspielen
                spiel.thMusik.soundStop(); //die derzeit geloopte musik stoppen
                soundAbspielen(spiel.getKlickSound()); //klicksound für buttonklick abspielen
                spiel.thMusik.soundLoopen(spiel.getStorySound()); //
                //startpunkte setzen
                setAltX(145);
                setGeklicktX(145);
                setGeklicktY(595);
                setAltY(595);
                spiel.setSpielstand("default");//spielstand setzen der geladen werden soll, bzw in dem man ist
                setKlickenMoeglich(false); //klicken nicht moeglich
                spiel.getIntroUeberspringen().setVisible(true); //button zum ueberspringen des outros sichtbar setzen
                spiel.getRaumsteuerung().spieleAnimation(spiel.getIntroBilder(), "intro"); //intro starten
                spiel.repaint();
                setKlickenMoeglich(true); //klicken wieder moeglich
            }

            //spielstory überspringen
            if (gewaehltesBild.getName().equals("Ueberspringen")) {

                spiel.setUeberspringen(false);
                spiel.repaint();
                spiel.thMusik.soundStop(); //storysoundmusik beenden
                soundAbspielen(spiel.getHundebellen()); //hundebellen abspielen
                XMLHandlingLesen xml = new XMLHandlingLesen(spiel);
                xml.ladeSpielstand(spiel, "default"); //spielstand laden eines neuen spiels
                xml = null;
                spiel.setPreloader(new Preloader(spiel));  //raumbilder vorausladen
                spiel.setPreload(true);
                spiel.repaint();
                spiel.getRaumsteuerung().ladeRaum(spiel.getAbstellkammer()); //startraum laden
            }

            //button, um das aktuelle spiel weiterzuspielen.
            if (gewaehltesBild.getName().equals("btnWeiterspielen")) {
                soundAbspielen(spiel.getKlickSound());
                spiel.getHauptmenue().remove(6);  //weiterspielen-button wieder entfernen
                //punktwerte setzen
                setAltX(613);
                setGeklicktX(613);
                setUrspruenglichX(613);
                setAltY(508);
                setGeklicktY(508);
                setUrspruenglichY(508);
                setKlickenMoeglich(false);
                spiel.getRaumsteuerung().ladeRaum(spiel.getSpeicherraum()); //startraum bei laden eines spielstands
                spiel.repaint();
                setKlickenMoeglich(true);
                spiel.getRaumsteuerung().setStandardsound(true);
            }

            //2. spielLaden
            if (gewaehltesBild.getName().equals("SpielLaden")) {
                soundAbspielen(spiel.getKlickSound());
                //ins menue zum spielstand laden wechseln
                spiel.getRaumsteuerung().wechselnMenue(spiel.getLadenMenue().getLadeMenueBilder());
                //4 buttons fuer die spielstaende mit den richtigen aufschriften hinzufuegen
                for (int i = 0; i < spiel.getLadenMenue().getSpielstaende().size(); i++) {
                    spiel.add(spiel.getLadenMenue().getSpielstaende().get(i));
                    spiel.getLadenMenue().getSpielstaende().get(i).repaint();
                }
                spiel.repaint();
            }
            //2.1 zurueck zu hauptmenue
            if (gewaehltesBild.getName().equals("LadenZurueck")) {
                soundAbspielen(spiel.getZurueckSound());
                //ins hauptmenue wechseln
                spiel.getRaumsteuerung().wechselnMenue(spiel.getHauptmenue());
                //4 buttons des laden menues entfernen
                for (int i = 0; i < spiel.getLadenMenue().getSpielstaende().size(); i++) {
                    spiel.remove(spiel.getLadenMenue().getSpielstaende().get(i));
                }
            }
            //3. optionen
            if (gewaehltesBild.getName().equals("Optionen")) {
                soundAbspielen(spiel.getKlickSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getOptionen());
            }
            //3.1 anleitung
            if (gewaehltesBild.getName().equals("Anleitung")) {
                soundAbspielen(spiel.getKlickSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getAnleitung());
            }

            //3.1.1 zurueck zu optionen
            if (gewaehltesBild.getName().equals("anleitungZurueck")) {
                soundAbspielen(spiel.getZurueckSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getOptionen());
            }
            //3.2 Credits
            if (gewaehltesBild.getName().equals("Credits")) {
                soundAbspielen(spiel.getKlickSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getCredits());
            }
            //3.2.1 zurueck zu optionen
            if (gewaehltesBild.getName().equals("creditsZurueck")) {
                soundAbspielen(spiel.getZurueckSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getOptionen());
            }
            //3.3 Sounds
            if (gewaehltesBild.getName().equals("Sound")) {
                soundAbspielen(spiel.getKlickSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getSoundMenue().getSoundmenueBilder());
                //buttons zum ein- und ausschalten der musik hinzufuegen
                spiel.add(spiel.getSoundMenue().getHgmusikSchalter());
                spiel.getSoundMenue().getHgmusikSchalter().repaint();
                spiel.add(spiel.getSoundMenue().getSoundSchalter());
                spiel.getSoundMenue().getSoundSchalter().repaint();
            }
            //zurueck zu optionen
            if (gewaehltesBild.getName().equals("SoundZurueck")) {
                soundAbspielen(spiel.getZurueckSound());
                //buttons aus dem soundmenue entfernen
                spiel.remove(spiel.getSoundMenue().getHgmusikSchalter());
                spiel.remove(spiel.getSoundMenue().getSoundSchalter());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getOptionen());
            }
            //3.4 Online
            if (gewaehltesBild.getName().equals("Online")) {
                soundAbspielen(spiel.getKlickSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getOnline());
            }
            //3.4.1 zurueck zu optionen
            if (gewaehltesBild.getName().equals("onlineZurueck")) {
                soundAbspielen(spiel.getZurueckSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getOptionen());
            }

            //zurueck zu hauptmenue
            if (gewaehltesBild.getName().equals("optionenZurueck")) {
                soundAbspielen(spiel.getZurueckSound());
                spiel.getRaumsteuerung().wechselnMenue(spiel.getHauptmenue());
            }
            //4. Beenden
            if (gewaehltesBild.getName().equals("Beenden")) {
                //beenden dialog starten und text und position setzen
                spiel.getDialogBeenden().setZusatztext("");
                spiel.getDialogBeenden().setLocation((spiel.getLocation().x + 700), (spiel.getLocation().y + 30));
                spiel.getDialogBeenden().setVisible(true);
            }

            //5. spielSpeicherMenue
            if (gewaehltesBild.getName().equals("spielSpeichernMenueZurueck")) {
                spiel.getRaumsteuerung().setStandardsound(true);
                //punktwerte setzen
                setAltX(613);
                setGeklicktX(613);
                setUrspruenglichX(613);
                setAltY(608);
                setGeklicktY(608);
                setUrspruenglichY(608);
                spiel.setAussehen(spiel.getSpielfigur().getBewegungUnten().get(0));
                spiel.getRaumsteuerung().ladeRaum(spiel.getSpeicherraum()); //speicherraum laden
                //buttons des speichermenues entfernen
                for (int i = 0; i < spiel.getSpeicherMenue().getSpielstaende().size(); i++) {
                    spiel.remove(spiel.getSpeicherMenue().getSpielstaende().get(i));
                }
                spiel.repaint();
            }

        }
    }

    //liefert das Bild aus bl zurueck, das in x,y liegt oder null, wenn dort keins vorhanden ist
    public Bild getGeklicktesBild(int x, int y, ArrayList<Bild> bl){
        //setzt b auf null = kein Bild//setzt b auf null = kein Bild
        Bild b = null;
        //jedes Bild der BildListe wird durchlaufen
        for (int zaehler=0 ; zaehler < bl.size() ; zaehler++){ 
            Bild aktuell = bl.get(zaehler);
            //Berechnung, ob Koordinaten auf dem aktuellen Bild liegen
            if (x > aktuell.getKoordinaten().getXKoordinate() && x < (aktuell.getKoordinaten().getXKoordinate() + aktuell.getBreite())
                && y > aktuell.getKoordinaten().getYKoordinate() && y < (aktuell.getKoordinaten().getYKoordinate() + aktuell.getHoehe())) {
                //b wird auf das aktuelle Bild gesetzt
                b = aktuell;
                //bricht die for-Schleife ab
                break;
            }
        }
        return b; //gibt b zurueck
    }

    //liefert das Bild aus bl zurueck, das in x,y liegt oder null, wenn dort keins vorhanden ist
    public Bild getBildAusInventar(int x, int y, ArrayList<Bild> bl){
        //setzt b auf null = kein Bild
        Bild b = null;
        //jedes Bild der BildListe wird durchlaufen
        for (int zaehler=0 ; zaehler < bl.size() ; zaehler++){ 
            Bild aktuell = bl.get(zaehler);
            //Berechnung, ob Koordinaten auf dem aktuellen Bild liegen
            if (x > aktuell.getKoordinaten().getXKoordinate() && x < (aktuell.getKoordinaten().getXKoordinate() + aktuell.getBreiteInventar())
                && y > aktuell.getKoordinaten().getYKoordinate() && y < (aktuell.getKoordinaten().getYKoordinate() + aktuell.getHoeheInventar())) {
                //b wird auf das aktuelle Bild gesetzt
                b = aktuell;
                //bricht die for-Schleife ab
                break;
            }
        }
        //gibt b zurueck
        return b; 
    }

    //Methode, einen Rahmen um einen Gegenstand in dem Inventar zu zeichnen
    public void zeichneRahmen(Bild bild){
        Graphics g = spiel.getGraphics();
        Graphics2D gr = (Graphics2D) g;
        //setze die Farbe des Rahmens auf schwarz
        gr.setColor(Color.black);
        //Rahmenzeichnung mit 2 Pixel Abstand vom Bild
        //Variablen für die Koordinaten des uebergebenen Bildes werden hier -2 Pixel gespeichert
        int x = bild.getKoordinaten().getXKoordinate() - 2;
        int y = bild.getKoordinaten().getYKoordinate() - 2;
        //der Breite und Hoehe des Bildes werden 4 Pixel hinzugefuegt und in Variablen gespeichert
        int breite =  bild.getBreiteInventar()+ 4;
        int hoehe = bild.getHoeheInventar() + 4;
        //Linien zeichnen
        gr.setStroke(new BasicStroke(4.0f));
        gr.drawLine(x, y, x + breite, y);
        gr.drawLine(x + breite, y, x + breite, y + hoehe);
        gr.drawLine(x, y, x, y + hoehe);
        gr.drawLine(x, y + hoehe, x + breite, y + hoehe);
    }

    /**
     * stoppt eine laufende soundausgabe und startet eine neue
     * @param soundinput datei die abgespielt werden soll
     */
    public void soundAbspielen( File soundinput){
        //wenn sounds abspielen eingeschaltet ist
        if(spiel.getSoundsAbspielen()){
            try {
                //wenn es einen sound gibt stoppe ihn
                if(getSound()!=null)getSound().stop();
                //erstelle einen neuen audioclip und speichern ihn in sound
                setSound(Applet.newAudioClip(soundinput.toURI().toURL()));
                //spiel den sound ab
                getSound().play();
            }
            catch(Exception e){
                e.printStackTrace();
                System.out.println("Sound " + soundinput + " konnte nicht gefunden werden.");
            }
        }
    }

    /**
     * stoppt 2 laufende soundausgaben, wenn es welche gibt und startet 2 neue soundausgaben
     * insbesondere fuer die ausgabe von sammelsound und interaktionssound gedacht
     *
     * @param einsammeln einsammelsound bzw 1.sound, der abgespielt werden soll
     * @param soundinput interaktionssound bzw 2. sound, der abgespielt werden soll
     */
    public void soundAbspielen( File einsammeln, File soundinput){
        if(spiel.getSoundsAbspielen()){
            try {
                if(getSound01()!=null)getSound01().stop();
                setSound01(Applet.newAudioClip(einsammeln.toURI().toURL()));
                getSound01().play();
            }
            catch(Exception e){
                e.printStackTrace();
                System.out.println("Sound " + soundinput + " konnte nicht gefunden werden.");
            }
            try {
                if(getSound()!=null)getSound().stop();
                setSound(Applet.newAudioClip(soundinput.toURI().toURL()));
                getSound().play();
            }
            catch(Exception e){
                e.printStackTrace();
                System.out.println("Sound " + soundinput + " konnte nicht gefunden werden.");
            }
        }
    }

    //get- Methoden
    public int getGeklicktX(){
       return geklicktX;
    }
    public int getGeklicktY(){
       return geklicktY;
    }

    public int getGewaehltesBildX(){
        return gewaehltesBildX;
    }

    public int getGewaehltesBildY(){
        return gewaehltesBildY;
    }

    public int getGewaehltesBildBreite(){
        return gewaehltesBildBreite;
    }

    public int getGewaehltesBildHoehe(){
        return gewaehltesBildHoehe;
    }

    public int getUrspruenglichX(){
        return urspruenglichX;
    }

    public int getUrspruenglichY(){
        return urspruenglichY;
    }

    public int getAltX(){
        return altX;
    }

    public int getAltY(){
        return altY;
    }

    public Bild getAngeklicktesBild(){
        return angeklicktesBild;
       }

    public int getButton(){
        return this.button;
    }

    public boolean getControlDown(){
        return controlDown;
    }

    public MouseEvent getMaus(){
        return maus;
    }

    public int getBildnummer(){
        return bildNummer;
    }

    public ArrayList<File> getHenriHardSounds() {
        return henriHardSounds;
    }

    //set- Methoden
    public void setSp(SpielLoop spiel) {
        this.spiel = spiel;
    }

    public void setGeklicktX (int X){
        geklicktX=X;
    }

    public void setGeklicktY(int Y){
        geklicktY=Y;
    }

    public void setUrspruenglichX(int urspX){
        this.urspruenglichX=urspX;
    }

      public void setUrspruenglichY(int urspY){
        this.urspruenglichY=urspY;
    }

    public void setAltX(int X) {
        altX = X;
    }

    public void setAltY(int Y) {
        altY = Y;
    }

    public boolean isSoundeinstellungenGeaendert(){
        return soundeinstellungenGeaendert;
    }

    public void setSoundeinstellungenGeaendert(boolean soundeinstellungenGeaendert) {
        this.soundeinstellungenGeaendert = soundeinstellungenGeaendert;
    }

    public void setKlickenMoeglich(boolean klickenMoeglich) {
        this.klickenMoeglich = klickenMoeglich;
    }

    public boolean isKlickenMoeglich() {
        return klickenMoeglich;
    }

    public int getActX() {
        return actX;
    }

    public int getActY() {
        return actY;
    }

    /**
     * @param actX the actX to set
     */
    public void setActX(int actX) {
        this.actX = actX;
    }

    /**
     * @param actY the actY to set
     */
    public void setActY(int actY) {
        this.actY = actY;
    }

    /**
     * @return the sound
     */
    public AudioClip getSound() {
        return sound;
    }

    /**
     * @param sound the sound to set
     */
    public void setSound(AudioClip sound) {
        this.sound = sound;
    }

    /**
     * @return the sound01
     */
    public AudioClip getSound01() {
        return sound01;
    }

    /**
     * @param sound01 the sound01 to set
     */
    public void setSound01(AudioClip sound01) {
        this.sound01 = sound01;
    }
}
