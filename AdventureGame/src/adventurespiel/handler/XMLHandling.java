/**
 * THIS SOFTWARE IS PROVIDED BY METASTUFF, LTD. AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL METASTUFF, LTD. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * Copyright 2001-2005 (C) MetaStuff, Ltd. All Rights Reserved.
 */

package adventurespiel.handler;

import adventurespiel.steuerung.SpielLoop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
/**
 *
 * Enthält allgemeine Funktionen für den einfacheren Umgang mit XML.
 * Einlesen, Schreiben, Verändern von XML-Dateien
 *
 * @author Nadine
 * @version 25.10.09
 */
public abstract class XMLHandling
{
    protected static String xmlpfad = "src/xmls/";
    protected static SpielLoop spiel;

    public XMLHandling(){
    }

    public XMLHandling(SpielLoop loop){
        spiel = loop;
    }

    /**
     * Parst eine Datei und gibt ein Document zurück.
     * DocumentException wird bei ungültigem Dateipfad ausgelöst.
     *
     * @param pfad Dateipfad zur zu parsenden Datei
     * @return document, wenn es erfolgreich eingelesen werden konnte.
     * Repräsentiert, die aus der XML gelesene Baumstruktur.
     */
    public static Document parse(String pfad) throws NullPointerException{
        SAXReader reader = new SAXReader();
        Document document = erzeugeDocument("root");
        try{
            if(new File(pfad).exists()){
                document = reader.read(pfad);
            }
	    return document;
	}
	catch(Exception err){
	    System.err.println("Fehler in class:XMLHandling, funktion:parse");
	    System.err.println(err);
	    return null;
	}
    }

    /**
     *
     * @param Wurzelelement rootelement der datei
     * @return ein document mit einem wurzelelement
     */
    public static Document erzeugeDocument(String Wurzelelement){
        
	Document document = DocumentHelper.createDocument();
	document.addElement(Wurzelelement);
	return document;
    }

    /**
     * eine datei aus dem document erzeugen
     * 
     * @param dateipfad Pfad der zu schreibenden Datei
     * @param document Document mit XML-Struktur, das geschrieben werden soll
     * @return true, wenn die Datei nach schreiben existiert; sonst false
     */
    private static boolean schreibeDatei(String dateipfad, Document document){

	try {
		//Erzeugt eine formatierte XML-Datei mit Tabs, Zeilenbrüchen etc.
		OutputFormat format = OutputFormat.createPrettyPrint();
		XMLWriter writer = new XMLWriter(new FileWriter(dateipfad), format);
		writer.write(document);
		writer.close();
	}catch (IOException ex) {
	    System.err.println("Fehler in class:XMLHandling funktion:schreibeDatei");
	    System.err.println(ex);
	}
	return new File(dateipfad).exists();
    }

    /**
     * Schreibt eine neue Datei aus dem übergebenen Document, wenn noch keine Datei
     * unter dem übergebenen Pfad existiert.
     * 
     * @param dateipfad Pfad der neuen Datei
     * @param document Document mit zu schreibender Baumstruktur
     * @return true, wenn Datei noch nicht existierte und geschrieben wurde; sonst false
     */
    public static boolean schreibeNeueDatei(String dateipfad, Document document){

	if(!new File(dateipfad).exists()){
	    return schreibeDatei(dateipfad, document);
	}
	else{
	    return false;
	}
    }

    /**
     *
     * Überschreibt die Datei mit dem übergebenen Document, wenn die Datei
     * unter dem übergebenen Pfad bereits existiert.
     * 
     * @param dateipfad Pfad der zu ueberschreibenden Datei
     * @param document Document mit zu schreibender Baumstruktur
     * @return true, wenn Datei existierte und ueberschrieben wurde; sonst false
     */
    public static boolean ueberschreibeDatei(String dateipfad, Document document){
	if(new File(dateipfad).exists()){
	    return schreibeDatei(dateipfad, document);
	}
	else{
	    return false;
	}
    }

    /**
     *
     * Erzeugt einen Ordner mit evtl. einem oder mehreren Unterordnern
     *
     * @param ordnername Name des Ordners mit /, z.b. "ordner/"
     * @return true, wenn der Ordner erstellt werden konnte; false, wenn er bereits existierte
     */
    public static boolean erzeugeOrdner(String ordnername){

	File f = new File(ordnername);
	//Ordner erzeugen aus File
	return f.mkdirs();
    }

    /**
     * Wandelt einen String mit "true" oder "false" in ein boolean-Objekt
     *
     * @param s zu prüfender String
     * @return in boolean gewandelter String
     */
    public static boolean getBoolean(String s){

        if(s.equals("true")){
            return true;
        }else if(s.equals("false") || s.equals("")){
        return false;
        }else{
            System.out.println("Fehler in XML, String konnte nicht in boolean umgewandelt werden");
        }
        return false;
    }

    /**
     * Prüft ob ein String in einer Liste von Strings enthalten ist.
     *
     * @param wort soll gefunden werden
     * @param liste in der gesucht werden soll
     * @return true, wenn das wort in der liste enthalten ist; sonst false
     */
    public static boolean istEnthalten(String wort, ArrayList<String> liste){
        for (int i = 0; i < liste.size(); i++) {
            if(liste.get(i).equals(wort))return true;
        }
        return false;
    }

    /**
     * liest einen Text aus einem Document aus mit dem entsprechenden Pfad
     *
     * @param document Document, in dem der Text steht
     * @param xpath Xpath zum Text
     * @return String, der mithilfe des XPath im Document gefunden wird
     */
    public static String lies(Document document, String xpath){
        
        if(document.selectSingleNode(xpath)!=null) {
            return document.selectSingleNode(xpath).getText();
        }else return "0";
    }

    /**
     * liest eine int-Zahl aus einem Document aus mit dem entsprechenden Pfad
     *
     * @param document Document, in dem die int-Zahl steht
     * @param xpath Xpath zur int-Zahl
     * @return int-Zahl, die mithilfe des XPath im Document gefunden wird
     */
    public static int liesInt(Document document, String xpath){
        return Integer.parseInt(lies(document, xpath));
    }

    /**
     * liest einen boolschen Wert aus einem Document aus mit dem entsprechenden Pfad
     *
     * @param document Document, in dem der boolsche Wert steht
     * @param xpath Xpath zum boolschen Wert
     * @return boolscher Wert, der mithilfe des XPath im Document gefunden wird
     */
    public static boolean liesBoole(Document document, String xpath){
        return getBoolean(lies(document, xpath));
    }

    /**
     * liest einen Text aus einem Document aus mit dem entsprechenden Pfad
     *
     * @param document Document, in dem der Text steht
     * @param xpath Xpath zum Text
     * @return String, der mithilfe des XPath im Document gefunden wird
     */
    public static void schreib(Document document, String xpath, String text){
        if(document.selectSingleNode(xpath)!=null){
            document.selectSingleNode(xpath).setText(text);
        }
    }

    /**
     * liest eine int-Zahl aus einem Document aus mit dem entsprechenden Pfad
     *
     * @param document Document, in dem die int-Zahl steht
     * @param xpath Xpath zur int-Zahl
     * @return int-Zahl, die mithilfe des XPath im Document gefunden wird
     */
    public static void schreibInt(Document document, String xpath, int zahl){
        schreib(document, xpath, String.valueOf(zahl));
    }

    /**
     * liest einen boolschen Wert aus einem Document aus mit dem entsprechenden Pfad
     *
     * @param document Document, in dem der boolsche Wert steht
     * @param xpath Xpath zum boolschen Wert
     * @return boolscher Wert, der mithilfe des XPath im Document gefunden wird
     */
    public static void schreibBoole(Document document, String xpath, boolean boole){
        schreib(document, xpath, String.valueOf(boole));
    }

    /**
     * @return the spiel
     */
    public static SpielLoop getSpiel() {
        return spiel;
    }

    /**
     * @param spiel the spiel to set
     */
    public static void setSpiel(SpielLoop loop) {
        spiel = loop;
    }



}
