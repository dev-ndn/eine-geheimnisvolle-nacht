package tools;

import adventurespiel.fachklassen.Inventar;
import adventurespiel.fachklassen.Koordinaten;
import adventurespiel.fachklassen.Spielfigur;
import adventurespiel.handler.XMLHandling;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import org.dom4j.Document;
import org.dom4j.Element;

/**
 * im raster:
 * 1 = begehbares feld
 * 0 = nicht begehbares feld
 *
 * im frame sichtbar, editor:
 * grün (punkt+rahmen) = zur gehflaeche gehöriges feld, begehbar
 * gelb (rechteck) = von der gehflaeche wieder entfernt, nicht begehbar
 * rot (rechteck) = zur gehflaeche hinzugefügtes feld, begehbar
 *
 * im frame sichtbar, wegfindung:
 * weiß (punkt+rahmen) = ziel
 * grau (punkt+rahmen) = start
 * pink/magenta (rahmen+punkt) = begehbares wegfeld
 * pink/magenta (rahmen) = nicht begehbares wegfeld
 *
 * @author Nadine
 */
public class GehflaechenEditor extends Frame
{
    public Document raumxml;
    public String raumbild;

    public int hoehe;
    public int breite;
//rechter weißer rahmen
    public static int startx;
//oberer weißer rahmen
    public static int starty;
    public Rectangle klickflaeche = new Rectangle(30, 80, 960, 550);
//groesse der quadratischen felder
    public static int zellengroesse = 20;
//matrix mit 0en und 1en für die gehflaeche
    public int[][] raster =null;
    public static int anzahlSpalten;
    public static int anzahlZeilen;

//Wegfindung aktiv oder Editor
    public boolean wegfindungAktiv;

//aktuelle Position
    public int aktStartSpalte, aktZielSpalte;
    public int aktStartZeile, aktZielZeile;

//buttons
    public static Button zeichnen = new Button();
    public static Button loeschen = new Button();
    public static Button zuruecksetzen = new Button();
    public static Button laden = new Button();
    public static Button speichern = new Button();
//checkbox
    public static Checkbox check = new Checkbox();

    public static Choice raumwahl = new Choice();

//listener der Buttons; was soll bei welchem buttonklick passieren
    public ActionListener al = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource().equals(zeichnen)) zeichneGehflaeche();
            if(e.getSource().equals(loeschen)) repaint();
            if(e.getSource().equals(speichern)) speicherGehflaeche();
            if(e.getSource().equals(zuruecksetzen)) {
                initGehflaeche();
                repaint();
            }
            //sicherheitshalber auskommentiert um datenmassen und überspeicherungen vorerst zu vermeiden
            //if(e.getSource().equals(speichern)) speicherGehflaeche();
            if(e.getSource().equals(laden)) {
                paint(getGraphics());
                ladeGehflaeche();
                laden.setBackground(null);
                zeichneGehflaeche();
            }
        }
    };
//adapter des mausklicks
    public MouseAdapter ma = new MouseAdapter() {

        @Override
        public void mousePressed(MouseEvent e){
            if(!wegfindungAktiv && klickflaeche.contains(e.getPoint()))klickereignis(e);
            if(wegfindungAktiv) {
                paint(getGraphics());
                wegfindungsereignis(e);
            }
        }

    };

//listener der checkbox
    public ItemListener il2 = new ItemListener() {

        public void itemStateChanged(ItemEvent e) {
            raumxml = XMLHandling.parse("src/xmls/default/"+e.getItem().toString());
            raumbild = raumxml.selectSingleNode("//raum/@quelle").getText();
            ladeGehflaeche();
        }
    };

    public ItemListener il = new ItemListener() {

        @Override
        public void itemStateChanged(ItemEvent e) {
            if(e.getStateChange()==1) wegfindungAktiv = true;
            if(e.getStateChange()==2) wegfindungAktiv = false;
            if(wegfindungAktiv){
            zeichnen.setVisible(false);
            loeschen.setVisible(false);
            zuruecksetzen.setVisible(false);
            speichern.setVisible(false);
            laden.setVisible(false);
            repaint();
        }
        if(!wegfindungAktiv){
            zeichnen.setVisible(true);
            loeschen.setVisible(true);
            zuruecksetzen.setVisible(true);
            speichern.setVisible(true);
            laden.setVisible(true);
            repaint();
        }

        }
    };


    /**
     * konstruktor
     */
    public GehflaechenEditor(String dateipfad){
        super("Gehflaecheneditor und Wegfindung");
//daten einlesen vom bild und raum
        this.raumxml = XMLHandling.parse("src/xmls/default/"+dateipfad);
        Spielfigur spielfigur = new Spielfigur("Peter",null,new Koordinaten(0,0),null);
        Inventar inventar = new Inventar(spielfigur);
        spielfigur.setInventar(inventar);
        this.raumbild = raumxml.selectSingleNode("//raum/@quelle").getText();
        this.breite = 960;
        this.hoehe = 160;
//arraydimensionen festlegen
        anzahlSpalten = (this.breite/zellengroesse);
        anzahlZeilen = (this.hoehe/zellengroesse);
        this.aktStartZeile= this.aktZielZeile=anzahlZeilen-1;
        this.aktStartSpalte= this.aktZielSpalte=anzahlSpalten/2;
        this.raster = new int[anzahlSpalten][anzahlZeilen];
//raster zu beginn 0 setzen
        ladeGehflaeche();
        this.addMouseListener(ma);

    }

    public int findeBegehbaresFeld(int startspalte,int startzeile){
        while(raster[startspalte][startzeile]==0){
            startzeile++;
        }
        return startzeile;
    }

    public void sucheWeg(int startspalte, int startzeile, int zielspalte, int zielzeile){
        int tempStartspalte=startspalte;
        int tempStartzeile=startzeile;
//erhöhe oder erniedrige solange die spalte des temporaeren Feldes bis sie der des Ziels entspricht
        while(tempStartspalte!=zielspalte){
             if(tempStartspalte>zielspalte){
             zeichneRechteck(tempStartspalte, tempStartzeile, Color.magenta);
             tempStartspalte--;
             }
             if(tempStartspalte<zielspalte){
             zeichneRechteck(tempStartspalte, tempStartzeile, Color.magenta);
             tempStartspalte++;
             }
             if(raster[tempStartspalte][tempStartzeile]==1)fuellePunkt(tempStartspalte, tempStartzeile, Color.magenta);
        }
//erhöhe oder erniedrige solange die zeile des temporaeren Feldes bis sie der des Ziels entspricht
        while(tempStartzeile!=zielzeile){
             if(tempStartzeile>zielzeile){
                 zeichneRechteck(tempStartspalte, tempStartzeile, Color.pink);
                 tempStartzeile--;
                 }
             if(tempStartzeile<zielzeile){
                 zeichneRechteck(tempStartspalte, tempStartzeile, Color.pink);
                 tempStartzeile++;
             }
             if(raster[tempStartspalte][tempStartzeile]==1)fuellePunkt(tempStartspalte, tempStartzeile, Color.pink);
        }
//zeichne auch das zielfeld farbig
        zeichneRechteck(tempStartspalte, tempStartzeile, Color.pink);
    }

    @Override
    public void paint(Graphics gr){
        Toolkit tk = getToolkit();
//zeichne das Hintergrundbild des gewaehlten Raums
        gr.drawImage(tk.getImage(this.raumbild), 0, 0, this);
//zeilen; vertikale linien zeichnen
        for(int i = 0; i < anzahlZeilen; i++){
            gr.drawLine(startx, (zellengroesse*i)+starty, this.breite+startx, (zellengroesse*i)+starty);
        }
//spalten; horizontale linien zeichnen
        for(int i = 0; i < anzahlSpalten; i++){
            gr.drawLine((zellengroesse*i)+startx, starty, (zellengroesse*i)+startx, this.hoehe+starty);
        }
        String path = "//raum/gegenstand";
        for (int i = 0; i < raumxml.selectNodes(path).size(); i++) {
            gr.drawImage(tk.getImage(raumxml.selectSingleNode(path+"["+(i+1)+"]/@quelleRaum").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@x").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@y").getText()),this);
        }
        path = "//raum/ikea";
        for (int i = 0; i < raumxml.selectNodes(path).size(); i++) {
            gr.drawImage(tk.getImage(raumxml.selectSingleNode(path+"["+(i+1)+"]/@quelle").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@x").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@y").getText()),this);
        }
        path = "//raum/tuer";
        for (int i = 0; i < raumxml.selectNodes(path).size(); i++) {
            gr.drawImage(tk.getImage(raumxml.selectSingleNode(path+"["+(i+1)+"]/@quelle").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@x").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@y").getText()),this);
        }
        path = "//raum/person";
        for (int i = 0; i < raumxml.selectNodes(path).size(); i++) {
            gr.drawImage(tk.getImage(raumxml.selectSingleNode(path+"["+(i+1)+"]/@quelle").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@x").getText()),
                    Integer.parseInt(raumxml.selectSingleNode(path+"["+(i+1)+"]/koordinaten/@y").getText()),this);
        }

        zeichneGehflaeche();
    }

    /**
     * zeichnet an der entsprechenden stelle im raster ein farbiges rechteck
     *
     * @param spalte zu findende spalte im raster
     * @param zeile zu findende zeile im raster
     * @param color farbe des rechtecks
     */
    public void fuelleRechteck(int spalte, int zeile, Color color){
        Graphics g = this.getGraphics();
        g.setColor(color);
        g.fillRect((zellengroesse*spalte)+startx, (zellengroesse*zeile)+starty, zellengroesse, zellengroesse);
    }

    /**
     * zeichnet an der entsprechenden stelle im raster ein farbiges rechteck
     *
     * @param spalte zu findende spalte im raster
     * @param zeile zu findende zeile im raster
     * @param color farbe des rechtecks
     */
    public void zeichneRechteck(int spalte, int zeile, Color color){
        Graphics g = this.getGraphics();
        g.setColor(color);
        g.drawRect((zellengroesse*spalte)+startx, (zellengroesse*zeile)+starty, zellengroesse, zellengroesse);
    }

    /**
     * zeichnet an der entsprechenden stelle im raster ein farbiges rechteck
     *
     * @param spalte zu findende spalte im raster
     * @param zeile zu findende zeile im raster
     * @param color farbe des rechtecks
     */
    public void fuellePunkt(int spalte, int zeile, Color color){
        Graphics g = this.getGraphics();
        g.setColor(color);
        g.fillOval(((zellengroesse)*spalte)+startx+6, (zellengroesse*zeile)+starty+6, zellengroesse/2, zellengroesse/2);
    }

    /**
     * zeichnet an der entsprechenden stelle im raster ein farbiges rechteck
     *
     * @param spalte zu findende spalte im raster
     * @param zeile zu findende zeile im raster
     * @param color farbe des rechtecks
     */
    public void schreibeStatus(int spalte, int zeile){
        Graphics g = this.getGraphics();
        g.setColor(Color.LIGHT_GRAY);
        g.drawString(String.valueOf(raster[spalte][zeile]),((zellengroesse)*spalte)+startx+6, (zellengroesse*zeile)+starty+12);
    }

    /**
     * fuehrt anweisungen aus die nach einem klcik erfolgen sollen
     *
     * @param e Mouseevent vom Eventhandler
     */
    public void klickereignis(MouseEvent e){
//initialisieren mit 0
        int spalte = 0;
        int zeile = 0;
        boolean ganzeSpalte = false;
        boolean ganzeZeile = false;
//spalte ermitteln
//wenn der klick rechts neben dem spielfeld erfolgt,
        if(e.getX()>(anzahlSpalten*zellengroesse)+startx) {
//...ist die aeusserste spalte aktiv
            spalte = (anzahlSpalten);
//...soll die ganze Zeile angeklickt werden
            ganzeZeile = true;
//wenn nicht rechts ausserhalb des spielfeldes geklickt wurde,
        }else{
//...ermittle die richtige Spalte, wenn der klick innerhalb des spielfeldes war
            if(!(e.getX()<startx))spalte = (e.getX()-startx)/zellengroesse;
        }
//zeile ermitteln
//wenn der klick unterhalb des spielfeldes erfolgt,
        if(e.getY()>(anzahlZeilen*zellengroesse)+starty) {
//...ist die unterste zeile aktiv
            zeile = (anzahlZeilen);
//...soll die ganze Spalte angeklickt werden
            ganzeSpalte = true;
//wenn nicht unterhalb des spielfeldes geklickt wurde,
        }else{
//...ermittle die richtige Zeile, wenn der klick innerhalb des spielfeldes war
           if(!(e.getY()<starty)) zeile = (e.getY()-starty)/zellengroesse;
        }
//spalte/zeile auf der Konsole ausgeben
            System.out.println(spalte+"/"+zeile);
//setze das Startfeld auf das ermittelte Feld
            if(raster[spalte][zeile]==1)aktZielSpalte=spalte;
            if(raster[spalte][zeile]==1)aktZielZeile=zeile;
            if(raster[spalte][zeile]==0 && wegfindungAktiv){
                aktZielZeile = findeBegehbaresFeld(spalte, zeile);
                aktZielSpalte = spalte;
            }
//wenn die wegfindung aktiviert ist, geh dahin zurueck, woher du kamst
            if(wegfindungAktiv) return;

//wenn die ganze Zeile geklickt werden soll
            if(ganzeZeile) {
                for(int i= 0;i< anzahlSpalten;i++){
//..aendere den Status fuer jedes Feld in dieser Zeile
                    aendereFeldstatus(i, zeile);
                    repaint();
                }
                return;
            }
//wenn die ganze Spalte geklickt werden soll
            if(ganzeSpalte) {
                for(int i= 0;i< anzahlZeilen;i++){
//..aendere den Status fuer jedes Feld in dieser Spalte
                    aendereFeldstatus(spalte, i);
                    repaint();
                }
//geh dahin zurueck wo du herkamst
                return;
            }
//falls nicht rechts oder unterhalb des spielfeldes geklickt wurde, aendere den Status des ermittelten Feldes
            aendereFeldstatus(spalte, zeile);
            repaint();
        
    }

    /**
     * fuehrt anweisungen aus die nach einem klick erfolgen sollen, wenn die
     * wegfindung aktiv ist
     * setzt das Zielfeld als neues Startfeld
     *
     * @param e Mouseevent vom Eventhandler
     */
    public void wegfindungsereignis(MouseEvent e){
//zeichne ein graues Rechteck und einen grauen gefüllten Punkt auf das Zielfeld
        zeichneRechteck(aktStartSpalte, aktStartZeile, Color.gray);
        fuellePunkt(aktStartSpalte, aktStartZeile, Color.gray);
//finde die aktuelle teile und spalte
        klickereignis(e);
//zeichne ein weißes Rechteck und einen weißen gefüllten Punkt auf das Startfeld
        zeichneRechteck(aktZielSpalte, aktZielZeile, Color.white);
        fuellePunkt(aktZielSpalte, aktZielZeile, Color.white);
        sucheWeg(aktStartSpalte, aktStartZeile, aktZielSpalte, aktZielZeile);
//setze das Zielfeld als neues Startfeld
        aktStartSpalte=aktZielSpalte;
        aktStartZeile=aktZielZeile;
    }

    /**
     * wenn der status des feldes an der stelle spalte/zeile im raster 0 war, wird er auf 1 gesetzt
     * wenn der status des feldes  an der stelle spalte/zeile im raster 1 war, wird er wieder auf 0 gesetzt und gelb gezeichnet
     *
     * @param spalte zu findende spalte im raster
     * @param zeile zu findende zeile im raster
     */
    public void aendereFeldstatus(int spalte, int zeile){

        if(this.raster[spalte][zeile] == 0){
            this.raster[spalte][zeile] = 1;
            fuelleRechteck(spalte, zeile, Color.red);
            return;
        }
        if(this.raster[spalte][zeile] == 1) {
            this.raster[spalte][zeile] = 0;
            fuelleRechteck(spalte, zeile, Color.yellow);
        }
        }

    /**
     * setzt den inhalt des raster auf 0en zurück
     */
    public void initGehflaeche(){

        for(int i = 0; i < (anzahlSpalten); i++){
            for(int j = 0; j < (anzahlZeilen); j++){
                this.raster[i][j] = 1;
            }
        }
    }

    /**
     * zeichnet jede stelle im raster, die begehbar sein soll(den wert 1 hat) grün
     */
    public void zeichneGehflaeche(){

        for(int i = 0; i < anzahlSpalten; i++){
            for(int j = 0; j < anzahlZeilen; j++){
                if(this.raster[i][j] == 1) zeichneRechteck(i, j, Color.green);
                schreibeStatus(i, j);
            }
        }
        rasterAufKonsole();
    }

    /**
     * speichert die aktuell in raster enthaltene gehflaeche in eine xml-datei
     */
    public void speicherGehflaeche(){
        speichern.setBackground(Color.red);
        Document doc = XMLHandling.erzeugeDocument("root");
        doc.getRootElement().addAttribute("zellengroesse",String.valueOf(zellengroesse));
        doc.getRootElement().addAttribute("anzahlSpalten",String.valueOf(anzahlSpalten));
        doc.getRootElement().addAttribute("anzahlZeilen",String.valueOf(anzahlZeilen));
        doc.getRootElement().addAttribute("abstandOben",String.valueOf(starty));
        doc.getRootElement().addAttribute("abstandLinks",String.valueOf(startx));
        doc.getRootElement().addAttribute("hoehe",String.valueOf(hoehe));
        doc.getRootElement().addAttribute("breite",String.valueOf(breite));
        doc.getRootElement().addAttribute("raumxml",this.raumxml.getName().substring(17));
        Element feld;
        int zaehler = 0;
        for(int i = 0; i < (anzahlSpalten); i++){
            for(int j = 0; j < (anzahlZeilen); j++){
                if(this.raster[i][j]==0){
                 feld = doc.getRootElement().addElement("feld"+zaehler);
                 feld.addAttribute("spalte",String.valueOf(i));
                 feld.addAttribute("zeile",String.valueOf(j));
                 feld.addAttribute("status", String.valueOf(this.raster[i][j]));
                 }
                 zaehler++;
            }
        }

        XMLHandling.ueberschreibeDatei("src/xmls/default/gehflaeche"+this.raumxml.getName().substring(17), doc);
        speichern.setBackground(null);
    }

    public void ladeGehflaeche(){
        laden.setBackground(Color.red);
        Document doc = XMLHandling.parse("src/xmls/default/gehflaeche"+raumxml.getName().substring(17));
        zellengroesse = Integer.parseInt(doc.selectSingleNode("//root/@zellengroesse").getText());
        anzahlSpalten = Integer.parseInt(doc.selectSingleNode("//root/@anzahlSpalten").getText());
        anzahlZeilen = Integer.parseInt(doc.selectSingleNode("//root/@anzahlZeilen").getText());
        hoehe = Integer.parseInt(doc.selectSingleNode("//root/@hoehe").getText());
        breite = Integer.parseInt(doc.selectSingleNode("//root/@breite").getText());
        starty = Integer.parseInt(doc.selectSingleNode("//root/@abstandOben").getText());
        startx = Integer.parseInt(doc.selectSingleNode("//root/@abstandLinks").getText());
        this.raster = new int[anzahlSpalten][anzahlZeilen];
        int zaehler = 0;
        for(int i = 0; i < (anzahlSpalten); i++){
            for(int j = 0; j < (anzahlZeilen); j++){
                if(doc.selectSingleNode("//feld"+zaehler+"/@status") == null){
                    this.raster[i][j] =1;
                }else {
                    this.raster[i][j] = Integer.parseInt(doc.selectSingleNode("//feld"+zaehler+"/@status").getText());
                }
                 zaehler++;
            }
        }
        rasterAufKonsole();
        laden.setBackground(null);
        repaint();
    }

    public void rasterAufKonsole(){
        for (int i = 0; i < anzahlZeilen; i++) {
            System.out.println("\t");
            for (int j = 0; j < anzahlSpalten; j++) {
                System.out.print(raster[j][i]+" ");
            }
        }
    }



    public static void main(String[] args) {

        GehflaechenEditor fenster = new GehflaechenEditor("Raum6.xml");
        fenster.setSize(1024, 768);
        fenster.setLocationRelativeTo(null);
        fenster.setLayout(null);
        fenster.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        zeichnen.setLabel("Gehfläche zeichnen");
        zeichnen.setLocation(30, 700);
        zeichnen.setSize(120,30);
        zeichnen.addActionListener(fenster.al);
        fenster.add(zeichnen);

        loeschen.setLabel("Markierungen löschen");
        loeschen.setLocation(160, 700);
        loeschen.setSize(140,30);
        loeschen.addActionListener(fenster.al);
        fenster.add(loeschen);

        zuruecksetzen.setLabel("Gehfläche zurücksetzen auf 0");
        zuruecksetzen.setLocation(310, 700);
        zuruecksetzen.setSize(170,30);
        zuruecksetzen.addActionListener(fenster.al);
        fenster.add(zuruecksetzen);

        speichern.setLabel("Gehfläche in xml speichern");
        speichern.setLocation(490, 700);
        speichern.setSize(160,30);
        speichern.addActionListener(fenster.al);
        fenster.add(speichern);

        laden.setLabel("Gehfläche aus xml laden");
        laden.setLocation(660, 700);
        laden.setSize(160,30);
        laden.addActionListener(fenster.al);
        fenster.add(laden);

        check.setLabel("Wegfindung aktivieren");
        check.setLocation(30,40);
        check.setSize(160,30);
        check.addItemListener(fenster.il);
        fenster.add(check);
        fenster.wegfindungAktiv = false;

        for (int i = 0; i < 6; i++) {
            raumwahl.add("Raum"+(i+1)+".xml");
        }
        raumwahl.select("Raum6.xml");
        raumwahl.setLocation(200,40);
        raumwahl.setSize(160, 30);
        raumwahl.addItemListener(fenster.il2);
        fenster.add(raumwahl);

        fenster.setVisible(true);
    }
}
