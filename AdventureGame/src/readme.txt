Inhalt
=========================================
1. Einleitung
2. Verantwortung
3. spielstart mit Windows
4. Spielstart mit Mac
5. Kurzanleitung Spielprinzip
6. Lizenz fuer Dom4J-Framework
7. Lizenz fuer jaxen-Framework

========================================================================================================
1. Einleitung
=========================================
"Eine geheimnisvolle Nacht" entstand waehrend des Wintersemesters 2009/10 (3.Semester) im Studiengang 
Informationstechnologie und Gestaltung (kurz bekannt unter IGi) als Softwareprojekt von:

Mounir Ben-Sabeur
Elisa Schoenherr
Simon Jokuschies
Jessica Ullrich
Elena Wagner
Alena Wagner
Alisa Dymshits
Nadine Sauvan

Copyright 2010

========================================================================================================
2. Verantwortung
==========================================
Die oben genannten Personen (nachfolgend "wir") uebernehmen keine Verantwortung fuer moeglicherweise
entstehende Schaeden durch das Spiel. Wir haben uns sehr bemueht, koennen jedoch fuer nichts garantieren.

Das Projekt entstand innerhalb eines studentischen Projekts und garantiert daher keine Fehlerfreiheit.
Nach bestem Wissen und Gewissen wurde getestet. 
Wir hoffen, dass das Spiel Spass bereitet und weder grosse inhaltliche noch technische Maengel unerwartet 
aufweist.

Durch Veraendern der mitgelieferten Dateien koennen Funktionsprobleme auftreten, daher ist es anzuraten
nichts zu bearbeiten, hinzuzufuegen oder zu loeschen. Wir garantieren keine Lauffaehigkeit bei 
Veraenderung der im Ordner "Eine_geheimnisvolle_Nacht" mitgelieferten Daten.

========================================================================================================
3. Spielstart mit WINDOWS
==========================================
# spiel_starten_Win.bat ausfuehren

oder 

auf der Konsole (cmd.exe) folgende Zeile eingeben

(bitte selbst genauer ueberpruefen, bevor diese Variante versucht wird)

# java -Xms64m -Xmx256m -jar [hier dateipfad eintragen]\Eine_geheimnisvolle_Nacht\dist\game.jar

[hier dateipfad eintragen] ersetzen durch den Dateipfad mit den uebergeordneten Ordnern 

Beispiel:
Eine_geheimnisvolle_Nacht wurde direkt auf der Festplatte C: abgelegt, dann lautet die Konsolenzeile

java -Xms64m -Xmx256m -jar c:\Eine_geheimnisvolle_Nacht\dist\game.jar

========================================================================================================
4. Spielstart mit MAC
==========================================

# spiel_starten_Mac.command ausfuehren

oder

(bitte selbst genauer ueberpruefen, bevor diese Variante versucht wird)

# im Terminal folgende Zeile eingeben

java -Xms64m -Xmx256m -jar [hier dateipfad eintragen]/Eine_geheimnisvolle_Nacht/dist/game.jar

[hier dateipfad eintragen] ersetzen durch den Dateipfad zum Spielordner mit den uebergeordneten Ordnern


========================================================================================================
5. Kurzanleitung Spielprinzip
==========================================

# Rechtsklick im Inventar (Leiste unten) gibt weitere Infos �ber die Gegenstaende im Inventar

# Linksklick waehlt einen Gegenstand im Inventar aus (schwarzer Rahmen)

--> Klick auf einen zweiten Gegenstand versucht beide zu kombinieren

--> Klick im Spielfeld auf ein Objekt versucht beide miteinander interagieren zu lassen

# Linksklick im Spielfeld setzt die Figur in Gang, wenn die Entfernung gro� genug ist

--> anschliessend
 
wird ein Gegenstand eingesammelt, wenn einer angeklickt wurde

oder

wird der Raum gewechselt, wenn man eine T�r angeklickt hat

oder 

ein Text zum angeklickten Objekt ausgegeben, falls es einen gibt

========================================================================================================
6. Lizenz fuer Dom4J-Framework
===========================================

Redistribution and use of this software and associated documentation ("Software"), with or without 
modification, are permitted provided that the following conditions are met:

   1. Redistributions of source code must retain copyright statements and notices. Redistributions must 
also contain a copy of this document.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions 
and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3. The name "DOM4J" must not be used to endorse or promote products derived from this Software without 
prior written permission of MetaStuff, Ltd. For written permission, please contact dom4j-info@metastuff.com.
   4. Products derived from this Software may not be called "DOM4J" nor may "DOM4J" appear in their names 
without prior written permission of MetaStuff, Ltd. DOM4J is a registered trademark of MetaStuff, Ltd.
   5. Due credit should be given to the DOM4J Project - http://www.dom4j.org

THIS SOFTWARE IS PROVIDED BY METASTUFF, LTD. AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESSED OR IMPLIED 
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL METASTUFF, LTD. OR ITS CONTRIBUTORS BE LIABLE FOR 
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) 
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
POSSIBILITY OF SUCH DAMAGE.

Copyright 2001-2005 (C) MetaStuff, Ltd. All Rights Reserved. 

========================================================================================================
7. Lizenz fuer jaxen-Framework
===========================================

/*
 $Id: LICENSE.txt 1128 2006-02-05 21:49:04Z elharo $

 Copyright 2003-2006 The Werken Company. All Rights Reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:

  * Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.

  * Neither the name of the Jaxen Project nor the names of its
    contributors may be used to endorse or promote products derived 
    from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */
